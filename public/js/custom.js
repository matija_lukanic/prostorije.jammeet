$(document).ready(function() {
	$(".dan-checkbox").change(function() {
		$(this).parent().toggleClass("checked-div");
	});
	$(".mjesec-checkbox").change(function() {
		$(this).parent().toggleClass("checked-div");
	});

	$(".godina-checkbox").change(function() {
		$(this).parent().toggleClass("checked-div");
	});


});


	function createHrDatum(date){
		//napravi naziv dana, mjeseca i godine na hr iz datuma
		var months = [
			"siječanj",
			"veljača",
			"ožujak",
			"travanj",
			"svibanj",
			"lipanj",
			"srpanj",
			"kolovoz",
			"rujan",
			"listopad",
			"studeni",
			"prosinac",
		];
		var days = [
			"Ponedjeljak",
			"Utorak",
			"Srijeda",
			"Četvrtak",
			"Petak",
			"Subota",
			"Nedjelja"
		];
		var d = new Date(date);

		return days[d.getDay()-1] + ' ' + d.getDate() + '. ' + (d.getMonth()+1) + '. ' + d.getFullYear() + '.';

	}
