<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Mjesto extends Model
{
    public $table = 'mjesto';

    public function zupanija() {
        return $this->belongsTo("App\Zupanija", "id_zupanija");

    }

}
