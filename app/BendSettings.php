<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BendSettings extends Model
{
    public $table = 'bend_settings';
    public $timestamps = false;


    protected $fillable = ['user_id', 'jammeet_id', 'category', 'notes'];

}
