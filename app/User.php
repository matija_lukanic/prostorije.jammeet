<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Http\Resources\ProstorijeGalerijaForJammeet as GalerijaResource;



class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        //'name', 'email', 'password', 'id_mjesto'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function mjesto() {
	     return $this->belongsTo("App\Mjesto", "id_mjesto");

    }


    public function prostorije() {
        return $this->hasMany("App\TerminiProstorije", "user_id");

    }


    public function getSmProfile(){
        return $this->profil_slika;
    }

    public function getSlobodniTermini($request) {
        //izvuci sve slobodne termine od korisnikovih prostorije

        $terminiProstorije = [];

        foreach ($this->prostorije as $prostorija) {
          // code...
          //$terminiProstorije[$prostorija->id] = $prostorija->getSlobodniTermini($request);
          $terminiProstorije[$prostorija->id]['prostorija_naziv'] = $prostorija->name;
          $terminiProstorije[$prostorija->id]['prostorija_opis'] = $prostorija->opis;
          $terminiProstorije[$prostorija->id]['prostorija_profil'] = $prostorija->getProfil();
          $terminiProstorije[$prostorija->id]['prostorija_galerija'] = GalerijaResource::collection($prostorija->gallery);
          $terminiProstorije[$prostorija->id]['termini'] = $prostorija->getSlobodniTermini($request);
          //var_dump($prostorija->id);
        }

        return $terminiProstorije;

    }

    public function orderedVrijeme()
    {
      //dobijem array - sva vremena termina od svih prostorija korisnika poredane po kljucu
      //[13301730] = $termin

        $user_termini_ordered = [];

        foreach ($this->prostorije as $prostor)
        {

          foreach ($prostor->termini as $key => $termin)
          {

            $p = substr($termin->vrijeme->pocetak, 0, -3);
            $k = substr($termin->vrijeme->kraj, 0, -3);

            $user_termini_ordered[$p.$k] = $termin;

          }

        }

        ksort($user_termini_ordered);
        return $user_termini_ordered;

    }

    public function getAllProstorijeTermini()
    {

      //$user_termini_ordered = $this->orderedVrijeme();
      $event_array = [];

      //ovo je da se ne dupliciraju datumi i vrijeme kad su odabrane sve Prostorije
      //npr 21.3.2020 postoji termin 13-15 za sve prostorije, s ovim ce se prikazivati samo jedan
      $addedDatumAndVrijeme = [];

      foreach ($this->prostorije as $key => $prostor)
      {

        foreach ($prostor->termini as $key => $termin)
        {

          foreach ($termin->kalendar as $kalendar)
          {

            if(!in_array($kalendar->datum.$termin->vrijeme->id, $addedDatumAndVrijeme))
            {
                $addedDatumAndVrijeme[] = $kalendar->datum.$termin->vrijeme->id;

                if($kalendar->rezervirano)
                {
                  $user_rezervirao = [];

                  /*
                  OVO NE BRISAT OVAKO BI TREBALO NEKAKO FUNKCIONIRAT KAD BUDE SVE OK
                  $user_rezervirao['name'] = $kalendar->rezervirano->user->name;
                  $user_rezervirao['slug'] = $kalendar->rezervirano->user->slug;
                  $user_rezervirao['profil_slika'] = $kalendar->rezervirano->user->profil_slika;
                  */

                  switch ($kalendar->rezervirano->user_id) {
                      case 1:
                          $user_rezervirao['name'] = 'Mamut';
                          $user_rezervirao['profil_slika'] = 'https://jammeet.com/slike_burza_m/avatari/mini-15573476095jpg.jpg';
                          break;
                      case 2:
                          $user_rezervirao['name'] = 'Asheraah';
                          $user_rezervirao['profil_slika'] = 'https://jammeet.com/slike_burza_m/avatari/mini-15582820425732622224826114684382838836574265793839104ojpg.jpg';
                          break;
                      case 3:
                          $user_rezervirao['name'] = 'Sillycons';
                          $user_rezervirao['profil_slika'] = 'https://jammeet.com/slike_burza_m/avatari/mini-1555932601coverjpg.jpg';
                          break;
                      default:
                          echo "Your favorite color is neither red, blue, nor green!";
                  }


                  $editable = false;
                }
                else
                {
                  $user_rezervirao = '';
                  $editable = true;
                }

                $event_array[] = [
                        'id' => $kalendar->id,
                        'title' => substr($termin->vrijeme->pocetak, 0, -3) . ' : ' . substr($termin->vrijeme->kraj, 0, -3),
                        'start' => substr($kalendar->datum, 0, -9) . 'T' . $termin->vrijeme->pocetak,
                        'end' => substr($kalendar->datum, 0, -9) . 'T' . $termin->vrijeme->kraj,
                        'termin_id' => $termin->id,
                        'datum_str' => substr($kalendar->datum, 0, -9),
                        'user' => $user_rezervirao,
                        'editable' => false //ne moze se drag n droppat na drugi datum
                        //'editable' => $editable,
                ];

            }


          }

        }
      }

      //dd($event_array);
      return $event_array;

    }



    public function getAllRezerviraniTerminiIds() {
        //izvuci sve id-jeve od rezerviranh termina koje user ima, i ove u buducnosti i u proslosti

        $termini = [];
        foreach ($this->prostorije as $prostorija)
        {
            foreach ($prostorija->termini as $termin)
            {
                foreach ($termin->kalendar as $kalendar)
                {
                    if($kalendar->rezervirano)
                    {
                        $termini[] = $kalendar->rezervirano->id;
                    }
                }


            }

        }
        return $termini;

    }



}
