<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Http\Resources\TerminiKalendarForJammeet as KalendarResource;
use App\TerminiKalendar;

class TerminiVrijemeProstorijeVeza extends Model
{
    public $table = 'termini_vrijeme_prostorije_veza';

    public function prostorija() {
        return $this->belongsTo("App\TerminiProstorije", "prostorija_id");

    }

    public function vrijeme() {
        return $this->belongsTo("App\TerminiVrijeme", "termin_id");

    }

    public function kalendar() {
        return $this->hasMany("App\TerminiKalendar", 'prostorija_termini_vrijeme_id');

    }

    public function getSlobodniTermini($request) {
        //izvuci sve slobodne termine iz kalendara

        $todayYMD = Carbon::now('Europe/Zagreb')->toDateString();
        $nowVrijeme = Carbon::now('Europe/Zagreb')->format('H:i:s'); // 15:33:21

        $startDate = isset($request->datum_od) ? $request->datum_od : Carbon::now('Europe/Zagreb')->toDateString();
        $startDate = !Carbon::parse($startDate)->isPast() ? $startDate : Carbon::now('Europe/Zagreb')->toDateString();

        $endDate = isset($request->datum_do) ? $request->datum_do : Carbon::now('Europe/Zagreb')->addWeek()->toDateString();
        $endDate = !Carbon::parse($startDate)->gt(Carbon::parse($endDate)) ? $endDate : Carbon::parse($startDate)->addWeek()->toDateString();

        $startVrijeme = isset($request->vrijeme_od) ? $request->vrijeme_od : '00:00:00';
        $endVrijeme = isset($request->vrijeme_do) ? $request->vrijeme_do : '24:00:00';

        //TerminiKalendar::whereBetween('datum', [$startDate, $endDate])->get();

        $termini = [];
        $termini2 = [];
        foreach (TerminiKalendar::whereBetween('datum', [$startDate, $endDate])->where('prostorija_termini_vrijeme_id', $this->id)->get() as $termin) {

          if(!$termin->rezervirano && $startVrijeme <= $termin->termin_prostorija_vrijeme->vrijeme->pocetak && $endVrijeme >= $termin->termin_prostorija_vrijeme->vrijeme->pocetak)
          {
              //ako je search za vrijeme od 17:00 do 21:00
              //prikazat cu i termine koje pocinju u 21:00

              $datumSh = substr($termin->datum, 0, -9);

              if( !($todayYMD == $datumSh && $termin->termin_prostorija_vrijeme->vrijeme->pocetak <= $nowVrijeme) )
              {
                  //nemoj dodat ako se radi o danasnjem danu i ako je vrijeme termina u proslosti
                  $termini[$datumSh] = $termin;
              }
          }
        }
        return $termini;

    }


}
