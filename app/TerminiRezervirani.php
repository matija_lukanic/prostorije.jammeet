<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\User;
use App\TerminiProstorije;

class TerminiRezervirani extends Model
{
    public $table = 'termini_rezervirani';

    protected $guarded = ['id'];

    public function kalendar() {
        return $this->belongsTo("App\TerminiKalendar", "termini_kalendar_id");

    }

    public static function checkIfUserCanReserveProba($prostorije_user_id, $jammeet_user_id) {

        $user = User::find($prostorije_user_id);

        $maxNumOfProbaInAdvance = $user->probe_unaprijed;
        $probeBenda = TerminiProstorije::bendUkupnoProba($prostorije_user_id, $jammeet_user_id);


        $result = [
            'canAddProba' => false,
            'message'=> 'Kod '.$user->name.' maksimalan broj rezervacija proba unaprijed je '. $maxNumOfProbaInAdvance . ', Vi trenutno imate dogovoreno ' . $probeBenda['poslije'] .'.',
            'alertType' => 'warning'
        ];

        if($maxNumOfProbaInAdvance > $probeBenda['poslije'])
        {
            $result = [
                'canAddProba' => true,
                'message'=> '',
                'alertType' => 'success'
            ];
        }

        return $result;

    }

    //public static function getNumberOfProbaRezervi


        /*
    public function user() {

        if( !empty($this->user_id) )
        {
          return $this->user_id;
        }
        else if( !empty($this->naziv_benda) )
        {
          return $this->naziv_benda;
        }
        else
        {
          return false;
        }

    }


    OVO SU USER-i SA JAMMEET-a

    public function user() {
        return $this->belongsTo("App\Users", "user_id");

    }

      */

}
