<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\TerminiKalendar as TerminiKalendarResource;

class TerminiVrijemeProstorijeVeza extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
              'id' => $this->id,
              'prostorija_id' => $this->prostorija_id,
              'termin_id' => $this->termin_id,
              'vrijeme' => $this->vrijeme,
              'kalendar' => TerminiKalendarResource::collection($this->kalendar)
              ];
    }
}
