<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public $preserveKeys = true;

    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
              'id' => $this->id,
              'name' => $this->name,
              'slug' => $this->slug,
              'opis' => $this->opis,
              'email' => $this->email,
              'facebook' => $this->facebook,
              'instagram' => $this->instagram,
              'date_end' => $this->date_end,
              'youtube' => $this->youtube,
              'mobitel' => $this->mobitel,
              'profil_slika' => $this->profil_slika,
              'mjesto' => $this->mjesto->naziv,
              'probe_unaprijed' => $this->probe_unaprijed,
              'slobodniTermini' => $this->getSlobodniTermini($request),
              ];
    }
}
