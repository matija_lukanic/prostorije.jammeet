<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\TerminiVrijemeProstorijeVeza as TerminiVrijemeProstorijeVezaResource;

class TerminiProstorije extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
              'id' => $this->id,
              'name' => $this->name,
              'opis' => $this->opis,
              'profil_slika' => $this->getProfil(),
              //'termini' => TerminiVrijemeProstorijeVezaResource::collection($this->termini),
              'vrijemeOrdered' => $this->vrijemeOrdered()
              ];
    }
}
