<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TerminiRezervirani extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
              'id' => $this->id,
              'termini_kalendar_id' => $this->termini_kalendar_id ,
              'user_id' => $this->user_id,
              'naziv_benda' => $this->naziv_benda
              ];
    }
}
