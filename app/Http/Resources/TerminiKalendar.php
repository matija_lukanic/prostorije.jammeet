<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TerminiKalendar extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
              'id' => $this->id,
              'termin_id' => $this->termin_prostorija_vrijeme->id,
              'title' => substr($this->termin_prostorija_vrijeme->vrijeme->pocetak, 0, -3) . ' : ' . substr($this->termin_prostorija_vrijeme->vrijeme->kraj, 0, -3),
              'start' => $this->datum . 'T' . $this->termin_prostorija_vrijeme->vrijeme->pocetak,
              'end' => $this->datum . 'T' . $this->termin_prostorija_vrijeme->vrijeme->kraj,
              'datum_str' => $this->datum,
              'rezervirano' => $this->rezervirano,
              'num' => null //ovo se odnosi na kad je All prostorije tab otvoren, da datumi imaju npr 1/3 ako je jedan termin zauzet koji je dostupan za sve prostorije              
              ];
    }
}
