<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserMjesta extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
              'id' => $this->id,
              'mjesto' => $this->naziv,
              'id_zupanija' => $this->zupanija->id,
              'zupanija' => $this->zupanija->naziv,
              'id_drzava' => $this->zupanija->drzava->id,
              'drzava' => $this->zupanija->drzava->naziv,
              ];
    }
}
