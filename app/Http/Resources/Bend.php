<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Bend extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public $preserveKeys = true;

    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
              'id' => $this->id,
              'user_id' => $this->user_id,
              'jammeet_id' => $this->jammeet_id,
              'category' => $this->category,
              'notes' => $this->notes,
              ];
    }
}
