<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TerminiVrijeme;
use App\TerminiVrijemeProstorijeVeza;
use App\TerminiKalendar;
use App\TerminiProstorije;
use App\Http\Resources\TerminiVrijemeProstorijeVeza as TerminiVrijemeProstorijeVezaResource;
use App\Http\Resources\TerminiKalendar as TerminiKalendarResource;
use Carbon\Carbon;

class TerminiVrijemeProstorijeVezaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $pocetak = $request->pocetak;
        $kraj = $request->kraj;
        $prostorija_id = $request->prostorija_id;

        $termin = TerminiVrijeme::where('pocetak', $pocetak)->where('kraj', $kraj)->first();

        if($termin == null)
        {            
          $termin = new TerminiVrijeme;
          $termin->pocetak = $pocetak;
          $termin->kraj = $kraj;
          $termin->created_at = Carbon::now('Europe/Zagreb');
          $termin->updated_at = $termin->created_at;
          $termin->save();
        }


        if($prostorija_id == 'all')
        {
            $prostorije_arr = TerminiProstorije::where('user_id', $request->user_id)->pluck('id');
        }
        else
        {
            $prostorije_arr = [$prostorija_id];
        }

        $i = 0;    
        foreach ($prostorije_arr as $prostorija_id) {

            $termin_user = TerminiVrijemeProstorijeVeza::where('prostorija_id', $prostorija_id)->where('termin_id', $termin->id)->first();
          
            if($termin_user == null)
            {            
              $termin_user = new TerminiVrijemeProstorijeVeza;
              $termin_user->prostorija_id = $prostorija_id;
              $termin_user->termin_id = $termin->id;
              $termin_user->created_at = Carbon::now('Europe/Zagreb');
              $termin_user->updated_at = $termin_user->created_at;
              
              if($termin_user->save())
              {
                  $i++;
              }
            }

        }

           
        if($i == count($prostorije_arr))
        {
            return new TerminiVrijemeProstorijeVezaResource($termin_user);
        }
        else
        {
            return response()->json(false);
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($prostorija_id)
    {
        
        $termini = TerminiVrijemeProstorijeVeza::where('prostorija_id', $prostorija_id)->pluck('id');
        $termini = TerminiKalendar::whereIn('prostorija_termini_vrijeme_id', $termini)->get();

        return TerminiKalendarResource::collection($termini);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
