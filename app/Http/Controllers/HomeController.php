<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Mjesto;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //Auth::loginUsingId(1);
        $this->middleware('auth',  ['only' => ['dashboard']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home/index');
    }
    public function dashboard()
    {
      $user = Auth::user();

      return view('index', compact('user'));
    }
}
