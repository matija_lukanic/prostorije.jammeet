<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\Mjesto;
use App\BendSettings;
use App\Http\Resources\Bend as BendResource;
use App\Http\Resources\UserMjesta as UserMjestaResource;
use Auth;
use File;
use Illuminate\Support\Facades\Response;

class ApiBendoviController extends Controller
{
    //

    public function get(Request $request) {


        $settings = BendSettings::where('user_id', $request->user_id)->where('jammeet_id', $request->jammeet_id)->first();

        if($settings != null)
        {
            return new BendResource($settings);
        }
        else{
            return response()->json( [
                'category' => 0,
                'notes' => ''
            ] );
        }


    }


    public function save(Request $request) {

        $settings = BendSettings::where('user_id', $request->user_id)->where('jammeet_id', $request->jammeet_id)->first();

        if($settings == null)
        {
            $settings = BendSettings::create($request->all());
        }
        else{
            $settings->update($request->all());
        }


        return new BendResource($settings);

    }


}
