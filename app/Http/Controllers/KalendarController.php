<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

//use App\Users;
use App\Bend;

use App\User;
use App\TerminiRezervirani;
use App\TerminiVrijeme;
use App\TerminiProstorije;
use App\TerminiVrijemeProstorijeVeza;

use Carbon\Carbon;
use \DateTime;


class KalendarController extends Controller
{

    public static  $title = "";
    public static  $title_h1 = "";
    public static  $desc = "";
    public static  $tip_benda_title = "";
    public static  $tip_benda_desc = "";
    public static  $pretraga = array();

   public function __construct()
   {
       $this->middleware('auth');
   }

    public function index(Request $request) {


      //$jammeetUsers = json_decode(file_get_contents("https://jammeet.com/api/users/all?pass=wbrCf}tq7T@^NSw4~ZZu%SrmV.:nx27G83a[g[Jp~67LnJbjjcd,PZ4"), true);
      $jammeetUsers = file_get_contents("https://jammeet.com/api/users/all?pass=wbrCf}tq7T@^NSw4~ZZu%SrmV.:nx27G83a[g[Jp~67LnJbjjcd,PZ4");

      $user = Auth::user();

      //dd($jammeetUsers);
      //lista termina koje prostor(ije) imaju
      $user_termini_ordered = $user->orderedVrijeme();

      //lista termina u kalendaru koja se prikaze kad se ucita kalendar
      //$event_array = json_encode($user->getAllProstorijeTermini());
      $event_array = json_encode([]);

      return view('kalendar', compact('user', 'user_termini_ordered', 'event_array', 'jammeetUsers'));
    }


    public function pojedini_prostor($id, Request $request) {

      $prostor = TerminiProstorije::find($id);
      $user = $prostor->user;

      return view('kalendar', compact('prostor', 'user'));
    }



}
