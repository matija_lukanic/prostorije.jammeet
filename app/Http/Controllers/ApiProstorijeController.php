<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\TerminiProstorije;
use App\ProstorijeGallery;
use App\Http\Resources\TerminiProstorije as TerminiProstorijeResource;
use Auth;
use File;

class ApiProstorijeController extends Controller
{
    //

    public function delete(Request $request) {

          File::delete('uploads/prostorije/' . $request->input('img'));

          if( $request->input('id') !== null) {
            ProstorijeGallery::find($request->input('id'))->delete();
          }

    }

    public function update(Request $request) {

          ProstorijeGallery::where('prostorija_id', $request->input('prostorija_id'))->update(['profile' => 0]);

          ProstorijeGallery::where('prostorija_id', $request->input('prostorija_id'))->where('id', $request->input('img_id'))->update(['profile' => 1]);

    }


    public function get(Request $request) {


        if($request->pass == 'wbrCf}tq7T@^NSw4~ZZu%SrmV.:nx27G83a[g[Jp~67LnJbjjcd,PZ4')
        {
          $prostorije = TerminiProstorije::orderBy('name')->get();
          return TerminiProstorijeResource::collection($prostorije);
        }

    }

}
