<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\TerminiProstorije;
use App\TerminiVrijemeProstorijeVeza;
use App\TerminiKalendar;
use App\TerminiRezervirani;
use App\ProstorijeGallery;
use Auth;

class ProstorijeController extends Controller
{
    //
    public function index()
    {
      $user= Auth::user();

      return view('prostorije/index', compact('user'));
    }

     public function __construct()
     {
         $this->middleware('auth');
         //$this->middleware('auth')->only(['uredi']);
     }


    public function create(Request $request)
    {

        $user= Auth::user();

    	  if ($request->isMethod('post')) {

          $validatedData = $request->validate([
              'name' => 'required',
          ]);

    	  	$new = new TerminiProstorije;
          $new->name = $request->input('name');
          $new->opis = $request->input('opis');
    	  	//$new->profil_slika = empty($request->input('profil_slika')) ? 'default.jpg' : $request->input('profil_slika');
    	  	$new->user_id = $user->id;

    	  	$new->save();

          $imgs = explode(',', $request->input('profil_slika'));
          if(count($imgs)>0 && $imgs[0] != '')
          {
              foreach ($imgs as $key => $imageName) {

                  $newImg = new ProstorijeGallery;
                  $newImg->url = $imageName;
                  $newImg->prostorija_id = $new->id;
                  $newImg->profile = $key == 0 ? 1 : 0;
                  $newImg->save();

              }
          }

          return redirect()->route('uredi-prostoriju', ['id' => $new->id]);

    	  }


      return view('prostorije/prostorije-novo', compact('user'));
    }

    public function uredi($id, Request $request)
    {

        $user= Auth::user();
        $prostorija= TerminiProstorije::findOrFail($id);

        if( !in_array($id, $user->prostorije->pluck('id')->toArray())){
          abort(403, 'Unauthorized action.');
        }


      return view('prostorije/prostorija', compact('user', 'prostorija'));
    }

      public function posturedi($id, Request $request)
    {
          $prostorija= TerminiProstorije::findOrFail($id);

          $prostorija->name = $request->input('name');
          $prostorija->opis = $request->input('opis');

    	  	$prostorija->save();

          $imgs = explode(',', $request->input('profil_slika'));

          $profileImg = ProstorijeGallery::where('prostorija_id', $prostorija->id)->where('profile', 1)->first();

          if(count($imgs)>0 && $imgs[0] != '')
          {
              foreach ($imgs as $key => $imageName) {

                  $newImg = new ProstorijeGallery;
                  $newImg->url = $imageName;
                  $newImg->prostorija_id = $prostorija->id;
                  if($profileImg == null && $key == 0)
                  {
                    $newImg->profile = 1;
                  }
                  $newImg->save();

              }
          }

          return redirect()->route('uredi-prostoriju', ['id' =>$prostorija->id]);
    }


    public function delete($id, Request $request)
    {

        $user= Auth::user();
        $prostorija= TerminiProstorije::findOrFail($id);

        if( !in_array($id, $user->prostorije->pluck('id')->toArray())){
          abort(403, 'Unauthorized action.');
        }

        $all = TerminiVrijemeProstorijeVeza::where('prostorija_id', $id)->pluck('id')->toArray();
        $all2 = TerminiKalendar::whereIn('prostorija_termini_vrijeme_id', $all)->pluck('id')->toArray();

        ProstorijeGallery::where('prostorija_id', $id)->delete();
        TerminiRezervirani::whereIn('termini_kalendar_id', $all2)->delete();
        TerminiKalendar::whereIn('prostorija_termini_vrijeme_id', $all)->delete();
        TerminiVrijemeProstorijeVeza::where('prostorija_id', $id)->delete();

        $prostorija->delete();


        return redirect()->route('prostorije');
    }


    public function dropzone(Request $request)
    {
            $image = $request->file('file');
            $imageName = time() . rand(1, 1000) . $image->getClientOriginalName();
            $image->move(public_path('uploads/prostorije'),$imageName);

            //$new = new ProstorijeGallery;
            //$new->url = $imageName;
            //$new->save();

            //$imageUpload = new ImageUpload();
            //$imageUpload->filename = $imageName;
            //$imageUpload->save();
            return response()->json(['success'=>$imageName]);
    }


}
