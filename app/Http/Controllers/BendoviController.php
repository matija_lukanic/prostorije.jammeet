<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\TerminiRezervirani;
use Auth;

class BendoviController extends Controller
{

   public function __construct()
   {
       $this->middleware('auth');
   }

    public function index(Request $request) {

      $user= Auth::user();

      //$bendovi = TerminiRezervirani::get();

      $jammeetUsers2 = file_get_contents("https://jammeet.com/api/users/all?pass=wbrCf}tq7T@^NSw4~ZZu%SrmV.:nx27G83a[g[Jp~67LnJbjjcd,PZ4");
      //id, name, profil_slika, grad

      $rezerviraniTermini = $user->getAllRezerviraniTerminiIds();
      $bendovi2 = TerminiRezervirani::whereIn('id', $rezerviraniTermini)->groupBy('user_id')->pluck('user_id')->toArray();

      $bendovi = [];
      foreach (json_decode($jammeetUsers2, true) as $user2) {

        if(in_array($user2['id'], $bendovi2)){

          $bendovi[$user2['id']] = $user2;
        }
      }


      return view('bendovi-lista', compact('user', 'bendovi'));

    }
}
