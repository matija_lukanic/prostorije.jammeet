<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\Mjesto;
use App\TerminiRezervirani;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\UserMjesta as UserMjestaResource;
use Auth;
use File;
use Illuminate\Support\Facades\Response;

class ApiUsersController extends Controller
{
    //

    public function get(Request $request) {

        /*
        pass=wbrCf}tq7T@^NSw4~ZZu%SrmV.:nx27G83a[g[Jp~67LnJbjjcd,PZ4
        user_id=2
        mjesto_id=2
        datum_od=2020-09-14
        datum_do=2020-10-12
        vrijeme_od=15:00:00
        vrijeme_do=17:00:00
        slug=prostorija

        [
          {"id":2,"name":"Matija Lukani\u0107","opis":null,"email":"mlukanic123@gmail.com",
            "facebook":null,"mobitel":null,"profil_slika":"default.jpg","mjesto":"Zagreb",
            "slobodniTermini":{
              "3":{"prostorija_naziv":"Prostorija1", "prostorija_opis":null,"prostorija_profil":"default.jpg", "termini":[]},
              "4":{"prostorija_naziv":"Prostorija2", "prostorija_opis":null,"prostorija_profil":"default.jpg",
                  "termini":{"2020-11-01":[{"id":2573,"title":"13:00 : 15:00"}],
                             "2020-11-02":[{"id":2498,"title":"13:00 : 15:00"}]
                            }

            }
          }
        */

        if($request->pass == 'wbrCf}tq7T@^NSw4~ZZu%SrmV.:nx27G83a[g[Jp~67LnJbjjcd,PZ4')
        {

            if(isset($request->slug) && !isset($request->user_id))
            {
                $user_id = User::where('slug', $request->slug)->first();
                $request->user_id = $user_id->id;
            }

            $page = isset($request->page) ? $request->page * 9 : 0;

            if(isset($request->user_id) && isset($request->mjesto_id))
            {
                $users = User::where('id', $request->user_id)->where('id_mjesto', $request->mjesto_id)->orderBy('name')->offset($page)->limit(9)->get();
            }
            else if (isset($request->user_id)) {
                $users = User::where('id', $request->user_id)->orderBy('name')->offset($page)->limit(9)->get();
            }
            else if (isset($request->mjesto_id)) {
                $users = User::where('id_mjesto', $request->mjesto_id)->orderBy('name')->offset($page)->limit(9)->get();
            }
            else{
                $users = User::orderBy('name')->orderBy('name')->offset($page)->limit(9)->get();
            }

            //$users = isset($request->user_id) ? User::where('id', $request->user_id)->get() : User::orderBy('name')->get();

            return UserResource::collection($users);
        }

    }


    public function gettotalusers(Request $request) {

        /*
        pass=wbrCf}tq7T@^NSw4~ZZu%SrmV.:nx27G83a[g[Jp~67LnJbjjcd,PZ4
        mjesto_id=2

        */

        if($request->pass == 'wbrCf}tq7T@^NSw4~ZZu%SrmV.:nx27G83a[g[Jp~67LnJbjjcd,PZ4')
        {
            if (isset($request->mjesto_id)) {
                $users = User::where('id_mjesto', $request->mjesto_id)->get();
            }
            else{
                $users = User::orderBy('name')->get();
            }

            //$users = isset($request->user_id) ? User::where('id', $request->user_id)->get() : User::orderBy('name')->get();

            return response()->json( count($users) );
        }

    }

    public function getmjesta(Request $request) {

          if($request->pass == 'wbrCf}tq7T@^NSw4~ZZu%SrmV.:nx27G83a[g[Jp~67LnJbjjcd,PZ4')
          {

            $mjesta = User::pluck('id_mjesto');
            $mjesta = Mjesto::whereIn('id', $mjesta)->get();
            //$users = isset($request->user_id) ? User::where('id', $request->user_id)->get() : User::orderBy('name')->get();

            return UserMjestaResource::collection($mjesta);
          }
    }


    public function createproba(Request $request) {

        if($request->pass == 'wbrCf}tq7T@^NSw4~ZZu%SrmV.:nx27G83a[g[Jp~67LnJbjjcd,PZ4')
        {

            $check = TerminiRezervirani::checkIfUserCanReserveProba($request->prostorije_user_id, $request->jammeet_user_id);

            if($check['canAddProba'])
            {

                $new = new TerminiRezervirani;
                $new->termini_kalendar_id  = $request->termini_kalendar_id ;
                $new->user_id = $request->jammeet_user_id;
                $new->created_at = Carbon::now('Europe/Zagreb');
                $new->updated_at = $new->created_at;
                $new->save();

                $message = 'Došlo je do greške prilikom rezerviranja probe.';
                $alert = 'danger';
                if($new)
                {
                    $message = 'Proba je rezervirana!';
                    $alert = 'success';

                }

                return response()->json(['message'=>$message, 'alertType' => $alert]);
            }
            else{

                return response()->json(['message'=>$check['message'], 'alertType' => $check['alertType']]);
            }


        }
    }

    public function profileimg (Request $request){

        File::delete('uploads/prostorije/' . $request->input('img'));
        $user = User::find($request->input('user_id'));
        $user->profil_slika = 'default.jpg';
        $user->save();

    }

}
