<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Zupanija;
use App\TerminiProstorije;
use App\TerminiVrijemeProstorijeVeza;
use App\TerminiKalendar;
use App\TerminiRezervirani;
use App\ProstorijeGallery;
use Auth;

class PostavkeController extends Controller
{
    //
    public function profil(Request $request)
    {
      $user= Auth::user();

        if ($request->isMethod('post')) {

            $validatedData = $request->validate([
                'name' => 'required',
                'id_mjesto' => 'required',
            ]);
            $user->update($request->all());

        }

        $maticno_podrucja_optgroup = Zupanija::optgroup_list();


      return view('postavke/profil', compact('user', 'maticno_podrucja_optgroup'));
    }

    public function probe(Request $request)
    {
        $user= Auth::user();

        if ($request->isMethod('post')) {

            $user->update($request->all());

        }


        return view('postavke/probe', compact('user'));
    }

     public function __construct()
     {
         $this->middleware('auth');
         //$this->middleware('auth')->only(['uredi']);
     }



    public function dropzone(Request $request)
    {
            $image = $request->file('file');
            $imageName = 'profil' . time() . rand(1, 1000) . $image->getClientOriginalName();
            $image->move(public_path('uploads/prostorije'),$imageName);

            //$new = new ProstorijeGallery;
            //$new->url = $imageName;
            //$new->save();

            //$imageUpload = new ImageUpload();
            //$imageUpload->filename = $imageName;
            //$imageUpload->save();
            return response()->json(['success'=>$imageName]);
    }


}
