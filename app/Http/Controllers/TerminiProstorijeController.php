<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TerminiProstorije;
use App\TerminiKalendar;
use App\TerminiVrijemeProstorijeVeza;
use App\Http\Resources\TerminiProstorije as TerminiProstorijeResource;
use App\Http\Resources\TerminiKalendar as TerminiKalendarResource;
use Auth;
use Carbon\Carbon;

class TerminiProstorijeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

   public function show(Request $request)
   {
       //$prostorija = TerminiProstorije::find($id);
       if($request->input('id') == 'all')
       {
           $prostorije = TerminiProstorije::where('user_id', $request->input('user_id'))->get();
       }
       else
       {
           $prostorije = TerminiProstorije::where('id', $request->input('id'))->get();
       }


       return TerminiProstorijeResource::collection($prostorije);
   }


   public function kalendar(Request $request)
   {
       //$prostorija = TerminiProstorije::find($id);

       $dates = $request->input('allDatesInMonth');

       $firstDate = Carbon::createFromFormat('D M d Y H:i:s e+', $dates[0]);
       $lastDate = Carbon::createFromFormat('D M d Y H:i:s e+', $dates[ count($dates) - 1 ]);

       //$date = TerminiKalendar::where('datum', '>=', $date->toDateString())->get();

       if($request->input('id') == 'all')
       {
           $prostorije_id = TerminiProstorije::where('user_id', $request->input('user_id'))->pluck('id');
       }
       else
       {
           $prostorije_id = [$request->input('id')];
       }


       $vp = TerminiVrijemeProstorijeVeza::whereIn('prostorija_id', $prostorije_id)->pluck('id');
       $kalendar = TerminiKalendar::where('datum', '>=', $firstDate->toDateString())->where('datum', '<=', $lastDate->toDateString())->whereIn('prostorija_termini_vrijeme_id', $vp)->get();

       return TerminiKalendarResource::collection($kalendar);
   }

    public function kalendarday(Request $request)
    {
        //$prostorija = TerminiProstorije::find($id);

        $date = $request->input('date');

        $firstDate = Carbon::createFromFormat('D M d Y H:i:s e+', $date);

        //$date = TerminiKalendar::where('datum', '>=', $date->toDateString())->get();

        if($request->input('id') == 'all')
        {
            $prostorije_id = TerminiProstorije::where('user_id', $request->input('user_id'))->pluck('id');
        }
        else
        {
            $prostorije_id = [$request->input('id')];
        }


        $vp = TerminiVrijemeProstorijeVeza::whereIn('prostorija_id', $prostorije_id)->pluck('id');
        $kalendar = TerminiKalendar::where('datum', '>=', $firstDate->toDateString())->where('datum', '<=', $firstDate->toDateString())->whereIn('prostorija_termini_vrijeme_id', $vp)->get();

        return TerminiKalendarResource::collection($kalendar);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
