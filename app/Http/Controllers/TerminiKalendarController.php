<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\TerminiKalendar as TerminiKalendarResource;
use App\Http\Resources\TerminiRezervirani as TerminiRezerviraniResource;
use App\TerminiKalendar;
use App\TerminiUsers;
use App\TerminiVrijeme;
use App\TerminiVrijemeProstorijeVeza;
use App\TerminiProstorije;
use App\TerminiRezervirani;
use Carbon\Carbon;
use Auth;

class TerminiKalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $termini = TerminiKalendar::all();
        return TerminiKalendarResource::collection($termini);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //ovdje mora ic mogucnost multiple ubacivanja
      //ubacuje se jedan termin ali se taj jedan termin moze ubacivat za sve prostorije ili pojedinacno

        //$vrijeme = TerminiVrijeme::find($request->id);
        $vrijeme_id = $request->id;


        if($request->prostorija_id == 'all')
        {
            $prostorije_arr = TerminiProstorije::where('user_id', $request->user_id)->pluck('id');
        }
        else
        {
            $prostorije_arr = [$request->prostorija_id];
        }

        //ovo nam treba za termini_kalendar tablicu
        $vrijemeProstorijeArr = [];

        //provjerit ako postoji prostorija sa tim vremenom, ako ne, dodaj to vrijeme za prostoriju
        foreach ($prostorije_arr as $id) {

            $pr = TerminiVrijemeProstorijeVeza::where('prostorija_id', $id)->where('termin_id', $vrijeme_id)->first();

            if($pr == NULL)
            {
                //kombinacija termina i prostora ne postoji; dodaj novi
                $new = new TerminiVrijemeProstorijeVeza;
                $new->prostorija_id = $id;
                $new->termin_id = $vrijeme_id;
                $new->created_at = Carbon::now('Europe/Zagreb');
                $new->updated_at = $new->created_at;
                $new->save();
                $vrijemeProstorijeArr[] = $new->id;
            }
            else {
              $vrijemeProstorijeArr[] = $pr->id;
            }
        }

        //$all = TerminiVrijemeProstorijeVeza::whereIn('prostorija_id', $prostorije_arr)->where('termin_id', $vrijeme->id)->pluck('id');

        $resources = [];
        foreach ($vrijemeProstorijeArr as $id) {

            $terminKalendar = TerminiKalendar::where('prostorija_termini_vrijeme_id', $id)->where('datum', $request->datum)->first();

            if($terminKalendar == NULL)
            {

                $terminKalendar = new TerminiKalendar;
                $terminKalendar->datum = $request->input('datum');
                $terminKalendar->mjesto_id = $request->input('mjesto_id');
                $terminKalendar->created_at = Carbon::now('Europe/Zagreb');
                $terminKalendar->updated_at = $terminKalendar->created_at;
                $terminKalendar->prostorija_termini_vrijeme_id = $id;
                $terminKalendar->save();

            }

            $resources[] = new TerminiKalendarResource($terminKalendar);

        }


        return $resources;

    }

    public function storemultiple(Request $request)
    {
        $resources=[];
        foreach ($request->ids as $vrijeme_id)
        {

            //$vrijeme = TerminiVrijeme::find($vrijeme_id);

            if($request->prostorija_id == 'all')
            {
                $prostorije_arr = TerminiProstorije::where('user_id', $request->user_id)->pluck('id');
            }
            else
            {
                $prostorije_arr = [$request->prostorija_id];
            }

            //ovo nam treba za termini_kalendar tablicu
            $vrijemeProstorijeArr = [];

            //provjerit ako postoji prostorija sa tim vremenom, ako ne, dodaj to vrijeme za prostoriju
            foreach ($prostorije_arr as $id) {

                $pr = TerminiVrijemeProstorijeVeza::where('prostorija_id', $id)->where('termin_id', $vrijeme_id)->first();

                if($pr == NULL)
                {
                    //kombinacija termina i prostora ne postoji; dodaj novi
                    $new = new TerminiVrijemeProstorijeVeza;
                    $new->prostorija_id = $id;
                    $new->termin_id = $vrijeme_id;
                    $new->created_at = Carbon::now('Europe/Zagreb');
                    $new->updated_at = $new->created_at;
                    $new->save();
                    $vrijemeProstorijeArr[] = $new->id;
                }
                else {
                  $vrijemeProstorijeArr[] = $pr->id;
                }
            }

            //$all = TerminiVrijemeProstorijeVeza::whereIn('prostorija_id', $prostorije_arr)->where('termin_id', $vrijeme->id)->pluck('id');
            $startDate = Carbon::createFromFormat('Y-m-d', $request->date_start);
            $endDate = Carbon::createFromFormat('Y-m-d', $request->date_end);

            foreach ($request->input('dates') as $date) {

              $cDate =  Carbon::createFromFormat('Y-m-d', $date);

              if( ($cDate->between($startDate,$endDate) || $date == $endDate) && $cDate >= Carbon::now()->toDateString() )
              {
                //dodaj datum u kalendar za koji user ima placeno
                //or sam dodao jer ponekad radi bez njea ponekad ne

                foreach ($vrijemeProstorijeArr as $id) {

                    $terminKalendar = TerminiKalendar::where('prostorija_termini_vrijeme_id', $id)->where('datum', $date)->first();

                    if($terminKalendar == NULL)
                    {

                        $terminKalendar = new TerminiKalendar;
                        $terminKalendar->datum = $date;
                        $terminKalendar->mjesto_id = $request->input('mjesto_id');
                        $terminKalendar->created_at = Carbon::now('Europe/Zagreb');
                        $terminKalendar->updated_at = $terminKalendar->created_at;
                        $terminKalendar->prostorija_termini_vrijeme_id = $id;
                        $terminKalendar->save();

                    }

                    $resources[] = new TerminiKalendarResource($terminKalendar);

                }

              }

            }

        }


                return $resources;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        $termini = TerminiKalendar::get();
        return TerminiKalendarResource::collection($termini);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

   public function prostorijeimajutermin(Request $request)
   {

         $tvpv = TerminiVrijemeProstorijeVeza::find($request->termin_parent_id);
         $vrijemeId = $tvpv->termin_id; // TerminiVrijeme - 15:00 - 17:000
         $prostorije_ids = TerminiProstorije::where('user_id', $request->user_id)->pluck('id');

         $prostorijaImaTermin = [];
         $all = TerminiVrijemeProstorijeVeza::whereIn('prostorija_id', $prostorije_ids)->where('termin_id', $vrijemeId)->get();

         foreach ($all as $one) {
              $terminId = TerminiKalendar::where('prostorija_termini_vrijeme_id', $one->id)->where('datum', $request->startdate . ' 00:00:00')->first();

              if ($terminId)
              {
                if ($terminId->rezervirano)
                {
                 $prostorijaImaTermin[$one->prostorija_id] = new TerminiRezerviraniResource($terminId->rezervirano);
                }
                else {
                 $prostorijaImaTermin[$one->prostorija_id] = '';
                }
              }
         }

         $prostorijaNemaTermin = [];
        foreach ($prostorije_ids as $id) {
          if (!array_key_exists($id, $prostorijaImaTermin)){
              $prostorijaNemaTermin[] = $id;
          }
        }

        return ['prostorijaImaTermin' => $prostorijaImaTermin, 'prostorijaNemaTermin' => $prostorijaNemaTermin];

   }


   public function savetermin(Request $request)
   {

         $tvpv = TerminiVrijemeProstorijeVeza::find($request->termin_parent_id);
         $vrijemeId = $tvpv->termin_id; // TerminiVrijeme - 15:00 - 17:000
        $arr = [];

        foreach ($request->rezs as $rez) {

          $vrijemeProstorId = TerminiVrijemeProstorijeVeza::where('prostorija_id', $rez['prostorija_id'])->where('termin_id', $vrijemeId)->first();

          $terminId = TerminiKalendar::where('prostorija_termini_vrijeme_id', $vrijemeProstorId->id)->where('datum', $request->startdate . ' 00:00:00')->first();

          if( $terminId != null){

            $newRez = TerminiRezervirani::updateOrCreate(
                      [
                        'termini_kalendar_id' => $terminId->id,
                        //'user_id' => $rez['user_id'],
                        //'naziv_benda' => $rez['naziv_benda'],
                      ]
                    );

            if( empty($rez['user_id']) && empty($rez['naziv_benda']) )
            {
                //nije upisan naziv benda ni odabran bend, brisi termin
                $newRez->delete();
            }
            else{
                $newRez->user_id = $rez['user_id'];
                $newRez->naziv_benda = $rez['naziv_benda'];
                $newRez->updated_at = Carbon::now();
                $newRez->save();

                $arr[] = new TerminiRezerviraniResource($newRez);
            }


          }

        }

        return $arr;

   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $terminKalendar = TerminiKalendar::findOrFail($request->input('id'));
        $terminKalendar->datum = $request->input('datum');
        $terminKalendar->updated_at = Carbon::now('Europe/Zagreb');

        if($terminKalendar->save())
        {
            return new TerminiKalendarResource($terminKalendar);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

      //ovo je kad preko modala otvorim termin i odabrem izbriši termini

      $terminKalendar = TerminiKalendar::findOrFail($request->id);

      if($request->type == 'all')
      {
          //brisu se termini za sve prostorije
          $user = $terminKalendar->termin_prostorija_vrijeme->prostorija->user;
          $vrijemeId = $terminKalendar->termin_prostorija_vrijeme->vrijeme->id;

          foreach ($user->prostorije as $key => $prostorija) {
              $tvpv = TerminiVrijemeProstorijeVeza::where('prostorija_id', $prostorija->id)->where('termin_id', $vrijemeId)->first();
              TerminiKalendar::where('prostorija_termini_vrijeme_id', $tvpv->id)->where('datum', $terminKalendar->datum)->delete();
          }
      }
      else
      {
          //brise se termin samo za jednu prostoriju
          if($terminKalendar->delete())
          {
              return new TerminiKalendarResource($terminKalendar);
          }
      }
    }

   public function destroy2(Request $request)
   {

     //ovo je kad je odabrana opcija da se brise termine pa se termini poklikaju i onda se odabere izbrisi termine
     $ids = $request->ids;
     foreach ($ids as $id) {

          $terminKalendar = TerminiKalendar::findOrFail($id);

          if($request->type == 'all')
          {
              //brisu se termini za sve prostorije
              $user = $terminKalendar->termin_prostorija_vrijeme->prostorija->user;
              $vrijemeId = $terminKalendar->termin_prostorija_vrijeme->vrijeme->id;

              foreach ($user->prostorije as $key => $prostorija) {
                  $tvpv = TerminiVrijemeProstorijeVeza::where('prostorija_id', $prostorija->id)->where('termin_id', $vrijemeId)->first();
                  if($tvpv != null)
                  {

                    TerminiKalendar::where('prostorija_termini_vrijeme_id', $tvpv->id)->where('datum', $terminKalendar->datum)->delete();
                  }
              }
          }
          else
          {
              //brise se termin samo za jednu prostoriju
              if($terminKalendar->delete())
              {
                  //return new TerminiKalendarResource($terminKalendar);
              }
          }

     }

     return response()->json($ids);

   }

    public function destroymultiple(Request $request)
    {
          //ovo je kad preko Uredi Termine modala otvorim i onda odabrem neki termin da se izbrise

        $vrijeme = TerminiVrijeme::find($request->id);

        if($request->prostorija_id == 'all')
        {
            $prostorije_arr = TerminiProstorije::where('user_id', $request->user_id)->pluck('id');
        }
        else
        {
            $prostorije_arr = [$request->prostorija_id];
        }

      $termini = TerminiVrijemeProstorijeVeza::whereIn('prostorija_id', $prostorije_arr)->where('termin_id', $vrijeme->id)->pluck('id');

      TerminiVrijemeProstorijeVeza::whereIn('prostorija_id', $prostorije_arr)->where('termin_id', $vrijeme->id)->delete();
      $termini2 = TerminiKalendar::whereIn('prostorija_termini_vrijeme_id', $termini)->pluck('id');
      TerminiKalendar::whereIn('id', $termini)->delete();

      return response()->json($termini2);
    }
}
