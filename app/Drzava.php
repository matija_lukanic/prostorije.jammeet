<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Drzava extends Model
{
    public $table = 'drzava';

    public function zupanija() {
        return $this->hasMany("App\Zupanija", "id_drzava");

      }


}
