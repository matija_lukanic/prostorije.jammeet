<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Zupanija extends Model
{
    public $table = 'zupanija';

     public static function optgroup_list(){

  	/*
  	   [
  	    'Primorsko-goranska županija' => ['1' => 'Klana', '2' =>Rijeka ...],
  	    'Bjelovarsko-bilogorska' => ['50' => 'Bjelovar' ...],
  	    ]
  	*/

  	$arr=[];
        foreach (Zupanija::get() as $key) {

        $arr2=[];

        $k = $key->mjesto->keyBy('id')->pluck('naziv', 'id')->toArray();
        $arr2 = $arr2 + $k;

        $arr[$key->naziv.' ('.$key->drzava->short.')']=$arr2;
            //die(var_dump($arr2));

        }

        return $arr;

    }

    public function mjesto() {
        return $this->hasMany("App\Mjesto", "id_zupanija");

      }
      public function drzava() {
          return $this->belongsTo("App\Drzava", "id_drzava");

      }


}
