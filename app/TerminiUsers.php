<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TerminiUsers extends Model
{
    public $table = 'termini_users';

    /*
    public function maticno_podrucje() {
    	return $this->belongsTo("App\Maticno_podrucje", "mjesto_id");

    }
    */


    public function prostorije() {
        return $this->hasMany("App\TerminiProstorije", "user_id");

    }


}
