<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TerminiVrijeme extends Model
{
    public $table = 'termini_vrijeme';
 
    /*
    public function maticno_podrucje() {
    	return $this->belongsTo("App\Maticno_podrucje", "mjesto_id");
    	
    }
    */

    public function termini() {
        return $this->hasMany("App\TerminiVrijemeProstorijeVeza");
    	
    }

       


}
