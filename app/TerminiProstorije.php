<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\TerminiVrijemeProstorijeVeza;
use App\TerminiKalendar;
use App\TerminiRezervirani;
use App\ProstorijeGallery;
use Auth;
use Illuminate\Support\Facades\File;
use App\Http\Resources\TerminiKalendarForJammeet as KalendarResource;


class TerminiProstorije extends Model
{
    public $table = 'termini_prostorije';
    protected $appends = ['totalProbaHad', 'totalProbaDogovorenih'];


    public function getTotalProbaHadAttribute()
   {
      $all = TerminiVrijemeProstorijeVeza::where('prostorija_id', $this->id)->pluck('id');
      $all = TerminiKalendar::whereIn('prostorija_termini_vrijeme_id', $all)->where('datum', '<', Carbon::now()->format('Y-m-d'))->pluck('id');
      $all = TerminiRezervirani::whereIn('termini_kalendar_id', $all)->count();
       return $all;
   }
   public function getTotalProbaDogovorenihAttribute()
  {
     $all = TerminiVrijemeProstorijeVeza::where('prostorija_id', $this->id)->pluck('id');
     $all = TerminiKalendar::whereIn('prostorija_termini_vrijeme_id', $all)->where('datum', '>=', Carbon::now()->format('Y-m-d'))->pluck('id');
     $all = TerminiRezervirani::whereIn('termini_kalendar_id', $all)->count();
      return $all;
  }

    /*
    public function maticno_podrucje() {
    	return $this->belongsTo("App\Maticno_podrucje", "mjesto_id");

    }
    */

    public function user() {
        return $this->belongsTo("App\User", "user_id");

    }


    public function termini() {
        return $this->hasMany("App\TerminiVrijemeProstorijeVeza", "prostorija_id");

    }
    public function gallery() {
        return $this->hasMany("App\ProstorijeGallery", "prostorija_id");

    }


    public function getSlobodniTermini($request) {
        //izvuci sve slobodne termine od pojedine prostorije

        $terminiProstorije = [];
        $terminiProstorije2 = [];
        $terminiProstorije3 = [];

        foreach ($this->termini as $prostorijaTermin) {
          $terminiProstorije[] = $prostorijaTermin->getSlobodniTermini($request);
        }

        foreach ($terminiProstorije as $prostorijaTermin2) {

            $datum = null;
            foreach ($prostorijaTermin2 as $datum => $prostorijaTermin) {

                $key = substr($prostorijaTermin->termin_prostorija_vrijeme->vrijeme->pocetak, 0, -3) . ' : ' . substr($prostorijaTermin->termin_prostorija_vrijeme->vrijeme->kraj, 0, -3);

                $terminiProstorije2[$datum][$key] = new KalendarResource($prostorijaTermin);
            }

        }

        //za posloit da termini unutar datuma budeu poredani po satu (13:00-15:00 pa 15:00-17:00 itd)
        foreach ($terminiProstorije2 as $datum => $termini) {
            //dd($datum);
            ksort($termini);
            $terminiProstorije3[$datum] = $termini;
        }

        //za poslozit po datumu - 1.12.20, 2.12.20, 4.12.20...
        ksort($terminiProstorije3);

        return $terminiProstorije3;

    }

    public function vrijemeOrdered($value='')
    {
        //sve termine od prostora poredaj po vremenu
        // 13-15, 15-17, 19-21...
        $termini_ordered = [];

        foreach ($this->termini as $key => $termin)
        {
          $p = substr($termin->vrijeme->pocetak, 0, -3);
          $k = substr($termin->vrijeme->kraj, 0, -3);

          $termini_ordered[$p.$k] = $termin;

        }

        ksort($termini_ordered);
        return $termini_ordered;
    }


    public static function bendUkupnoProba($prostorije_user_id, $bend_id) {

        //$termini = $this->termini->pluck('id');
        $prostorije = TerminiProstorije::where('user_id', $prostorije_user_id)->pluck('id');
        $termini = TerminiVrijemeProstorijeVeza::whereIn('prostorija_id', $prostorije)->pluck('id');
        $termini_prije = TerminiKalendar::whereIn('prostorija_termini_vrijeme_id', $termini)
                                    ->where('datum', '<=' , Carbon::today()->toDateString())
                                    ->pluck('id');

        $termini_poslije = TerminiKalendar::whereIn('prostorija_termini_vrijeme_id', $termini)
                                    ->where('datum', '>' , Carbon::today()->toDateString())
                                    ->pluck('id');


        $arr['prije'] = TerminiRezervirani::whereIn('termini_kalendar_id', $termini_prije)
                                                ->where('user_id', $bend_id)
                                                ->count();

        $arr['poslije'] = TerminiRezervirani::whereIn('termini_kalendar_id', $termini_poslije)
                                                    ->where('user_id', $bend_id)->count();
        return $arr;

    }

    public function getProfil()
    {
      $slika = ProstorijeGallery::where('prostorija_id', $this->id)->where('profile', 1)->first();
      return $slika == null ? 'default.jpg' : $slika->url;
    }




}
