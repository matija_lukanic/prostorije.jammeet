<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TerminiKalendar extends Model
{
    public $table = 'termini_kalendar';
 
    public function mjesto() {
        return $this->belongsTo("App\Maticni_ured_grad", "mjesto_id");
    }

    public function termin_prostorija_vrijeme() {
        return $this->belongsTo("App\TerminiVrijemeProstorijeVeza", "prostorija_termini_vrijeme_id");
    }

    public function rezervirano() {
        return $this->hasOne("App\TerminiRezervirani");
    }

    


}
