<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('kalendar', '\App\Http\Controllers\KalendarController@index');
Route::post('kalendar', '\App\Http\Controllers\KalendarController@index');
Route::get('kalendar/{id}', '\App\Http\Controllers\KalendarController@pojedini_prostor');
Route::post('kalendar/{id}', '\App\Http\Controllers\KalendarController@pojedini_prostor');

Route::post('kalendar', '\App\Http\Controllers\KalendarController@index');
Route::get('bendovi', '\App\Http\Controllers\BendoviController@index');
Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/dashboard', 'HomeController@dashboard')->name('home');

Route::get('/prostorije', 'ProstorijeController@index')->name('prostorije');

Route::post('/prostorije/dropzone', 'ProstorijeController@dropzone');
Route::get('/prostorije/novo', 'ProstorijeController@create');
Route::post('/prostorije/novo', 'ProstorijeController@create');
Route::get('/prostorije/{id}', 'ProstorijeController@uredi')->name('uredi-prostoriju');
Route::post('/prostorije/{id}', 'ProstorijeController@posturedi');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::post('/prostorije/delete/{id}', 'ProstorijeController@delete');


Route::get('/postavke/profil', 'PostavkeController@profil')->name('postavke-profil');
Route::post('/postavke/profil', 'PostavkeController@profil')->name('postavke-profil');
Route::get('/postavke/probe', 'PostavkeController@probe')->name('postavke-probe');
Route::post('/postavke/probe', 'PostavkeController@probe')->name('postavke-probe');
Route::post('/postavke/dropzone', 'PostavkeController@dropzone');
