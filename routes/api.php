<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('termini-kalendars', 'TerminiKalendarController@index');
Route::get('termini-kalendar/{user_id}', 'TerminiKalendarController@show');
Route::post('termini-kalendar', 'TerminiKalendarController@store');
Route::post('termini-kalendar/save-termin', 'TerminiKalendarController@savetermin');
Route::post('termini-kalendar/prostorije-imaju-termin', 'TerminiKalendarController@prostorijeimajutermin');
Route::put('termini-kalendar/update', 'TerminiKalendarController@update');
Route::post('termini-kalendar-multiple', 'TerminiKalendarController@storemultiple');
Route::delete('termini-kalendar', 'TerminiKalendarController@destroy');
Route::delete('termini-kalendar2', 'TerminiKalendarController@destroy2');
Route::delete('termini-kalendar-multiple', 'TerminiKalendarController@destroymultiple');


Route::post('termini-vrijeme-prostorije-veza', 'TerminiVrijemeProstorijeVezaController@store');
Route::get('termini-vrijeme-prostorije-veza/prostorija/{prostorija_id}', 'TerminiVrijemeProstorijeVezaController@show');


Route::post('termini-prostorije', 'TerminiProstorijeController@show');
Route::post('termini-prostorije/kalendar', 'TerminiProstorijeController@kalendar');
Route::post('termini-prostorije/kalendar/day', 'TerminiProstorijeController@kalendarday');

Route::delete('profil-slika', 'ApiProstorijeController@delete');
Route::put('profil-slika', 'ApiProstorijeController@update');

Route::get('users/all', 'ApiUsersController@get');
Route::get('users/mjesta', 'ApiUsersController@getmjesta');
Route::get('users/gettotalusers', 'ApiUsersController@gettotalusers');

Route::post('users/create/proba', 'ApiUsersController@createproba');
Route::delete('users/profileimg', 'ApiUsersController@profileimg');

Route::post('bendovi/get', 'ApiBendoviController@get');
Route::post('bendovi/save', 'ApiBendoviController@save');
