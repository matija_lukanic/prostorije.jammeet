<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DodajUseruAtribute extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
                $table->string('slug', 255)->nullable();
                $table->string('facebook', 200)->nullable();
                $table->string('youtube', 200)->nullable();
                $table->string('instagram', 200)->nullable();
                $table->string('mobitel', 200)->nullable();
                $table->string('profil_slika', 255)->default('default.jpg');
                $table->text('opis')->nullable();
                $table->integer('id_mjesto')->unsigned()->default(1);
                $table->foreign('id_mjesto')->references('id')->on('mjesto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
