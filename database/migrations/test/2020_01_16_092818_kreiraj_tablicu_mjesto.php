<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KreirajTablicuMjesto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mjesto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('naziv', 200);
            $table->integer('id_zupanija')->unsigned();
            $table->foreign('id_zupanija')->references('id')->on('zupanija');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mjesto');
    }
}
