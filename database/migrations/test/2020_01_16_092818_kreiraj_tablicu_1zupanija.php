<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KreirajTablicu1zupanija extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zupanija', function (Blueprint $table) {
            $table->increments('id');
            $table->string('naziv', 100);
            $table->integer('id_drzava')->unsigned();
            $table->foreign('id_drzava')->references('id')->on('drzava');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mjesto');
    }
}
