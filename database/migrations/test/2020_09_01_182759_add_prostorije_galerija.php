<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProstorijeGalerija extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prostorije_gallery', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url', 255)->nullable();
            $table->integer('prostorija_id')->unsigned();
            $table->foreign('prostorija_id')->references('id')->on('termini_prostorije');
            $table->tinyInteger('profile')->nullable()->default(0);
            $table->timestamps();
        });
        Schema::table('termini_prostorije', function($table) {
           $table->dropColumn('profil_slika');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
