@extends('layouts.admin')

@section('extra_scripts')

<link href="{{ URL::asset('calendar/packages/core/main.css') }}" rel='stylesheet' />
<link href="{{ URL::asset('calendar/packages/timegrid/main.css') }}" rel='stylesheet' />
<link href="{{ URL::asset('calendar/packages/daygrid/main.css') }}" rel='stylesheet' />
<link href="{{ URL::asset('calendar/packages/list/main.css') }}" rel='stylesheet' />
<script src="{{ URL::asset('calendar/packages/core/main.js') }}"></script>
<script src="{{ URL::asset('calendar/packages/interaction/main.js') }}"></script>
<script src="{{ URL::asset('calendar/packages/moment/main.js') }}"></script>
<script src="{{ URL::asset('calendar/packages/moment-timezone/main.js') }}"></script>
<script src="{{ URL::asset('calendar/packages/rrule/main.js') }}"></script>
<script src="{{ URL::asset('calendar/packages/bootstrap/main.js') }}"></script>
<script src="{{ URL::asset('calendar/packages/daygrid/main.js') }}"></script>
<script src="{{ URL::asset('calendar/packages/timegrid/main.js') }}"></script>
<script src="{{ URL::asset('calendar/packages/list/main.js') }}"></script>



<script>

  //var active_user =  echo json_encode($prostor) ?>

  document.addEventListener('DOMContentLoaded', function() {
    var Calendar = FullCalendar.Calendar;
    var Draggable = FullCalendarInteraction.Draggable


    /* initialize the external events
    -----------------------------------------------------------------*/

    var containerEl = document.getElementById('external-events-list');
    new Draggable(containerEl, {
      itemSelector: '.fc-event'
    });


  });

  document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var events = <?php echo $event_array ?>;
        var jammeetUsers = <?php echo $jammeetUsers ?>;
        var user_prostorije = <?php echo count($user->prostorije) ?>;

        var jammeetUsersIndexed = [];

        $.each(jammeetUsers, function(key, i) {
          jammeetUsersIndexed[i.id] = i;
        })

        var minh = 0;
        for(var i = 1; i<=user_prostorije; i++){
            minh+=54;
        }
        minh+=64;
        $('.prostorije-container').css('min-height', minh+'px')

        function createInfoDiv(text, aClass)
        {
          $('.info-alert-div').alert('close');
          var div =
          '<div class="info-alert-div alert alert-'+aClass+' alert-dismissible fade show position-fixed" role="alert">'+
            '<p class="mb-0">'+text+'</p>'+
            '<button type="button" class="close pt-2" data-dismiss="alert" aria-label="Close">'+
              '<span class="mb-1" aria-hidden="true">&times;</span>'+
            '</button>'+
          '</div>';

          $('body').append(div);

          setTimeout(function(){ $('.info-alert-div').alert('close'); }, 5000);

        }

        function showOverlay(){
          $('#loading-overlay').css('display', 'block');
        }
        function hideOverlay(){
          $('#loading-overlay').css('display', 'none');
        }
        function showCalendarLoader(){
          $('.calendar-loader').removeClass('d-none');
        }
        function hideCalendarLoader(){
          $('.calendar-loader').addClass('d-none');
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
          });


        //console.log(events);

        var calendar = new FullCalendar.Calendar(calendarEl,
        {
          plugins: [ 'dayGrid' , 'timeGrid', 'list' , 'interaction', 'bootstrap', 'rrule', 'moment', 'momentTimezone'],
          header: {
            left: $(window).width() < 1200 ? 'title':'prev,next,today',
            center: $(window).width() < 1200 ? '':'title',
            right: $(window).width() < 1200 ? 'prev,next':'dayGridMonth,listWeek,prev,next',
            //right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
          },
          buttonText: {
            today:    'danas',
            month:    'mjesec',
            list:     'lista',
          },
          //defaultView: 'listWeek', //dayGridMonth
          defaultView: $(window).width() < 1200 ? 'listWeek':'dayGridMonth', //dayGridMonth
          height: 'auto',
          events: events,
          //editable: true, //ako zelim da elemnti unutar kalendaru budu draggagle
          droppable: true, // this allows things to be dropped onto the calendar
          eventReceive: function(argument) {

          },
          drop: function(arg) {


            if($('i[data-parent_id="'+arg.draggedEl.attributes.name.value+'"][data-startdate="'+arg.dateStr+'"]').length==0)
            {
              showOverlay();

              var prostorija_id = $('.prostorija-tab.active').data('id');


              $.ajax({
                url: '../../api/termini-kalendar',
                method: 'post',
                data: {
                  id: arg.draggedEl.attributes.name.value,
                  datum: arg.dateStr,
                  prostorija_id: prostorija_id,
                  user_id: active_user.id,
                  mjesto_id: 1
                },
                success: function(results){

                  var add = [];
                  var total = results.length;

                  var datePre = '';
                  var datePre2 = '';

                  $.each(results, function(key, date)
                  {
                    datePre2 = calendar.getEventById( date.id );
                    if(datePre2)
                    {
                      datePre = calendar.getEventById( date.id );
                    }
                  })


                  $.each(results, function(key, date)
                  {
                    if(key < 1)
                    {
                      //ako je vise, kad su sve prostorije odabrane onda se taj termin dodat onoliko puta koliko prostorija ima
                      //zato treba sprijecit

                      var activeTab = $('.prostorija-tab.active').data('id');

                      if( datePre != ''  && activeTab == 'all' && datePre.extendedProps.num != '0/' + total)
                      {
                          //ovo je slucaj:
                          //odabrane su sve prostorije, imas npr 3 prostorije, termin postoji za dvije prostorije
                          //dodao si sad taj termin preko dropdown-a, u kalendaru ce i dalje pisat npr 0/2 a trebalo bi bit 0/3, zato refresham kalenadar

                          if(calendar.view.dayTable)
                          {

                            var allDatesInMonth = calendar.view.dayTable.daySeries.dates;
                          }
                          else{

                              var allDatesInMonth = calendar.view.dayDates;
                          }

                          renderTerminiByMonth(activeTab, allDatesInMonth, active_user.id)
                      }

                      date.num = '0/' + total;
                      add.push(date);
                    }
                  });
                  addEventCustom(add);


                    hideOverlay();

                }
              });

            }

            /*

            // is the "remove after drop" checkbox checked?

            vratit ako zelim mogucnost da na drop se izbrise event
            if (document.getElementById('drop-remove').checked) {
              // if so, remove the element from the "Draggable Events" list
              arg.draggedEl.parentNode.removeChild(arg.draggedEl);
            }
            */
          },
          eventDrop: function (arg)
          {

            //ovo je kad drag n drop-as datum koji je vec u kalendaru na drugu poziciju, to zasad ne planiram koristit

            //console.log(arg);
            var dt = arg.event.start;

            var month = dt.getMonth() + 1;
            var day = dt.getDate();

            month < 10 ? month = '0'+month : '';
            day < 10 ? day = '0'+day : '';

            dt = dt.getFullYear() + "-" + month + "-" + day;

            //console.log(arg.oldEvent.extendedProps.termin_id);
            //console.log(dt);

            if($('i[data-parent_id="'+arg.oldEvent.extendedProps.termin_id+'"][data-startdate="'+dt+'"]').length==0)
            {

              $.ajax({
                url: '../../api/termini-kalendar/update',
                method: 'put',
                data: {
                    id: arg.oldEvent.id,
                    datum: dt
                },
                success: function(result){
                }
              });

            }
            else
            {
              arg.oldEvent.remove();
              //ovo je staro, ovdje mora ic array s date-ima
              addEventCustom(arg.oldEvent);
            }


          },
          eventRender: function (arg) {

              //var photo = '<i class="far fa-clock edu-danger-error admin-check-pro" aria-hidden="true"></i>';
              var photo = '<i class="far fa-clock edu-danger-error admin-check-pro" aria-hidden="true"></i>';
              var user_name = '';
              var has_class_left = '';
              var has_class_right = '';
              var jammeet_id = '';

              var activeTab = $('.prostorija-tab.active').data('id');

              if( arg.event.extendedProps.rezervirano != null  && activeTab != 'all'  )
              {
                /*
                ovo ne brisat, ovako nekako bi trebalo funkcionirat kad bude delalo
                photo = '<img class="img-event" src="../../slike_burza_m/avatari/mini-'+arg.event.extendedProps.user.profil_slika+'">';
                user_name = arg.event.extendedProps.user.name + '<br>';
                has_class = 'has-band';
                */

                has_class_right = 'has-band d-flex align-items-center';

                if( arg.event.extendedProps.rezervirano.user_id)
                {

                  has_class_left = 'has-band d-flex align-items-center';

                  if(jammeetUsersIndexed[arg.event.extendedProps.rezervirano.user_id])
                  {
                    //u slucaju da je user_id u bazi kod rezrevacija, a taj id vise ne postoji na jammeet-u (user izbrisao profil),
                    //da ne izbaci error i ne prestane radit stranica
                      jammeet_id = 'data-jammeet-user-id="'+arg.event.extendedProps.rezervirano.user_id+'"';
                      user_name = jammeetUsersIndexed[arg.event.extendedProps.rezervirano.user_id].name + '<br>';
                      photo = '<img class="img-event" src="https://jammeet.com/slike_burza_m/avatari/mini-'+jammeetUsersIndexed[arg.event.extendedProps.rezervirano.user_id].profil_slika+'">';
                  }
                  else{
                      has_class_left = 'custom-green-bg';
                      user_name = '<span class="rez-u">' + arg.event.extendedProps.rezervirano.naziv_benda + '</span><br>';
                  }

                }
                else if( arg.event.extendedProps.rezervirano.naziv_benda )
                {
                      has_class_left = 'custom-green-bg';
                      user_name = '<span class="rez-u">' + arg.event.extendedProps.rezervirano.naziv_benda + '</span><br>';
                }


              }

              var extra = '';
              if(activeTab== 'all' && arg.event.extendedProps.num != null)
              {
                var res = arg.event.extendedProps.num.split("/");

                if(res[0] == res[1])
                {
                  //popunjei su svi termini
                  has_class_left = 'custom-green-bg';
                  has_class_right = 'custom-green-bg';
                }


                extra = '<span class="position-absolute text-availability">' +  arg.event.extendedProps.num + '</span>';
              }


              //$(arg.el).attr('data-target', '#modal-termin');
              //$(arg.el).attr('data-toggle', 'modal');
              $(arg.el).attr('data-parent_id', arg.event.extendedProps.termin_id);
              $(arg.el).attr('data-id', arg.event.id);
              $(arg.el).attr('data-startdate', arg.event.extendedProps.datum_str);
              $(arg.el).attr('data-vrijeme', arg.event.title);

              arg.el.innerHTML =
                  '<div class="div-left '+has_class_left+'">'+
                    photo+
                  '</div>'+
                  '<div class="div-right position-relative '+has_class_right+'" '+jammeet_id+'>'+
                      '<span>' + user_name + arg.event.title + '</span>' +
                      extra +
                  '</div>'
                  //'<i class="delete-event fa fa-times edu-danger-error admin-check-pro" aria-hidden="true" data-parent_id ="'+arg.event.extendedProps.termin_id+'" data-id="'+arg.event.id+'" data-startdate="'+arg.event.extendedProps.datum_str+'"></i>'
                ;


             //console.log(arg);
          },
          selectable: true,
          selectMirror: true,
          locale: 'hr',
          firstDay: 1,
          validRange: {
            end: new Date(active_user.date_end).setDate(new Date(active_user.date_end).getDate())
          },
          datesRender : function (view) {

            var id = $('.prostorija-tab.active').data('id');


            //kelander je veljača ovo je 1. ozujak
            /*
            var firstDayNextMonth = new Date(view.view.currentEnd);

            var userDateEnd = new Date(active_user.date_end);

            if(firstDayNextMonth.getTime() > userDateEnd.getTime())
            {
              //radi se o mjesecu za koji user ima zadnje placeno, treba disable-at forwatrd mjesece
              $('.fc-next-button').attr('disabled', true)
            }
            else{
              $('.fc-next-button').attr('disabled', false)
            }
            */

            if(view.view.dayTable)
            {
                var allDatesInMonth = view.view.dayTable.daySeries.dates;
                renderTerminiByMonth(id, allDatesInMonth, active_user.id)
            }
            else{

                var dates = view.view.dayDates;
                renderTerminiByMonth(id, dates, active_user.id)
            }

          },
          /*
          eventClick: function(event, element) {

              var t = $(event.el);

              var p = $('.delete-event', t);

              event.id = p.data('id');
              console.log(event);

            $(calendarEl).fullCalendar('updateEvent', event);

          },
          */
          select: function(arg) {
            //var title = prompt('Event Title:');
            //var title = '';

            var today = new Date();
            var yesterday = new Date();
            yesterday.setDate(yesterday.getDate() - 1)


            var date2 = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
            today = new Date(date2);

            if( !$('#delete-multiple-termin').hasClass('delete-is-active') &&  arg.start.getTime() >= yesterday.getTime() )
            {
              //ako nije odabrana opcija za brisanje termina i ako je danasnji dan veci od odabranog termina

                $('.btn-add-termin').css('display', 'inline-block');


                var add_new_termin = $('#add-new-termin');

                if(arg.start.getDay()==0)
                {
                  //ako je nedjelja prikazi div prema lijevo
                add_new_termin.css('left', arg.jsEvent.clientX-209);
                }
                else
                {
                add_new_termin.css('left', arg.jsEvent.clientX-5);
                }

                add_new_termin.css('top', arg.jsEvent.clientY+5);


                if(add_new_termin.is(":hidden"))
                {
                  add_new_termin.slideDown('fast');
                }
                else if(add_new_termin.is(":visible") && add_new_termin.data('date') == arg.startStr)
                {
                  add_new_termin.css('display', 'none');
                }


                add_new_termin.data('date', arg.startStr);
                $('.btn-add-termin').data('date', arg.startStr);

            }
            else if (!$('#delete-multiple-termin').hasClass('delete-is-active')){
                createInfoDiv('Ne možete dodati termin za datum u prošlosti.', 'warning')
            }

            //console.log(arg)

            calendar.unselect()

          },
        });


        calendar.render();

        $(document).mouseup(function(e)
        {
            var container = $("fc-content-skeleton");

            // if the target of the click isn't the container nor a descendant of the container
            if (!container.is(e.target) && container.has(e.target).length === 0 && !$("#add-new-termin").is(e.target) && $("#add-new-termin").has(e.target).length === 0)
            {
                $('#add-new-termin').css('display', 'none')
            }
        });


        /*
        if(add_new_termin.is(":hidden"))
        {
          add_new_termin.slideDown('fast');
        }
        else if(add_new_termin.is(":visible") && add_new_termin.data('date') == arg.startStr)
        {
          add_new_termin.css('display', 'none');
        }
        */

        $( window ).scroll(function() {
          $('#add-new-termin').is(":visible") ? $('#add-new-termin').css('display', 'none') : '';
        });

        $( '.fc-scroller' ).scroll(function() {
          $('#add-new-termin').is(":visible") ? $('#add-new-termin').css('display', 'none') : '';
        });

        $('.close-add-new-termin').click(function() {
          $('#add-new-termin').css('display', 'none');
        });

        /*
        function addEventCustom(result){
            calendar.addEvent({
                id: result.id,
                title: result.title,
                start: result.start,
                end: result.end,
                termin_id: result.termin_id,
                datum_str: result.datum_str,
                rezervirano: result.rezervirano,
                num: result.num
            });
        }
        */

      function addEventCustom(result){
          calendar.addEventSource(result);
      }

        function enableDeleteTermini()
        {
          $('#delete-multiple-termin').addClass('delete-is-active')
          $('#delete-multiple-termin').html('<i class="far fa-trash-alt"></i> Odustani od brisanja termina')

          $('#calendar .fc-day-grid-event').addClass('delete-active')
          $('.delete-termini-display').removeClass('d-none')

          $('#open-uredi-termine').attr('disabled', true)


        }

        function disableDeleteTermini()
        {
            $('#delete-multiple-termin').removeClass('delete-is-active')
            $('#delete-multiple-termin').html('<i class="far fa-trash-alt"></i> Izbriši termine')

            $('.delete-termini-display').addClass('d-none')

            $('#calendar .fc-day-grid-event').removeClass('delete-active')
            $('#calendar .fc-day-grid-event').removeClass('for-delete')
            $('#calendar .fc-day-grid-event .admin-check-pro').addClass('fa-clock')
            $('#calendar .fc-day-grid-event .admin-check-pro').removeClass('fa-trash-alt')
            $('#calendar .fc-day-grid-event .admin-check-pro').removeClass('fa-pencil-alt')

            $('#open-uredi-termine').attr('disabled', false)
            $('#delete-selected-termini').attr('disabled', true)
        }

        function checkToEnableDeleteTerminiBtn()
        {
          if( $('#calendar .fc-day-grid-event.for-delete').length )
          {
              $('#delete-selected-termini').attr('disabled', false)
          }
          else{
              $('#delete-selected-termini').attr('disabled', true)
          }
        }

        $(document).on("click","#delete-multiple-termin",function() {

          if( $(this).hasClass('delete-is-active') )
          {
            disableDeleteTermini()
          }
          else {
            enableDeleteTermini()
          }

        })


        $(document).on("click","#select-all-termini",function() {


            $('#calendar .fc-day-grid-event').addClass('for-delete')
            $('#calendar .admin-check-pro').removeClass('fa-clock')
            $('#calendar .admin-check-pro').addClass('fa-trash-alt')

            checkToEnableDeleteTerminiBtn()

        })

        $(document).on("click","#delete-selected-termini",function() {

            showOverlay();
            var ids = [];
            $('#calendar .fc-day-grid-event.for-delete').each(function(){
              ids.push($(this).data('id'))
            })

            if(ids.length)
            {
              var prostorija_id = $('.prostorija-tab.active').data('id');

              var deleteType = prostorija_id == 'all' ? 'all' : 'one';
              var confirmText = 'Želite izbrisati sve odabrane termine?';

              var r = confirm(confirmText);
              if(r)
              {

                  $.ajax({
                      url: '../../api/termini-kalendar2',
                      method: 'delete',
                      data:  {
                        ids: ids,
                        type: deleteType
                      },
                      success: function(ids){

                        $.each(ids, function(key, id){
                          var event = calendar.getEventById( id );

                          if(event != null)
                          {
                            event.remove();
                          }

                        })

                          hideOverlay();
                          disableDeleteTermini()
                      }
                  });

              }else {

                  hideOverlay();
                  disableDeleteTermini()
              }

            }
            else{
                hideOverlay();
                disableDeleteTermini()
            }

        })



        $(document).on('mouseenter', '.fc-day-grid-event', function(e) {
          if( $(this).hasClass('delete-active') )
          {
              $('.admin-check-pro', $(this)).removeClass('fa-clock')
              $('.admin-check-pro', $(this)).addClass('fa-trash-alt')
              $('#calendar .fc-day-grid-event .admin-check-pro').addClass('far')
              $('#calendar .fc-day-grid-event .admin-check-pro').removeClass('fas')
          }
          else {
              $('.admin-check-pro', $(this)).toggleClass("fa-clock fa-pencil-alt");
              $('.admin-check-pro', $(this)).toggleClass("far fas");
          }
        });
        $(document).on('mouseleave', '.fc-day-grid-event', function(e) {

              if( $(this).hasClass('delete-active') && !$(this).hasClass('for-delete') )
              {
                  $('.admin-check-pro', $(this)).toggleClass('fa-clock fa-trash-alt')
              }
              else if(!$(this).hasClass('delete-active')) {
                  $('.admin-check-pro', $(this)).toggleClass("fa-clock fa-pencil-alt");
                  $('.admin-check-pro', $(this)).toggleClass("far fas");
              }
        });


        $(document).on("click",".fc-list-item",function() {


            $('#modal-termin').modal('show');

            $('#modal-termin .title').text( createHrDatum( $(this).data('startdate') ) + ' - ' + $(this).data('vrijeme') ) ;

            $('#modal-termin .delete-event').data( 'parent_id', $(this).data('parent_id') ) ;
            $('#modal-termin .delete-event').data( 'id', $(this).data('id') ) ;
            $('#modal-termin .delete-event').data( 'startdate', $(this).data('startdate') ) ;

            $('#modal-termin .input-img').remove()

            if( $('.prostorija-tab.active').data('id') == 'all')
            {

              $('#modal-termin .loader-div').toggleClass('d-none d-flex')

              $.ajax({
                url: '../../api/termini-kalendar/prostorije-imaju-termin',
                method: 'post',
                data: {
                  termin_parent_id: $(this).data('parent_id'),
                  startdate: $(this).data('startdate'),
                  user_id: active_user.id,
                },
                success: function(results){

                  $.each(results.prostorijaImaTermin, function(id, rez) {
                    $('#modal-termin .input-group-bend[data-prostorija_id="'+id+'"]').removeClass('d-none');

                    var name = '';

                    if(rez != '')
                    {
                      var name = jammeetUsersIndexed[rez.user_id] ? jammeetUsersIndexed[rez.user_id].name : rez.naziv_benda;
                      var img = jammeetUsersIndexed[rez.user_id] ? 'https://jammeet.com/slike_burza_m/avatari/mini-' + jammeetUsersIndexed[rez.user_id].profil_slika : '';
                      $('#modal-termin .input-group-bend[data-prostorija_id="'+id+'"]').append( '<img data-id="'+id+'" class="ml-3 position-absolute input-img rounded-circle" src="'+img+'">' );

                    }

                    $('#modal-termin .input-group-bend[data-prostorija_id="'+id+'"] .bend-form').val(name)

                  })
                  $.each(results.prostorijaNemaTermin, function(key, id) {
                    $('#modal-termin .input-group-bend[data-prostorija_id="'+id+'"]').addClass('d-none');

                    $('#modal-termin .input-group-bend[data-prostorija_id="'+id+'"] .bend-form').val()
                    $('#modal-termin .input-group-bend[data-prostorija_id="'+id+'"] .input-img').remove()
                  })

                    $('#modal-termin .loader-div').toggleClass('d-none d-flex')

                }
              });

            }
            else {


              var jammeet_id = $('.div-right', $(this)).data('jammeet-user-id');

              if(jammeet_id !== undefined)
              {
                $( '#bend-list li[data-id="'+jammeet_id+'"]' ).trigger( "click" );
              }
              else{
                var name =  $('.div-right span.rez-u', $(this)).text();
                $('#modal-termin .input-group-bend[data-prostorija_id="'+$('.prostorija-tab.active').data('id')+'"] .bend-form').val(name);

              }

              //bend-list

            }



        });

        $(document).on("click",".fc-day-grid-event",function() {

          if( $(this).hasClass('delete-active') )
          {
              $(this).toggleClass('for-delete')
              $('.admin-check-pro', $(this)).removeClass('fa-clock')
              $('.admin-check-pro', $(this)).addClass('fa-trash-alt')

              checkToEnableDeleteTerminiBtn()

          }
          else {

              $('#modal-termin').modal('show');

              $('#modal-termin .title').text( createHrDatum( $(this).data('startdate') ) + ' - ' + $(this).data('vrijeme') ) ;

              $('#modal-termin .delete-event').data( 'parent_id', $(this).data('parent_id') ) ;
              $('#modal-termin .delete-event').data( 'id', $(this).data('id') ) ;
              $('#modal-termin .delete-event').data( 'startdate', $(this).data('startdate') ) ;

              $('#modal-termin .input-img').remove()

              if( $('.prostorija-tab.active').data('id') == 'all')
              {

                $('#modal-termin .loader-div').toggleClass('d-none d-flex')

                $.ajax({
                  url: '../../api/termini-kalendar/prostorije-imaju-termin',
                  method: 'post',
                  data: {
                    termin_parent_id: $(this).data('parent_id'),
                    startdate: $(this).data('startdate'),
                    user_id: active_user.id,
                  },
                  success: function(results){

                    $.each(results.prostorijaImaTermin, function(id, rez) {
                      $('#modal-termin .input-group-bend[data-prostorija_id="'+id+'"]').removeClass('d-none');

                      var name = '';

                      if(rez != '')
                      {
                        var name = jammeetUsersIndexed[rez.user_id] ? jammeetUsersIndexed[rez.user_id].name : rez.naziv_benda;
                        var img = jammeetUsersIndexed[rez.user_id] ? 'https://jammeet.com/slike_burza_m/avatari/mini-' + jammeetUsersIndexed[rez.user_id].profil_slika : '';
                        $('#modal-termin .input-group-bend[data-prostorija_id="'+id+'"]').append( '<img data-id="'+id+'" class="ml-3 position-absolute input-img rounded-circle" src="'+img+'">' );

                      }

                      $('#modal-termin .input-group-bend[data-prostorija_id="'+id+'"] .bend-form').val(name)

                    })
                    $.each(results.prostorijaNemaTermin, function(key, id) {
                      $('#modal-termin .input-group-bend[data-prostorija_id="'+id+'"]').addClass('d-none');

                      $('#modal-termin .input-group-bend[data-prostorija_id="'+id+'"] .bend-form').val()
                      $('#modal-termin .input-group-bend[data-prostorija_id="'+id+'"] .input-img').remove()
                    })

                      $('#modal-termin .loader-div').toggleClass('d-none d-flex')

                  }
                });

              }
              else {


                var jammeet_id = $('.div-right', $(this)).data('jammeet-user-id');

                if(jammeet_id !== undefined)
                {
                  $( '#bend-list li[data-id="'+jammeet_id+'"]' ).trigger( "click" );
                }
                else{
                  var name =  $('.div-right span.rez-u', $(this)).text();
                  $('#modal-termin .input-group-bend[data-prostorija_id="'+$('.prostorija-tab.active').data('id')+'"] .bend-form').val(name);

                }

                //bend-list

              }

          }

        });

        $(document).on("click",".btn-add-termin",function() {

          $(this).css('display', 'none');


          if($('i[data-parent_id="'+$(this).data('id')+'"][data-startdate="'+$(this).data('date')+'"]').length==0)
          {
            showOverlay();

            var prostorija_id = $('.prostorija-tab.active').data('id');

            $.ajax({
              url: '../../api/termini-kalendar',
              method: 'post',
              data: {
                id: $(this).data('id'),
                datum: $(this).data('date'),
                prostorija_id: prostorija_id,
                user_id: active_user.id,
                mjesto_id: 1
              },
              success: function(results){

                var add = [];

                var total = results.length;

                var datePre = '';
                var datePre2 = '';

                $.each(results, function(key, date)
                {
                  datePre2 = calendar.getEventById( date.id );
                  if(datePre2)
                  {
                    datePre = calendar.getEventById( date.id );
                  }
                })


                $.each(results, function(key, date)
                {
                  if(key < 1)
                  {

                    //ako je vise, kad su sve prostorije odabrane onda se taj termin dodat onoliko puta koliko prostorija ima
                    //zato treba sprijecit


                    var activeTab = $('.prostorija-tab.active').data('id');

                    if( datePre != ''  && activeTab == 'all' && datePre.extendedProps.num != '0/' + total)
                    {
                        //ovo je slucaj:
                        //odabrane su sve prostorije, imas npr 3 prostorije, termin postoji za dvije prostorije
                        //dodao si sad taj termin preko dropdown-a, u kalendaru ce i dalje pisat npr 0/2 a trebalo bi bit 0/3, zato refresham kalenadar
                        if(calendar.view.dayTable)
                        {

                          var allDatesInMonth = calendar.view.dayTable.daySeries.dates;
                        }
                        else{

                            var allDatesInMonth = calendar.view.dayDates;
                        }
                        renderTerminiByMonth(activeTab, allDatesInMonth, active_user.id)
                    }


                    date.num = '0/' + total;
                    add.push(date);

                  }
                });

                addEventCustom(add);
                hideOverlay();

              }

            });

          }


        });


        $('#dodaj-termin').click(function(){

            showOverlay();

            var pocetak_h = $('#modal-uredi-termine .form-pocetak .timePart.hours').val();
            var pocetak_m = $('#modal-uredi-termine .form-pocetak .timePart.minutes').val();
            var kraj_h = $('#modal-uredi-termine .form-kraj .timePart.hours').val();
            var kraj_m = $('#modal-uredi-termine .form-kraj .timePart.minutes').val();
            var prostorija_id = $('.prostorija-tab.active').data('id');

            pocetak_h = pocetak_h < 10 ? pocetak_h = '0'+pocetak_h : pocetak_h;
            pocetak_m = pocetak_m < 10 ? pocetak_m = '0'+pocetak_m : pocetak_m;
            kraj_h = kraj_h < 10 ? kraj_h = '0'+kraj_h : kraj_h;
            kraj_m = kraj_m < 10 ? kraj_m = '0'+kraj_m : kraj_m;

            var pocetak = pocetak_h + ':' + pocetak_m;
            var kraj = kraj_h + ':' + kraj_m;

            $.ajax({
              url: '../../api/termini-vrijeme-prostorije-veza',
              method: 'post',
              data: {
                pocetak: pocetak,
                kraj: kraj,
                prostorija_id: prostorija_id,
                user_id: active_user.id
              },
              success: function(result){

                if(result)
                {

                  var content =

                          '<div data-id="' + result.vrijeme.id + '" class="d-inline-block col-12 col-md-5 zero-padding delete-termin-div mb-3 pl-5 pr-5 pr-md-3">'+
                            '<input class="styled-checkbox termin-checkbox" data-id="' + result.vrijeme.id + '" id="checkbox-termin-' + result.vrijeme.id + '" type="checkbox" name="remember">'+
                            '<label for="checkbox-termin-' + result.vrijeme.id + '">'+
                              '<div class="fc-event termin-div">'+
                                '<div class="div-left">'+
                                  '<i class="far fa-clock edu-danger-error admin-check-pro" aria-hidden="true"></i>'+
                                '</div>'+
                                '<div class="div-right">'+
                                    '<p>' + pocetak + ' : ' + kraj + '</p>'+
                                '</div>'+
                              '</div>'+
                            '</label>'+

                            '<span data-id="' + result.vrijeme.id + '" class="delete-termin-btn ml-2" aria-hidden="true">×</span>'+

                          '</div>';

                  $('#modal-uredi-termine .dodani-termini').append(content);


                  var content =
                        '<div name="' + result.id + '" class="fc-event termin-div mr-2">'+
                          '<div class="div-left">'+
                            '<i class="far fa-clock edu-danger-error admin-check-pro" aria-hidden="true"></i>'+
                          '</div>'+
                          '<div class="div-right">'+
                              '<p>' + pocetak + ' : ' + kraj + '</p>'+
                          '</div>'+
                        '</div>';

                  $(content).insertBefore('#external-events-list .prepend-here');

                  addTerminToOnclickList(result);

                }

                hideOverlay();

              }
            });



        });


        $('#spremi-termine-u-kal').click(function(){

          showOverlay();

          var termin_ids = [];
          $('.termin-checkbox').each(function(e){
              if($(this).is(":checked")){
                termin_ids.push($(this).data('id'));
              }
          });

          var days = [];
          $('.dan-checkbox').each(function(e){
              if($(this).is(":checked")){
                days.push($(this).data('dan'));
              }
          });

          var months = [];
          $('.mjesec-checkbox').each(function(e){
              if($(this).is(":checked")){
                months.push($(this).data('mjesec'));
              }
          });


          var years = [startYear];
          if(startYear != endYear)
          {
              years.push(endYear);
          }
          /*
          $('.godina-checkbox').each(function(e){
              if($(this).is(":checked")){
                years.push($(this).data('godina'));
              }
          });

          if(years.length == 0)
          {
            years.push(startYear)
          }
          */

          var dates = [];
          $.each(years, function(key, year){

            $.each(months, function(key, month){

              $.each(days, function(key, day){

                  dates = dates.concat(getNumberOfDaysInMonth(day, month, year));

              });


            });

          });

          var prostorija_id = $('.prostorija-tab.active').data('id');

          //console.log(termin_ids);
          //console.log(dates);
          //console.log(prostorija_id);


          if(dates.length > 0 && termin_ids.length > 0)
          {
            $.ajax({
              url: '../../api/termini-kalendar-multiple',
              method: 'post',
              data: {
                ids: termin_ids,
                dates: dates,
                prostorija_id: prostorija_id,
                user_id: active_user.id,
                date_start: active_user.date_start,
                date_end: active_user.date_end,
                mjesto_id: 1
              },
              success: function(result){
                var check = [];
                var add = [];
                var add2 = [];
                var counter = [];

                $.each(result, function(key, date)
                {

                  var provjera = date.start+date.end;

                  if(!check.includes(provjera))
                  {
                    check.push(provjera);

                    date.num = '0/1';
                    add.push(date);

                    counter[provjera+'total'] = 1;
                    add2[provjera] = date;
                  }else {

                    counter[provjera+'total']++;
                    add2[provjera].num = '0/' + counter[provjera+'total'];
                  }

                });
                addEventCustom(add);


                $(".termin-checkbox").prop("checked", false);
                $(".dan-checkbox").prop("checked", false);
                $(".mjesec-checkbox").prop("checked", false);
                $(".godina-checkbox").prop("checked", false);
                $('#modal-uredi-termine .checked-div').removeClass('checked-div');

                hideOverlay();

              }
            });
          }
          else
          {
            //nije odabrano vrijeme termin i/ili nisu odabrani dan i mjesec
            //alert('Mora biti odabran minimalno jedan termin, dan i mjesec');
            createInfoDiv('Mora biti odabran minimalno jedan termin, dan i mjesec', 'warning')
            hideOverlay();

          }


        });

        function getNumberOfDaysInMonth(day, month, year = 2019) {
            //month 1 - sijecanj...
            //0 - nedjelja

            month--;

            var d = new Date(year, month, 1),
            days = [];


            //d.setDate(1);


            // Get the first Monday in the month
            while (d.getDay() !== day) {
                d.setDate(d.getDate() + 1);
            }

            // Get all the other Mondays in the month
            while (d.getMonth() === month) {
                days.push( yyyymmdd(d.getTime()) );
                d.setDate(d.getDate() + 7);
            }


            return days;
        }

        function yyyymmdd(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [year, month, day].join('-');
        }


        //console.log(calendar);

        $(document).on('click', '.delete-event', function(e) {

            var event = calendar.getEventById( $(this).data('id') );

            //console.log($(this).data('id'));
            var prostorija_id = $('.prostorija-tab.active').data('id');

            var deleteType = prostorija_id == 'all' ? 'all' : 'one';
            var confirmText = 'Izbrisati ovaj termin za ';
            confirmText+= prostorija_id == 'all' ? 'sve prostorije?' : $('.prostorija-tab.active').text() + '?';

            var r = confirm(confirmText);
            if(r)
            {
                $.ajax({
                    url: '../../api/termini-kalendar',
                    method: 'delete',
                    data:  {
                      id: $(this).data('id'),
                      type: deleteType
                    }
                });

                if(event != null)
                {
                  event.remove();
                }
            }

        });

        $(document).on('click', '.delete-termin-btn', function(e) {

            showOverlay();
            alert('treba napravit provjeru, ako bend ima probu u to vrijeme da se napravi promt dal zeli da se izbrise termin svejedno');

            var data_id = $(this).data('id');
            $('.delete-termin-div[data-id="'+data_id+'"]').remove();
            $('.termin-div[name="'+data_id+'"]').remove();
            $('.btn-add-termin[data-id="'+data_id+'"]').parent().parent().remove();
            $('#add-new-termin .terminRow[data-id="'+data_id+'"]').remove();

            var prostorija_id = $('.prostorija-tab.active').data('id');

           // $('.delete-event[data-parent_id="'+data_id+'"]').trigger('click');

            $.ajax({
                url: '../../api/termini-kalendar-multiple',
                method: 'delete',
                data: {
                  id: $(this).data('id'),
                  prostorija_id: prostorija_id,
                  user_id: active_user.id
                },
                success: function(termini){

                   $.each(termini, function (key, val) {

                    var event = calendar.getEventById( val );
                    if(event != null)
                    {
                      event.remove();
                    }
                   });

                  hideOverlay();
                }
            });


        });


          $('.prostorija-tab').click(function(){


                showOverlay();
                showCalendarLoader();
                var prostorija_id = $(this).data('id')
                $('#modal-termin .modal-title').text( $(this).text() )

                if(prostorija_id == 'all')
                {
                  $('#modal-termin .input-group-bend').removeClass('d-none');
                }
                else {
                  $('#modal-termin .input-group-bend').addClass('d-none');
                  $('#modal-termin .input-group-bend[data-prostorija_id="'+prostorija_id+'"]').removeClass('d-none');

                  //kad otvaram termin za uredit da je odmah dropdown ispod ton inputa
                  $('#modal-termin .input-group-bend[data-prostorija_id="'+prostorija_id+'"]').append( $('#bend-list') );

                }

                $.ajax({
                  url: '../../api/termini-prostorije',
                  method: 'post',
                  data: {
                    id: prostorija_id,
                    user_id: active_user.id
                  },
                  success: function(result)
                  {
                    calendar.removeAllEvents();

                    $('#external-events-list .termin-div').remove();
                    $('#add-new-termin .terminRow').remove();
                    $('#modal-uredi-termine .dodani-termini .delete-termin-div').remove();

                    if(calendar.view.dayTable)
                    {

                      var allDatesInMonth = calendar.view.dayTable.daySeries.dates;
                    }
                    else{

                        var allDatesInMonth = calendar.view.dayDates;
                    }
                    renderTerminiByMonth(prostorija_id, allDatesInMonth, active_user.id)

                    //ovo sluzi da mi se kad su sve prostorije odabrane, da se ne pojavljuje vise puta isto vrijeme za isti dan
                    //npr tri termina za 13-15 u ponedjeljak
                    var termini_check = [];
                    var termini_check2 = [];

                    //ovo sam dodao da mogu termine od prostorije sort-at po vremenu
                    var ordered = [];
                    var ordered2 = [];

                   $.each(result, function (key1, prostor) {

                    $('.prostorija-profil-img').css('background-image', 'url("../../uploads/prostorije/'+ prostor.profil_slika+'")')

                      $.each(prostor.vrijemeOrdered, function(key, termin)
                      {
                          var provjera = termin.vrijeme.pocetak+termin.vrijeme.kraj;

                          if(!termini_check.includes(provjera))
                          {
                            termini_check.push(provjera);

                            ordered.push(provjera);
                            ordered2[provjera] = termin;
                            //addTerminToExternalEventsList(termin);
                          }
                      });


                      /*
                      $.each(prostor.termini, function(key, termin){

                          $.each(termin.kalendar, function(key, date)
                          {
                            date.datum_str = date.datum_str.replace(" 00:00:00", "");
                            date.end = date.end.replace(" 00:00:00", "");
                            date.start = date.start.replace(" 00:00:00", "");

                            var provjera2 = date.start+date.end;

                            if(!termini_check2.includes(provjera2))
                            {
                              termini_check2.push(provjera2);
                              addEventCustom(date);
                            }


                          });

                      });
                      */


                   });

                   //morao sam izdvojiti izvan
                   ordered.sort();
                   $.each(ordered, function(key, val)
                   {
                     addTerminToExternalEventsList(ordered2[val]);
                     addTerminToOnclickList(ordered2[val]);
                   });

                   //hideOverlay();

                  }
                });


          });

          function renderTerminiByDay(active_prostorija_id, date, user_id){

              disableDeleteTermini();

              showOverlay();
              showCalendarLoader();


              $.ajax({
                url: '../../api/termini-prostorije/kalendar/day',
                method: 'post',
                data: {
                  id: active_prostorija_id,
                  date: date,
                  user_id: user_id
                },
                success: function(kalendar)
                {

                    calendar.removeAllEvents();
                    console.log(kalendar)

                  //ovo sluzi da mi se kad su sve prostorije odabrane, da se ne pojavljuje vise puta isto vrijeme za isti dan
                  //npr tri termina za 13-15 u ponedjeljak
                  var termini_check2 = [];
                  var add = [];
                  var add2 = [];
                  var counter = [];

                  var total = kalendar.length;

                  var i = 0;
                  var barWidth = 0;



                  (function loop() {

                      var width = i / total * 100;

                      //napravio sam ovako jer ako ide postotak po postotak progress bar onda width dode do 100% a u frontu
                      //se ne prikaze cijeli bar nego dode do tipa 70% i vec se sve FullCalendarInteraction
                      // na ovaj nacin, dode do 80% odmah skoci za 5% vise i stigne se sve izvrtit
                      if( width >= barWidth )
                      {
                        $('.termini-progress').css('width', (width + 5) +'%');
                        barWidth+=5;
                      }

                      //var counter = [];

                      if (i < total) {

                          var date = kalendar[i];

                          date.datum_str = date.datum_str.replace(" 00:00:00", "");
                          date.end = date.end.replace(" 00:00:00", "");
                          date.start = date.start.replace(" 00:00:00", "");

                          var provjera2 = date.start+date.end;

                          if(!termini_check2.includes(provjera2))
                          {
                            termini_check2.push(provjera2);
                            //add.push(date);
                            add.push(date);

                            counter[provjera2+'rez'] = 0;
                            counter[provjera2+'total'] = 1;
                            if(date.rezervirano){
                              counter[provjera2+'rez']++;
                            }

                            date.num = counter[provjera2+'rez'] + '/' + counter[provjera2+'total'];
                            add2[provjera2] = date;

                          }
                          else{

                            if(date.rezervirano){
                              counter[provjera2+'rez']++;
                            }
                            counter[provjera2+'total']++;
                            add2[provjera2].num = counter[provjera2+'rez'] + '/' + counter[provjera2+'total'];

                          }

                          i++;
                          setTimeout(loop, 0);
                      }
                      else{

                        console.log(add)

                          addEventCustom(add);
                          hideOverlay();
                          hideCalendarLoader();
                          $('.termini-progress').css('width', '0%');

                      }

                  })();


                }
              });

          }

          function renderTerminiByMonth(active_prostorija_id, allDatesInMonth, user_id){

              disableDeleteTermini();

              showOverlay();
              showCalendarLoader();


              $.ajax({
                url: '../../api/termini-prostorije/kalendar',
                method: 'post',
                data: {
                  id: active_prostorija_id,
                  allDatesInMonth: allDatesInMonth,
                  user_id: user_id
                },
                success: function(kalendar)
                {

                    calendar.removeAllEvents();

                    /*

                   $.each(calendar.getEvents(), function (key, val)
                   {

                    var event = calendar.getEventById( val.id );

                    if(event != null)
                    {
                      event.remove();
                    }
                   });
                   */

                  //ovo sluzi da mi se kad su sve prostorije odabrane, da se ne pojavljuje vise puta isto vrijeme za isti dan
                  //npr tri termina za 13-15 u ponedjeljak
                  var termini_check2 = [];
                  var add = [];
                  var add2 = [];
                  var counter = [];

                  var total = kalendar.length;

                  var i = 0;
                  var barWidth = 0;

                  /*
                  var test = [];
                  $.each(kalendar, function(key, date) {
                      date.datum_str = date.datum_str.replace(" 00:00:00", "");
                      date.end = date.end.replace(" 00:00:00", "");
                      date.start = date.start.replace(" 00:00:00", "");
                      test.push(date);
                  })
                  */


                  (function loop() {

                      var width = i / total * 100;

                      //napravio sam ovako jer ako ide postotak po postotak progress bar onda width dode do 100% a u frontu
                      //se ne prikaze cijeli bar nego dode do tipa 70% i vec se sve FullCalendarInteraction
                      // na ovaj nacin, dode do 80% odmah skoci za 5% vise i stigne se sve izvrtit
                      if( width >= barWidth )
                      {
                        $('.termini-progress').css('width', (width + 5) +'%');
                        barWidth+=5;
                      }

                      //var counter = [];

                      if (i < total) {

                          var date = kalendar[i];

                          date.datum_str = date.datum_str.replace(" 00:00:00", "");
                          date.end = date.end.replace(" 00:00:00", "");
                          date.start = date.start.replace(" 00:00:00", "");

                          var provjera2 = date.start+date.end;

                          if(!termini_check2.includes(provjera2))
                          {
                            termini_check2.push(provjera2);
                            //add.push(date);
                            add.push(date);

                            counter[provjera2+'rez'] = 0;
                            counter[provjera2+'total'] = 1;
                            if(date.rezervirano){
                              counter[provjera2+'rez']++;
                            }

                            date.num = counter[provjera2+'rez'] + '/' + counter[provjera2+'total'];
                            add2[provjera2] = date;

                          }
                          else{

                            if(date.rezervirano){
                              counter[provjera2+'rez']++;
                            }
                            counter[provjera2+'total']++;
                            add2[provjera2].num = counter[provjera2+'rez'] + '/' + counter[provjera2+'total'];

                          }

                          i++;
                          setTimeout(loop, 0);
                      }
                      else{

                          addEventCustom(add);
                          hideOverlay();
                          hideCalendarLoader();
                          $('.termini-progress').css('width', '0%');

                      }

                  })();


                }
              });

          }

          function addTerminToExternalEventsList(termin){

              if(termin)
              {

                termin.vrijeme.pocetak = termin.vrijeme.pocetak.length > 5 ? termin.vrijeme.pocetak.substring(0, termin.vrijeme.pocetak.length - 3) : termin.vrijeme.pocetak;
                termin.vrijeme.kraj = termin.vrijeme.kraj.length > 5 ? termin.vrijeme.kraj.substring(0, termin.vrijeme.kraj.length - 3) : termin.vrijeme.kraj;



                var content =

                        '<div data-id="' + termin.vrijeme.id + '" class="d-inline-block col-12 col-md-5 zero-padding delete-termin-div mb-3 pl-5 pr-5 pr-md-3">'+
                          '<input class="styled-checkbox termin-checkbox" data-id="' + termin.vrijeme.id + '" id="checkbox-termin-' + termin.vrijeme.id + '" type="checkbox" name="remember">'+
                          '<label for="checkbox-termin-' + termin.vrijeme.id + '">'+
                            '<div class="fc-event termin-div">'+
                              '<div class="div-left">'+
                                '<i class="far fa-clock edu-danger-error admin-check-pro" aria-hidden="true"></i>'+
                              '</div>'+
                              '<div class="div-right">'+
                                  '<p>' + termin.vrijeme.pocetak + ' : ' + termin.vrijeme.kraj + '</p>'+
                              '</div>'+
                            '</div>'+
                          '</label>'+

                          '<span data-id="' + termin.vrijeme.id + '" class="delete-termin-btn ml-2" aria-hidden="true">×</span>'+

                        '</div>';

                $('#modal-uredi-termine .dodani-termini').append(content);



                var content =
                      '<div name="' + termin.vrijeme.id + '" class="fc-event termin-div mr-2">'+
                        '<div class="div-left">'+
                          '<i class="far fa-clock edu-danger-error admin-check-pro" aria-hidden="true"></i>'+
                        '</div>'+
                        '<div class="div-right">'+
                            '<p>' + termin.vrijeme.pocetak + ' : ' + termin.vrijeme.kraj + '</p>'+
                        '</div>'+
                      '</div>';

                $(content).insertBefore('#external-events-list .prepend-here');

              }

          }

          function addTerminToOnclickList(termin) {

            if(termin)
            {

              termin.vrijeme.pocetak = termin.vrijeme.pocetak.length > 5 ? termin.vrijeme.pocetak.substring(0, termin.vrijeme.pocetak.length - 3) : termin.vrijeme.pocetak;
              termin.vrijeme.kraj = termin.vrijeme.kraj.length > 5 ? termin.vrijeme.kraj.substring(0, termin.vrijeme.kraj.length - 3) : termin.vrijeme.kraj;

              var content =
                      '<div class="row terminRow" data-id="' + termin.vrijeme.id + '">'+
                        '<div class="col-12 ">'+
                          '<div data-id="' + termin.vrijeme.id + '" data-date="2000" class="fc-event termin-div btn-add-termin">'+
                            '<div class="div-left">'+
                              '<i class="far fa-clock edu-danger-error admin-check-pro" aria-hidden="true"></i>'+
                            '</div>'+
                            '<div class="div-right">'+
                                '<p>' + termin.vrijeme.pocetak + ' : ' + termin.vrijeme.kraj + '</p>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                      '</div>';

              $('#add-new-termin').append(content);

            }

          }


        $(".bend-form").focus(function(){
          $(this).parent().append( $('#bend-list') );
          $('#bend-list').addClass('d-none')
        });

        $('#modal-termin').on('hide.bs.modal', function () {
          $('#bend-list').addClass('d-none')
          //$('#modal-termin .input-img').remove()
        })

        $(document).on("click","#modal-termin",function(e) {
          if ( !$(e.target).hasClass('bend-form') ) {
            $('#bend-list').addClass('d-none')
          }
        });

        $(document).on("click","#spremi-termin-izmjene",function(e) {

            var data = [];
            //var termin_id =  $('#modal-termin .delete-event').data( 'id' ) ;
            var termin_parent_id =  $('#modal-termin .delete-event').data( 'parent_id' ) ;
            var startdate =  $('#modal-termin .delete-event').data( 'startdate' ) ;

            $('#modal-termin .input-group-bend').each(function (key, item) {

              if( $(item).is(":visible") ){

                  if ( $('img.input-img', item).length){
                      //user od jammeet-a
                      var rez = {
                          prostorija_id: $(item).data('prostorija_id'),
                          user_id:  $('.input-img', item).data('id'),
                          naziv_benda: $('.bend-form', item).val()
                        };
                  }
                  else{
                    //obican string za naziv benda
                    var rez = {
                        prostorija_id: $(item).data('prostorija_id'),
                        user_id: "",
                        naziv_benda: $('.bend-form', item).val()
                      };
                  }

                  data.push(rez)

              }

            })


            $.ajax({
              url: '../../api/termini-kalendar/save-termin',
              method: 'post',
              data: {
                //termin_id: termin_id,
                termin_parent_id: termin_parent_id,
                startdate: startdate,
                rezs: data,
              },
              success: function(results){
                //console.log(results)

                if(calendar.view.dayTable)
                {

                  var allDatesInMonth = calendar.view.dayTable.daySeries.dates;
                }
                else{

                    var allDatesInMonth = calendar.view.dayDates;
                }

                  var id = $('.prostorija-tab.active').data('id');

                  renderTerminiByMonth(id, allDatesInMonth, active_user.id)
              }
            });

        });



        $(".bend-form").on("input", function(){

          var str = $(this).val().toLowerCase();
          //if (str.toLowerCase().indexOf("yes") >= 0)

          $( '.input-img' , $(this).parent() ).remove() //makni img od korisnika ako je ima u inputu

          $('#bend-list').removeClass('d-none')
          if( str.length == 0 )
          {
            $('#bend-list').addClass('d-none')
          }
          else {

            $('#bend-list li').each(function(key, item) {
              var str2 = $(item).data('name').toLowerCase();
              if (str2.toLowerCase().indexOf(str) >= 0)
              {
                //pronaden je string u nazivu od korisnika sa jammeet-a
                $(this).removeClass('d-none')
              }else {
                $(this).addClass('d-none')
              }
            });

          }

        });

        $(document).on("click","#bend-list li",function(e) {

          var name = $(this).data('name')
          var id = $(this).data('id')
          var profil_slika = 'https://jammeet.com/slike_burza_m/avatari/mini-' + $(this).data('profil_slika')

          $( this ).parent().prev( ".bend-form" ).val( name );
          $( '.input-img' , $(this).parent().parent() ).remove() //.input-group-bend
          $( this ).parent().parent().append( '<img data-id="'+id+'" class="ml-3 position-absolute input-img rounded-circle" src="'+profil_slika+'">' );

        });



        $(document).ready(function() {
            if($(window).width() >= 1200)
            {
              //$('#calendar .fc-header-toolbar').css('max-width', '80%')
              $('#calendar').addClass('month-view')
            }

        })

        $(document).on("click",".fc-dayGridMonth-button",function(e) {
            //$('#calendar .fc-header-toolbar').css('max-width', '80%')
            $('#calendar').addClass('month-view')
            $('#delete-multiple-termin').addClass('d-xl-inline-block')
        });
        $(document).on("click",".fc-listWeek-button",function(e) {
            //  $('#calendar .fc-header-toolbar').css('max-width', '580px')
            $('#calendar').removeClass('month-view')
            $('#delete-multiple-termin').removeClass('d-xl-inline-block')
        });

        var startYear = new Date(active_user.date_start).getFullYear();
        var endYear = new Date(active_user.date_end).getFullYear();


        //console.log(calendar.refetchEvents());

        //calendar end
  });


</script>
<style>

  body {
    font-size: 14px;
  }

  .info-alert-div{
    bottom: 20%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 1051;
  }
  .info-alert-div button{
    outline: none;
  }


  .fc-listWeek-view{
    max-width: 580px;
    margin: 0 auto;
  }
  @media(min-width:1200px)
  {
    .fc-listWeek-view{
      margin-left: 265px;
    }
  }

  #calendar.month-view .fc-listWeek-view{
      margin-left: auto;
  }

  .calendar-loader{
    z-index: 1050;
    top: 0;
    left: 0;
    cursor: wait;
    background: rgba(0,0,0,.3);
  }

  .fc-day-grid-container .text-availability{
      font-size: 14px;
      bottom: 5px;
      right: 5px;
      line-height: 1;
  }
  .fc-listWeek-view .text-availability{
    bottom: 8px;
    right: 14px;
  }


  div.termin-div{
    display: inline-block;
    height: 30px;
    border-radius: 0;
  }


  #modal-termin div.termin-div{
    margin-left: 54px;
    margin-top: 20px;
  }

  #modal-termin .modal-body{
    border-bottom: 1px solid #e5e5e5;
    margin-bottom: 15px;
  }


  #wrap {
    margin: 0 auto;
  }

  #bend-list{
    z-index: 3;
    top: 100%;
    max-height: 500px;
  }

  #bend-list li:hover{
    cursor: pointer;
    background: #F6F8FA;
  }

  .input-group-bend .input-img{
    top: 5px;
    right: 10px;
    width: 29px;
    z-index: 4;
  }
  .fc-header-toolbar{

    max-width: 580px;
    margin-left: auto;
    margin-right: auto;
    margin-top: 30px;
  }
  @media(min-width:1200px)
  {
    .fc-header-toolbar{
      margin-left: 265px;
    }
  }
  #calendar.month-view .fc-header-toolbar{
      margin-left: auto;
      max-width: 80%;
  }


  .fc-button-group {

    /*margin-right: 30px;*/
    float: left;
  }

  #calendar .fc-head th.fc-day-header {
    color: #fff;
    padding: 8px 10px;
    font-size: 18px;
  }
  #calendar td.fc-day {
    background-color: #fff;
    padding: 10px;
  }
  #calendar td.fc-day-top span.fc-day-number  {
    float: left;
    padding: 10px 15px;
    font-size: 16px;
  }

  #calendar .fc-event-container {
    padding: 3px 6px;
  }

  #calendar .fc-event-container .fc-content {
    min-height: 50px;
  }

  #calendar .fc-event-container .fc-day-grid-event {
    min-height: 50px;
    cursor: pointer;
  }
  #calendar .fc-list-item {
    background: #fff;
  }
  #calendar .fc-list-item:hover {
    cursor: pointer;
    background: #F6F8FA;
  }

  #calendar .fc-event-container .fc-day-grid-event.delete-active{
        opacity: 0.7;
  }

  #calendar .fc-event-container .fc-day-grid-event.for-delete {
    background-color: rgba(209, 26, 42, 0.2);
    border: 1px solid rgba(209, 26, 42, 0.3);
    opacity: 1;
  }
  #calendar .fc-event-container .fc-day-grid-event.for-delete .div-left{
    background-color: #e63b4a !important;
    background: #e63b4a !important;
  }
  #calendar .fc-event-container .fc-day-grid-event.for-delete .div-right{
    background-color: rgba(209, 26, 42, 0.2);
  }
  #calendar .fc-event-container .fc-day-grid-event.for-delete .div-left:after{
    border-left-color: #e63b4a !important;
    border-right-color: #e63b4a !important;
  }
  #calendar .fc-view-container .fc-list-item .div-left{
    display: none !important;
  }
  #calendar .fc-view-container .fc-list-item .div-right{
    margin-left: 0;
    padding: 8px 14px;
    border-top: 1px solid #eee;
  }



  #calendar .fc-event-container p {
    margin-bottom: 0;
    line-height: 50px;
    font-size: 16px;
  }
  #calendar .fc-event-container .div-right {
    margin-bottom: 0;
    line-height: 50px;
    font-size: 16px;
    margin-left: 0;
    padding-left: 55px;
  }


  #calendar .fc-event-container div.has-band.div-right{
      background-color: #8EE28E;
        line-height: 1;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        padding-right: 5px;
        height: 50px;

    }
  #calendar .fc-event-container div.has-band.div-left{
      width: 44px;
      background-color: #4BAD4B;

    }
  #calendar .fc-event-container div.has-band.div-left:after {
        content: "";
        left: 40px;
        border-left-color: #4BAD4B;
    }


  .custom-green-bg{
    background-color: #4BAD4B;
  }
  .custom-green-bg:after {
      border-left-color: #4BAD4B;
  }
  #calendar .fc-view-container .fc-list-item .custom-green-bg{
    background-color: #8EE28E;
  }

  #calendar .fc-event-container div.custom-green-bg.div-right{
      background-color: #8EE28E;

    }
  #calendar .fc-event-container div.has-band.div-right span  {

  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
    }

  #calendar .fc-title{
    white-space: normal;
  }
  a.fc-day-grid-event {
    margin: 0;
    margin-top: 0;
    padding: 0;
    border: none;
  }


  #add-new-termin{
    padding: 15px;
    border: 1px solid #ccc;
    position: fixed;
    top:0;
    bottom: 0;
    background-color: rgb(246, 248, 250);
    z-index: 1000;
    display: none;
    min-width: 198px;
    max-height: fit-content;
  }
  #add-new-termin h3 {
    margin-top: 0;
    margin-bottom: 20px;
    font-size: 20px;
  }
  .close-add-new-termin{

    font-size: 16px;
    position: absolute;
    top: 10px;
    right: 10px;
    cursor: pointer;
  }
  #loading-overlay{
    height: 100%;
    width: 100%;
    display: none;
    position: fixed;
    z-index: 1051;
    top: 0;
    left: 0;
    background-color: transparent;
    cursor: wait;
  }

  .modal-body{
    background-color: #F6F8FA;
  }

    .zero-padding{
      padding: 0;
    }

  @media (min-width: 1169px) and (max-width: 1199px)
  {
    .p1169{
      padding-top: 50px;
    }
  }

  @media (min-width: 992px) and (max-width: 1365px)
  {
    .left-sidebar-pro {
        display: none;
    }
    .all-content-wrapper {
      margin-left: 0;
    }
    .header-top-area{
      left: 0;
    }
  }



  @media (min-width: 992px) and (max-width: 1599px)
  {
    #calendar .fc-event-container .div-right{
      padding-left: 47px;
    }
    .fc-day-grid-container .text-availability{
      bottom: 2px;
      right: 2px;
    }
  }

</style>

@endsection

@section('title', 'Jammeet - kalendar')

@section('content')




<!-- Modal -->
<div id="modal-termin"  class="modal fade" tabindex="-1" role="dialog">

  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title">

            @if(count($user->prostorije) > 1)

              Sve prostorije

            @else
              @foreach($user->prostorije as $prostorija)
                {{$prostorija->name}}
                @break
              @endforeach
            @endif


        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body col-12 zero-padding mb-0">

        <h5 class="mx-2 text-center mt-4 title"></h5>

        <div class="row w-100 m-0 position-relative">

          <div style="z-index:10; background-color: #F6F8FA;" class="loader-div d-none position-absolute h-100 w-100 align-items-center justify-content-center">
            <img src="{{ URL::asset('img/loader.gif') }}">
          </div>

          <div class="col-12 px-3 px-xl-5 py-4 prostorije-container">

            @if(count($user->prostorije) > 0)

              @foreach($user->prostorije as $prostorija)

                <div class="input-group input-group-bend mb-3" data-prostorija_id="{{$prostorija->id}}">
                  <div class="input-group-prepend div-bend-input">
                    <span class="input-group-text bend-span">{{$prostorija->name}}</span>
                  </div>
                  <input type="text" class="form-control bend-form pr-5" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                </div>

              @endforeach

            @endif

          </div>

          <ul id="bend-list" class="d-none position-absolute bg-white border custom-scroller">

              @foreach(json_decode($jammeetUsers, true) as $ind => $jUser)

                  <?php
                    $class = $ind == 0 ? '' : 'border-top';
                  ?>

                  <li class="font-16 px-3 py-2 {{$class}}" data-name="{{$jUser['name']}}" data-id="{{$jUser['id']}}" data-profil_slika="{{$jUser['profil_slika']}}">
                    <img class="mr-3 rounded-circle" src="https://jammeet.com/slike_burza_m/avatari/mini-{{$jUser['profil_slika']}}">{{$jUser["name"]}} | {{$jUser["grad"]}}
                  </li>

              @endforeach

          </li>

        </div>

        <div class="row w-100 m-0">
          <div class="col-12 px-3 px-xl-5 pb-4">
            <p class="text-danger">
                * Lista prikazuje korisnike sa Jammeet platforme. Ako nema korisnika/benda u listi za kojeg radite rezervaciju za termin možete samo upisati naziv.
            </p>

          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn delete-event btn-danger mr-auto" data-dismiss="modal">Izbriši termin</button>
        <button id="spremi-termin-izmjene" type="button" class="btn btn-primary"  data-dismiss="modal">Spremi</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div id="modal-uredi-termine"  class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Termini - Prostorije</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body col-12 zero-padding">

        <h5 class="ml-5 pl-3 mt-4">Dodaj novi termin</h5>

        <div class="d-flex flex-wrap justify-content-center align-items-center align-items-md-end flex-column flex-md-row pt-3 pl-3 pr-3 pb-5">


          <div class="d-none d-md-inline-block mr-4">
            <div class="div-w-clock d-inline-block">
              <i class="far fa-clock edu-danger-error admin-check-pro" aria-hidden="true"></i>
            </div>
          </div>

          <div class="d-inline-block mb-4 mb-md-0">
              <p class="font-16 font-weight-bold text-center">Početak</p>
              <div class="form-pocetak"></div>
          </div>

          <div class="d-none d-md-inline-block pl-3 pr-3 pb-1">
              <p class="font-16 text-center mb-2">:</p>
          </div>

          <div class="d-inline-block mb-4 mb-md-0">
              <p class="font-16 font-weight-bold text-center">Kraj</p>
              <div class="form-kraj"></div>
          </div>

          <div class="d-inline-block d-md-block d-lg-inline-block ml-lg-4 mt-md-4 mt-lg-0">
            <button id='dodaj-termin' type="button" class="btn btn-custon-four btn-success mb-1">
                  <i class="fa fa-plus edu-checked-pro" aria-hidden="true"></i>
                  Dodaj termin
            </button>
          </div>

        </div>


        <div class="col-12 pt-4 pl-3 pr-3 pb-4 border-top dodani-termini">

            <h5 class="ml-5 mb-4">Dodani termini</h5>

            @foreach($user_termini_ordered as $ind => $termin)

                <?php
                  $p = substr($termin->vrijeme->pocetak, 0, -3);
                  $k = substr($termin->vrijeme->kraj, 0, -3);
                ?>

                <div data-id="{{$termin->vrijeme->id}}" class="d-inline-block col-12 col-md-5 zero-padding delete-termin-div mb-3 pl-5 pr-5 pr-md-3">

                    <input class="styled-checkbox termin-checkbox" data-id="{{$termin->vrijeme->id}}" id="checkbox-termin-{{$termin->vrijeme->id}}" type="checkbox" name="remember">
                    <label for="checkbox-termin-{{$termin->vrijeme->id}}">

                      <div class="fc-event termin-div">
                          <div class="div-left">
                              <i class="far fa-clock edu-danger-error admin-check-pro" aria-hidden="true"></i>
                          </div>
                          <div class="div-right">
                              <p>{{$p}} : {{$k}}</p>
                          </div>
                      </div>

                    </label>
                    <span data-id="{{$termin->vrijeme->id}}" class="delete-termin-btn ml-2" aria-hidden="true">×</span>

                </div>

            @endforeach

        </div>


        <div class="col-12 pt-4 pl-3 pr-3 pb-4 border-top">

            <h5 class="ml-5 mb-4">
              Dodaj termine u kalendar
              <i class="far fa-calendar-alt ml-2"></i>
            </h5>

            <p class="ml-5 mb-4 font-weight-bold">
              Dani
            </p>

            <div class="d-flex justify-content-center flex-column flex-md-row pr-5 pl-5">

              <?php
                $dani = [1 => 'Pon', 2 => 'Uto', 3 => 'Sri', 4 => 'Čet', 5 => 'Pet', 6 => 'Sub', 0 => 'Ned'];
              ?>

              @foreach($dani as $ind => $dan)

                  <div class="flex-fill day-div">
                      <input class="styled-checkbox dan-checkbox" data-dan="{{$ind}}" id="checkbox-dan-{{$ind}}" type="checkbox" name="remember">
                      <label for="checkbox-dan-{{$ind}}" class="mb-0 p-3 w-100">
                        <div class="d-inline-block p-div">
                            <p class="m-0 dan-p">
                            {{$dan}}
                            </p>
                        </div>
                      </label>
                  </div>

              @endforeach

            </div>


            <p class="ml-5 mt-4 mb-4 font-weight-bold">
              Mjeseci
            </p>


            <div class="d-flex flex-wrap justify-content-start flex-column flex-md-row pr-5 pl-5">

              <?php
                $mjeseci = [ "1. Sij", "2. Velj", "3. Ožu", "4. Tra", "5. Svi", "6. Lip", "7. Srp", "8. Kol", "9. Ruj", "10. Lis", "11. Stu", "12. Pro" ];

                //$startDate = DateTime::createFromFormat("Y-m-d", $user->date_start);
                //$endDate = DateTime::createFromFormat("Y-m-d", $user->date_end);

                $startDate = Carbon\Carbon::parse($user->date_start);
                $endDate = Carbon\Carbon::parse($user->date_end);

                $startYear = $startDate->format("Y");
                $endYear = $endDate->format("Y");

                $diff = $startDate->diffInMonths($endDate);

                $startMonth = $startDate->format("m");
                $endMonth = $endDate->format("m");
                //var_dump($endDate);

                $diff = $diff > 12 ? 12 : $diff;
                //var_dump($endDate);
                $notDisabledMonths = [];
                $i = 0;
                while($i <= $diff)
                {

                  $startMonth = $startMonth > 12 ? $startMonth-12 : $startMonth;

                  $notDisabledMonths[$startMonth] = $startMonth;
                  $startMonth++;
                  $i++;
                }


              ?>

              @foreach($notDisabledMonths as $ind => $m)

                  <div class="month-div">
                      <input class="styled-checkbox mjesec-checkbox" data-mjesec="{{$ind}}" id="checkbox-mjesec-{{$ind}}" type="checkbox" name="remember">
                      <label for="checkbox-mjesec-{{$ind}}" class="mb-0 p-3 w-100">
                        <div class="d-inline-block p-div">
                            <p class="m-0 dan-p">
                            {{$mjeseci[$m-1]}}
                            </p>
                        </div>
                      </label>
                  </div>

              @endforeach

            </div>

            <!--
            @if( $startYear != $endYear)

              <p class="ml-5 mt-4 mb-4 font-weight-bold">
                Godine
              </p>

              <div class="d-flex flex-wrap justify-content-start flex-column flex-md-row pr-5 pl-5">

                 @for($startYear; $startYear <= $endYear; $startYear++)

                    <div class="p-3 year-div col-auto">
                        <input class="styled-checkbox godina-checkbox" data-godina="{{$startYear}}" id="checkbox-godina-{{$startYear}}" type="checkbox" name="remember">
                        <label for="checkbox-godina-{{$startYear}}" class="mb-0">
                          <div class="d-inline-block p-div">
                              <p class="m-0 dan-p">
                              {{$startYear}}.
                              </p>
                          </div>
                        </label>
                    </div>

                  @endfor

                </div>

              @endif
            -->

        </div>


      </div>
      <div class="modal-footer">
        <button id="spremi-termine-u-kal" type="button" class="btn btn-primary">Spremi</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
      </div>
    </div>
  </div>
</div>


<div id="loading-overlay"></div>

  <div id='wrap' class="pt-5 pt-md-0">

    <div class="row">
      <div class="col-12">



        <ul class="custom-nav nav nav-tabs" id="myTab" role="tablist">

          @if(count($user->prostorije) > 1)

            <li class="nav-item d-flex">
              <a class="prostorija-tab nav-link active align-self-end" data-id="all" data-toggle="tab" role="tab" aria-controls="home" aria-selected="true">
                Sve prostorije
              </a>
            </li>

          @endif

          @foreach($user->prostorije as $prostorija)
            <li class="nav-item d-flex">
              <a class="prostorija-tab nav-link align-self-end  {{count($user->prostorije) > 1 ? '' : 'active'}}" data-id="{{$prostorija->id}}"  data-toggle="tab" role="tab" aria-controls="profile" aria-selected="false">{{$prostorija->name}}</a>
            </li>
          @endforeach
        </ul>

        <div class="d-flex flex-column flex-xl-row align-items-stretch termini-wrapper-div">

            <div class="p-2 mr-5 align-self-center">
              <div class="background-img prostorija-profil-img" style=" background-image: url('../../img/prostorije/default.jpg');">

              </div>
            </div>

            <div>

              <div class="h-100 d-flex align-items-start flex-column d-flex justify-content-between" id='external-events'>
                <h4>Termini</h4>

                <div id='external-events-list'>
                  <?php
                    $termini_count = count($user_termini_ordered);
                    $i = 0;
                  ?>

                  @foreach($user_termini_ordered as $termin)
                      <?php
                        $i++;
                        $p = substr($termin->vrijeme->pocetak, 0, -3);
                        $k = substr($termin->vrijeme->kraj, 0, -3);
                      ?>

                      <!--<div name="{{$termin->id}}" class='fc-event termin-div' data-event='{ title: "my event", extendedProps[termin_id: "666"] }'>-->
                      @if($i % 3 == 1)
                        <!--
                        <div class="col-12 row">
                        -->
                      @endif
                          <div name="{{$termin->vrijeme->id}}" class='fc-event termin-div mr-2'>
                            <div class="div-left">
                              <i class="far fa-clock edu-danger-error admin-check-pro" aria-hidden="true"></i>
                            </div>
                            <div class="div-right">
                                <p>{{$p}} : {{$k}}</p>
                            </div>
                          </div>
                      @if($i % 3 == 0 || $i == $termini_count)
                        <!--
                        </div>
                        -->
                      @endif

                  @endforeach

                  <span class="d-block prepend-here mb-4"></span>


                </div>

                <div class="d-flex w-100">

                  <button id="open-uredi-termine" class="fc-button d-inline-block fc-button-primary" type="button" data-target="#modal-uredi-termine" data-toggle="modal">
                    <i class="fas fa-pencil-alt"></i>
                    Uredi termine
                  </button>

                  <button id="delete-multiple-termin" class="fc-button d-none d-xl-inline-block fc-button-primary ml-3" type="button">
                    <i class="far fa-trash-alt"></i>
                    Izbriši termine
                  </button>

                  <div class="ml-auto delete-termini-display d-none">

                    <button id="select-all-termini" class="fc-button d-inline-block fc-button-primary ml-3" type="button">
                      <i class="fas fa-check"></i>
                        Odaberi sve termine u mjesecu
                    </button>

                    <button id="delete-selected-termini" class="d-inline-block btn btn-danger ml-3" style="font-size: 1em;" type="button" disabled>
                        Izbriši termine
                    </button>

                  </div>


                </div>


                <div class="pt-3 delete-termini-display d-none">
                  <span class="text-danger">
                    * Odaberite u kalendaru termine koje želite izbrisati i onda kliknite na gumb "Izbriši termine"
                  </span>
                </div>


                <div id="add-new-termin" data-date='2000'>

                  <h3>
                    Dodaj termine

                    <i class="close-add-new-termin fa fa-times edu-danger-error admin-check-pro" aria-hidden="true"></i>
                  </h3>
                  @foreach($user_termini_ordered as $termin)

                      <?php
                        $p = substr($termin->vrijeme->pocetak, 0, -3);
                        $k = substr($termin->vrijeme->kraj, 0, -3);
                      ?>
                      <div class="row terminRow" data-id="{{$termin->vrijeme->id}}">
                        <div class="col-12 ">

                          <div data-id="{{$termin->vrijeme->id}}" data-date='2000' class='fc-event termin-div btn-add-termin'>
                            <div class="div-left">
                              <i class="far fa-clock edu-danger-error admin-check-pro" aria-hidden="true"></i>
                            </div>
                            <div class="div-right">
                                <p>{{$p}} : {{$k}}</p>
                            </div>
                          </div>
                          <!--
                          <button id="btn-add-termin" data-date='2000' type="button" class="btn btn-custon-rounded-three btn-success"><i class="fa fa-check edu-checked-pro" aria-hidden="true"></i>
                            </button>
                            -->

                        </div>
                      </div>

                  @endforeach



                </div>

              </div>

            </div>

        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12 position-relative mt-4">


        <div id='calendar'></div>
        <div class="position-absolute w-100 h-100 calendar-loader">

          <div class="container p-4 mt-4 bg-white shadow border" style="max-width:600px">
            <h2 class="mt-1">Termini se učitavaju</h2>
            <p>Molimo pričekajte trenutak</p>
            <div class="progress mb-3">
              <div id="progressbar" class="progress-bar progress-bar-striped progress-bar-animated termini-progress"></div>
            </div>
            <!--
            <div id="progressbar"><div class="progress-label">Loading...</div></div>
          -->

          </div>

        </div>
      </div>
    </div>


    <div style='clear:both'></div>

  </div>


@endsection


@section('extra_scripts2')
<!-- time input scripts -->
<script src="{{ URL::asset('time-input/js/main.js') }}"></script>
<script src="{{ URL::asset('js/custom-calendar.js') }}"></script>

<link rel="stylesheet" href="{{ URL::asset('time-input/css/main.css') }}">



@endsection
