<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>500 Page | Kiaalap - Kiaalap Admin Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="admin-panel/img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Play:400,700" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="admin-panel/css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="admin-panel/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="admin-panel/css/owl.carousel.css">
    <link rel="stylesheet" href="admin-panel/css/owl.theme.css">
    <link rel="stylesheet" href="admin-panel/css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="admin-panel/css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="admin-panel/css/normalize.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="admin-panel/css/main.css">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="admin-panel/css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="admin-panel/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="admin-panel/css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="admin-panel/css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="admin-panel/css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="admin-panel/css/calendar/fullcalendar.print.min.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="admin-panel/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="admin-panel/css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="admin-panel/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<div class="error-pagewrap">
		<div class="error-page-int">
			<div class="content-error">
				<h1>Server Error <span class="counter"> 500</span></h1>
				<p>The server encountered something unexpected that didn't allow it to complete the request. We apologize.</p>
				<a href="index.blade.php">Dashboard</a>
				<a href="#">Report Problem</a>
			</div>
			<div class="text-center login-footer">
				<p>Copyright © 2018. All rights reserved. Template by <a href="https://colorlib.com/wp/templates/">Colorlib</a></p>
			</div>
		</div>   
    </div>
    <!-- jquery
		============================================ -->
    <script src="admin-panel/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="admin-panel/js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="admin-panel/js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="admin-panel/js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="admin-panel/js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="admin-panel/js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="admin-panel/js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="admin-panel/js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="admin-panel/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="admin-panel/js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="admin-panel/js/metisMenu/metisMenu.min.js"></script>
    <script src="admin-panel/js/metisMenu/metisMenu-active.js"></script>
    <!-- counterup JS
		============================================ -->
    <script src="admin-panel/js/counterup/jquery.counterup.min.js"></script>
    <script src="admin-panel/js/counterup/waypoints.min.js"></script>
    <script src="admin-panel/js/counterup/counterup-active.js"></script>
    <!-- morrisjs JS
		============================================ -->
    <script src="admin-panel/js/sparkline/jquery.sparkline.min.js"></script>
    <script src="admin-panel/js/sparkline/jquery.charts-sparkline.js"></script>
    <!-- calendar JS
		============================================ -->
    <script src="admin-panel/js/calendar/moment.min.js"></script>
    <script src="admin-panel/js/calendar/fullcalendar.min.js"></script>
    <script src="admin-panel/js/calendar/fullcalendar-active.js"></script>
    <!-- tab JS
		============================================ -->
    <script src="admin-panel/js/tab.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="admin-panel/js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="admin-panel/js/main.js"></script>
	<!-- tawk chat JS
		============================================ -->
    <script src="admin-panel/js/tawk-chat.js"></script>
</body>

</html>