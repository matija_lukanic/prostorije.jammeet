<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Jammeet - @yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- favicon
		============================================ -->

    <link href='https://use.fontawesome.com/releases/v5.0.6/css/all.css' rel='stylesheet'>
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <link href="{{ URL::asset('calendar/packages/bootstrap/main.css') }}" rel='stylesheet' />
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/font-awesome.min.css') }}">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/owl.transitions.css') }}">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/animate.css') }}">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/normalize.css') }}">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/main.css') }}">
    <!-- educate icon CSS
		============================================ -->
    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/educate-custon-icon.css') }}">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/scrollbar/jquery.mCustomScrollbar.min.css') }}">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/metisMenu/metisMenu.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/metisMenu/metisMenu-vertical.css') }}">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="{{ URL::asset('admin-panel/style.css?v=1.9.20') }}">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/responsive.css') }}">

    <!-- modernizr JS
		============================================ -->
    <script src="{{ URL::asset('admin-panel/js/vendor/modernizr-2.8.3.min.js') }}"></script>

    <!-- added with less -->
    <link rel="stylesheet" href="{{ URL::asset('admin-panel/custom.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}">


    @yield('extra_scripts')

</head>

<body>
    <!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
    <!-- Start Left menu area -->
    <div class="left-sidebar-pro">
        <nav id="sidebar" class="">
            <div class="sidebar-header">
                <a href="{{url('/dashboard')}}"><img class="main-logo" src="{{ URL::asset('img/logo.png') }}" alt="" /></a>
            </div>
            <div class="left-custom-menu-adp-wrap comment-scrollbar">
                <nav class="sidebar-nav left-sidebar-menu-pro">
                    <ul class="metismenu" id="menu1">

                        <li class="{{ Request::is('/dashboard') ? 'bg-light' : '' }}">
                            <a href="{{url('/dashboard')}}">
                               <span class="educate-icon educate-home icon-wrap"></span>
                               <span class="mini-click-non">Dashboard</span>
                            </a>
                        </li>

                        <li class="{{ Request::is('kalendar') ? 'bg-light' : '' }}">
                            <a href="{{url('/kalendar')}}">
              								   <span class="educate-icon educate-event icon-wrap sub-icon-mg"></span>
                                                 <span class="mini-click-non">Kalendar</span>
              							</a>
                        </li>

                        <li class="{{ Request::segment(1) == 'prostorije' ? 'active' : '' }}">

                            <a class="has-arrow" href="" aria-expanded="false">
                                    <span class="fa fa-music icon-wrap"></span>
                                   <span class="mini-click-non">Prostorije</span>
                            </a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <li class="{{ Request::is('prostorije') ? 'bg-light' : '' }}"><a href="{{url('/prostorije')}}"><span class="mini-sub-pro">Sve prostorije</span></a></li>
                                <li class="{{ Request::is('prostorije/novo') ? 'bg-light' : '' }}"><a href="{{url('/prostorije/novo')}}"><span class="mini-sub-pro">Dodaj novu</span></a></li>
                            </ul>
                        </li>


                        <li class="{{ Request::is('bendovi') ? 'bg-light' : '' }}">
                            <a href="{{url('bendovi')}}" aria-expanded="false">
                            <span class="fa fa-users icon-wrap"></span>

                            <span class="mini-click-non">Bendovi</span></a>
                        </li>

                        <li class="{{ Request::segment(1) == 'postavke' ? 'active' : '' }}">

                            <a class="has-arrow" href="" aria-expanded="false">
                                <span class="fa fa-cogs icon-wrap"></span>
                                <span class="mini-click-non">Postavke</span>
                            </a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <li class="{{ Request::is('postavke/profil') ? 'bg-light' : '' }}"><a href="{{url('/postavke/profil')}}"><span class="mini-sub-pro">Profil</span></a></li>
                                <li class="{{ Request::is('postavke/probe') ? 'bg-light' : '' }}"><a href="{{url('/postavke/probe')}}"><span class="mini-sub-pro">Rezervacija proba</span></a></li>
                            </ul>
                        </li>

                        <!--

                            <li>
                                <a class="has-arrow" href="all-students.blade.php" aria-expanded="false"><span class="educate-icon educate-student icon-wrap"></span> <span class="mini-click-non">Students</span></a>
                                <ul class="submenu-angle" aria-expanded="false">
                                    <li><a title="All Students" href="all-students.blade.php"><span class="mini-sub-pro">All Students</span></a></li>
                                    <li><a title="Add Students" href="add-student.blade.php"><span class="mini-sub-pro">Add Student</span></a></li>
                                    <li><a title="Edit Students" href="edit-student.blade.php"><span class="mini-sub-pro">Edit Student</span></a></li>
                                    <li><a title="Students Profile" href="student-profile.blade.php"><span class="mini-sub-pro">Student Profile</span></a></li>
                                </ul>
                            </li>

                            <li>
                                <a class="has-arrow" href="all-students.blade.php" aria-expanded="false">
                                <span class="educate-icon educate-professor icon-wrap"></span> <span class="mini-click-non">Profesors</span></a>
                            </li>
                            <li>
                                <a class="has-arrow" href="all-courses.blade.php" aria-expanded="false"><span class="educate-icon educate-course icon-wrap"></span> <span class="mini-click-non">Courses</span></a>
                                <ul class="submenu-angle" aria-expanded="false">
                                    <li><a title="All Courses" href="all-courses.blade.php"><span class="mini-sub-pro">All Courses</span></a></li>
                                    <li><a title="Add Courses" href="add-course.blade.php"><span class="mini-sub-pro">Add Course</span></a></li>
                                    <li><a title="Edit Courses" href="edit-course.blade.php"><span class="mini-sub-pro">Edit Course</span></a></li>
                                    <li><a title="Courses Profile" href="course-info.blade.php"><span class="mini-sub-pro">Courses Info</span></a></li>
                                    <li><a title="Product Payment" href="course-payment.blade.php"><span class="mini-sub-pro">Courses Payment</span></a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow" href="all-courses.blade.php" aria-expanded="false"><span class="educate-icon educate-library icon-wrap"></span> <span class="mini-click-non">Library</span></a>
                                <ul class="submenu-angle" aria-expanded="false">
                                    <li><a title="All Library" href="library-assets.blade.php"><span class="mini-sub-pro">Library Assets</span></a></li>
                                    <li><a title="Add Library" href="add-library-assets.blade.php"><span class="mini-sub-pro">Add Library Asset</span></a></li>
                                    <li><a title="Edit Library" href="edit-library-assets.blade.php"><span class="mini-sub-pro">Edit Library Asset</span></a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow" href="all-courses.blade.php" aria-expanded="false"><span class="educate-icon educate-department icon-wrap"></span> <span class="mini-click-non">Departments</span></a>
                                <ul class="submenu-angle" aria-expanded="false">
                                    <li><a title="Departments List" href="departments.blade.php"><span class="mini-sub-pro">Departments List</span></a></li>
                                    <li><a title="Add Departments" href="add-department.blade.php"><span class="mini-sub-pro">Add Departments</span></a></li>
                                    <li><a title="Edit Departments" href="edit-department.blade.php"><span class="mini-sub-pro">Edit Departments</span></a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow" href="mailbox.blade.php" aria-expanded="false"><span class="educate-icon educate-message icon-wrap"></span> <span class="mini-click-non">Mailbox</span></a>
                                <ul class="submenu-angle" aria-expanded="false">
                                    <li><a title="Inbox" href="mailbox.blade.php"><span class="mini-sub-pro">Inbox</span></a></li>
                                    <li><a title="View Mail" href="mailbox-view.blade.php"><span class="mini-sub-pro">View Mail</span></a></li>
                                    <li><a title="Compose Mail" href="mailbox-compose.blade.php"><span class="mini-sub-pro">Compose Mail</span></a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow" href="mailbox.blade.php" aria-expanded="false"><span class="educate-icon educate-interface icon-wrap"></span> <span class="mini-click-non">Interface</span></a>
                                <ul class="submenu-angle interface-mini-nb-dp" aria-expanded="false">
                                    <li><a title="Google Map" href="google-map.blade.php"><span class="mini-sub-pro">Google Map</span></a></li>
                                    <li><a title="Data Maps" href="data-maps.blade.php"><span class="mini-sub-pro">Data Maps</span></a></li>
                                    <li><a title="Pdf Viewer" href="pdf-viewer.blade.php"><span class="mini-sub-pro">Pdf Viewer</span></a></li>
                                    <li><a title="X-Editable" href="x-editable.blade.php"><span class="mini-sub-pro">X-Editable</span></a></li>
                                    <li><a title="Code Editor" href="code-editor.blade.php"><span class="mini-sub-pro">Code Editor</span></a></li>
                                    <li><a title="Tree View" href="tree-view.blade.php"><span class="mini-sub-pro">Tree View</span></a></li>
                                    <li><a title="Preloader" href="preloader.blade.php"><span class="mini-sub-pro">Preloader</span></a></li>@
                                    <li><a title="Images Cropper" href="images-cropper.blade.php"><span class="mini-sub-pro">Images Cropper</span></a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow" href="mailbox.blade.php" aria-expanded="false"><span class="educate-icon educate-charts icon-wrap"></span> <span class="mini-click-non">Charts</span></a>
                                <ul class="submenu-angle chart-mini-nb-dp" aria-expanded="false">
                                    <li><a title="Bar Charts" href="bar-charts.blade.php"><span class="mini-sub-pro">Bar Charts</span></a></li>
                                    <li><a title="Line Charts" href="line-charts.blade.php"><span class="mini-sub-pro">Line Charts</span></a></li>
                                    <li><a title="Area Charts" href="area-charts.blade.php"><span class="mini-sub-pro">Area Charts</span></a></li>
                                    <li><a title="Rounded Charts" href="rounded-chart.blade.php"><span class="mini-sub-pro">Rounded Charts</span></a></li>
                                    <li><a title="C3 Charts" href="c3.blade.php"><span class="mini-sub-pro">C3 Charts</span></a></li>
                                    <li><a title="Sparkline Charts" href="sparkline.blade.php"><span class="mini-sub-pro">Sparkline Charts</span></a></li>
                                    <li><a title="Peity Charts" href="peity.blade.php"><span class="mini-sub-pro">Peity Charts</span></a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow" href="mailbox.blade.php" aria-expanded="false"><span class="educate-icon educate-data-table icon-wrap"></span> <span class="mini-click-non">Data Tables</span></a>
                                <ul class="submenu-angle" aria-expanded="false">
                                    <li><a title="Peity Charts" href="static-table.blade.php"><span class="mini-sub-pro">Static Table</span></a></li>
                                    <li><a title="Data Table" href="data-table.blade.php"><span class="mini-sub-pro">Data Table</span></a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow" href="mailbox.blade.php" aria-expanded="false"><span class="educate-icon educate-form icon-wrap"></span> <span class="mini-click-non">Forms Elements</span></a>
                                <ul class="submenu-angle form-mini-nb-dp" aria-expanded="false">
                                    <li><a title="Basic Form Elements" href="basic-form-element.blade.php"><span class="mini-sub-pro">Bc Form Elements</span></a></li>
                                    <li><a title="Advance Form Elements" href="advance-form-element.blade.php"><span class="mini-sub-pro">Ad Form Elements</span></a></li>
                                    <li><a title="Password Meter" href="password-meter.blade.php"><span class="mini-sub-pro">Password Meter</span></a></li>
                                    <li><a title="Multi Upload" href="multi-upload.blade.php"><span class="mini-sub-pro">Multi Upload</span></a></li>
                                    <li><a title="Text Editor" href="tinymc.blade.php"><span class="mini-sub-pro">Text Editor</span></a></li>
                                    <li><a title="Dual List Box" href="dual-list-box.blade.php"><span class="mini-sub-pro">Dual List Box</span></a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow" href="mailbox.blade.php" aria-expanded="false"><span class="educate-icon educate-apps icon-wrap"></span> <span class="mini-click-non">App views</span></a>
                                <ul class="submenu-angle app-mini-nb-dp" aria-expanded="false">
                                    <li><a title="Notifications" href="notifications.blade.php"><span class="mini-sub-pro">Notifications</span></a></li>
                                    <li><a title="Alerts" href="alerts.blade.php"><span class="mini-sub-pro">Alerts</span></a></li>
                                    <li><a title="Modals" href="modals.blade.php"><span class="mini-sub-pro">Modals</span></a></li>
                                    <li><a title="Buttons" href="buttons.blade.php"><span class="mini-sub-pro">Buttons</span></a></li>
                                    <li><a title="Tabs" href="tabs.blade.php"><span class="mini-sub-pro">Tabs</span></a></li>
                                    <li><a title="Accordion" href="accordion.blade.php"><span class="mini-sub-pro">Accordion</span></a></li>
                                </ul>
                            </li>
                            <li id="removable">
                                <a class="has-arrow" href="#" aria-expanded="false"><span class="educate-icon educate-pages icon-wrap"></span> <span class="mini-click-non">Pages</span></a>
                                <ul class="submenu-angle page-mini-nb-dp" aria-expanded="false">
                                    <li><a title="Login" href="login.blade.php"><span class="mini-sub-pro">Login</span></a></li>
                                    <li><a title="Register" href="register.blade.php"><span class="mini-sub-pro">Register</span></a></li>
                                    <li><a title="Lock" href="lock.blade.php"><span class="mini-sub-pro">Lock</span></a></li>
                                    <li><a title="Password Recovery" href="password-recovery.blade.php"><span class="mini-sub-pro">Password Recovery</span></a></li>
                                    <li><a title="404 Page" href="404.blade.php"><span class="mini-sub-pro">404 Page</span></a></li>
                                    <li><a title="500 Page" href="500.blade.php"><span class="mini-sub-pro">500 Page</span></a></li>
                                </ul>
                            </li>
                        -->

                    </ul>
                </nav>
            </div>
        </nav>
    </div>
    <!-- End Left menu area -->
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="header-advance-area">
            <div class="header-top-area d-none d-md-block">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header-top-wraper">
                                <div class="row">
                                    <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                                        <div class="menu-switcher-pro">
                                            <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
													<i class="educate-icon educate-nav"></i>
												</button>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-7 col-sm-6 col-xs-12">
                                        <div class="header-top-menu tabl-d-n">
                                            <ul class="nav navbar-nav mai-top-nav flex-row">
                                                <li class="nav-item"><a href="#" class="nav-link">Home</a>
                                                </li>
                                                <li class="nav-item"><a href="#" class="nav-link">About</a>
                                                </li>
                                                <li class="nav-item"><a href="#" class="nav-link">Services</a>
                                                </li>
                                                <li class="nav-item dropdown res-dis-nn">
                                                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">Project <span class="angle-down-topmenu"><i class="fa fa-angle-down"></i></span></a>
                                                    <div role="menu" class="dropdown-menu ">
                                                        <a href="#" class="dropdown-item">Documentation</a>
                                                        <a href="#" class="dropdown-item">Expert Backend</a>
                                                        <a href="#" class="dropdown-item">Expert FrontEnd</a>
                                                        <a href="#" class="dropdown-item">Contact Support</a>
                                                    </div>
                                                </li>
                                                <li class="nav-item"><a href="#" class="nav-link">Support</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                        <div class="header-right-info">
                                            <ul class="nav navbar-nav mai-top-nav header-right-menu flex-row d-flex align-items-center">
                                                <li class="nav-item dropdown">
                                                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="educate-icon educate-message edu-chat-pro" aria-hidden="true"></i><span class="indicator-ms"></span></a>
                                                    <div role="menu" class="mt-2 author-message-top dropdown-menu ">
                                                        <div class="message-single-top">
                                                            <h1>Message</h1>
                                                        </div>
                                                        <ul class="message-menu">
                                                            <li>
                                                                <a href="#">
                                                                    <div class="message-img">
                                                                        <img src="admin-panel/img/contact/1.jpg" alt="">
                                                                    </div>
                                                                    <div class="message-content">
                                                                        <span class="message-date">16 Sept</span>
                                                                        <h2>Advanda Cro</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <div class="message-img">
                                                                        <img src="admin-panel/img/contact/4.jpg" alt="">
                                                                    </div>
                                                                    <div class="message-content">
                                                                        <span class="message-date">16 Sept</span>
                                                                        <h2>Sulaiman din</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <div class="message-img">
                                                                        <img src="admin-panel/img/contact/3.jpg" alt="">
                                                                    </div>
                                                                    <div class="message-content">
                                                                        <span class="message-date">16 Sept</span>
                                                                        <h2>Victor Jara</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <div class="message-img">
                                                                        <img src="admin-panel/img/contact/2.jpg" alt="">
                                                                    </div>
                                                                    <div class="message-content">
                                                                        <span class="message-date">16 Sept</span>
                                                                        <h2>Victor Jara</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <div class="message-view">
                                                            <a href="#">View All Messages</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="educate-icon educate-bell" aria-hidden="true"></i><span class="indicator-nt"></span></a>
                                                    <div role="menu" class="mt-2 notification-author dropdown-menu ">
                                                        <div class="notification-single-top">
                                                            <h1>Notifications</h1>
                                                        </div>
                                                        <ul class="notification-menu">
                                                            <li>
                                                                <a href="#">
                                                                    <div class="notification-icon">
                                                                        <i class="educate-icon educate-checked edu-checked-pro admin-check-pro" aria-hidden="true"></i>
                                                                    </div>
                                                                    <div class="notification-content">
                                                                        <span class="notification-date">16 Sept</span>
                                                                        <h2>Advanda Cro</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <div class="notification-icon">
                                                                        <i class="fa fa-cloud edu-cloud-computing-down" aria-hidden="true"></i>
                                                                    </div>
                                                                    <div class="notification-content">
                                                                        <span class="notification-date">16 Sept</span>
                                                                        <h2>Sulaiman din</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <div class="notification-icon">
                                                                        <i class="fa fa-eraser edu-shield" aria-hidden="true"></i>
                                                                    </div>
                                                                    <div class="notification-content">
                                                                        <span class="notification-date">16 Sept</span>
                                                                        <h2>Victor Jara</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <div class="notification-icon">
                                                                        <i class="fa fa-line-chart edu-analytics-arrow" aria-hidden="true"></i>
                                                                    </div>
                                                                    <div class="notification-content">
                                                                        <span class="notification-date">16 Sept</span>
                                                                        <h2>Victor Jara</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <div class="notification-view">
                                                            <a href="#">View All Notification</a>
                                                        </div>
                                                    </div>
                                                </li>


                                                @if(Auth::check())

                                                    <li class="nav-item">
                                                        <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle d-flex align-items-center">
                                                                <img class="mr-2" src="{{ URL::asset('uploads/prostorije') }}/{{Auth::user()->getSmProfile()}}" alt="" />
                                                                <span class="admin-name">{{$user->name}}</span>
                                                                <i class="fa fa-angle-down edu-icon edu-down-arrow"></i>
                                                            </a>
                                                        <ul role="menu" class="mt-2 dropdown-header-top author-log dropdown-menu ">
                                                            <li><a href="#"><span class="edu-icon edu-home-admin author-log-ic"></span>My Account</a>
                                                            </li>
                                                            <li><a href="#"><span class="edu-icon edu-user-rounded author-log-ic"></span>My Profile</a>
                                                            </li>
                                                            <li><a href="#"><span class="edu-icon edu-money author-log-ic"></span>User Billing</a>
                                                            </li>
                                                            <li><a href="#"><span class="edu-icon edu-settings author-log-ic"></span>Settings</a>
                                                            </li>
                                                            <li><a href="{{ url('/logout') }}"><span class="edu-icon edu-locked author-log-ic"></span>Log Out</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li class="nav-item nav-setting-open">
                                                      <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                                        <i class="educate-icon educate-menu"></i></a>

                                                        <div role="menu" class="mt-2 admintab-wrap menu-setting-wrap menu-setting-wrap-bg dropdown-menu ">
                                                            <ul class="nav nav-tabs custon-set-tab">
                                                                <li class="active"><a data-toggle="tab" href="#Notes">Notes</a>
                                                                </li>
                                                                <li><a data-toggle="tab" href="#Projects">Projects</a>
                                                                </li>
                                                                <li><a data-toggle="tab" href="#Settings">Settings</a>
                                                                </li>
                                                            </ul>

                                                            <div class="tab-content custom-bdr-nt">
                                                                <div id="Notes" class="tab-pane fade in active">
                                                                    <div class="notes-area-wrap">
                                                                        <div class="note-heading-indicate">
                                                                            <h2><i class="fa fa-comments-o"></i> Latest Notes</h2>
                                                                            <p>You have 10 new message.</p>
                                                                        </div>
                                                                        <div class="notes-list-area notes-menu-scrollbar">
                                                                            <ul class="notes-menu-list">
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="notes-list-flow">
                                                                                            <div class="notes-img">
                                                                                                <img src="admin-panel/img/contact/4.jpg" alt="" />
                                                                                            </div>
                                                                                            <div class="notes-content">
                                                                                                <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                                <span>Yesterday 2:45 pm</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="notes-list-flow">
                                                                                            <div class="notes-img">
                                                                                                <img src="admin-panel/img/contact/1.jpg" alt="" />
                                                                                            </div>
                                                                                            <div class="notes-content">
                                                                                                <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                                <span>Yesterday 2:45 pm</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="notes-list-flow">
                                                                                            <div class="notes-img">
                                                                                                <img src="admin-panel/img/contact/2.jpg" alt="" />
                                                                                            </div>
                                                                                            <div class="notes-content">
                                                                                                <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                                <span>Yesterday 2:45 pm</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="notes-list-flow">
                                                                                            <div class="notes-img">
                                                                                                <img src="admin-panel/img/contact/3.jpg" alt="" />
                                                                                            </div>
                                                                                            <div class="notes-content">
                                                                                                <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                                <span>Yesterday 2:45 pm</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="notes-list-flow">
                                                                                            <div class="notes-img">
                                                                                                <img src="admin-panel/img/contact/4.jpg" alt="" />
                                                                                            </div>
                                                                                            <div class="notes-content">
                                                                                                <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                                <span>Yesterday 2:45 pm</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="notes-list-flow">
                                                                                            <div class="notes-img">
                                                                                                <img src="admin-panel/img/contact/1.jpg" alt="" />
                                                                                            </div>
                                                                                            <div class="notes-content">
                                                                                                <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                                <span>Yesterday 2:45 pm</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="notes-list-flow">
                                                                                            <div class="notes-img">
                                                                                                <img src="admin-panel/img/contact/2.jpg" alt="" />
                                                                                            </div>
                                                                                            <div class="notes-content">
                                                                                                <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                                <span>Yesterday 2:45 pm</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="notes-list-flow">
                                                                                            <div class="notes-img">
                                                                                                <img src="admin-panel/img/contact/1.jpg" alt="" />
                                                                                            </div>
                                                                                            <div class="notes-content">
                                                                                                <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                                <span>Yesterday 2:45 pm</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="notes-list-flow">
                                                                                            <div class="notes-img">
                                                                                                <img src="admin-panel/img/contact/2.jpg" alt="" />
                                                                                            </div>
                                                                                            <div class="notes-content">
                                                                                                <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                                <span>Yesterday 2:45 pm</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="notes-list-flow">
                                                                                            <div class="notes-img">
                                                                                                <img src="admin-panel/img/contact/3.jpg" alt="" />
                                                                                            </div>
                                                                                            <div class="notes-content">
                                                                                                <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                                <span>Yesterday 2:45 pm</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="Projects" class="tab-pane fade">
                                                                    <div class="projects-settings-wrap">
                                                                        <div class="note-heading-indicate">
                                                                            <h2><i class="fa fa-cube"></i> Latest projects</h2>
                                                                            <p> You have 20 projects. 5 not completed.</p>
                                                                        </div>
                                                                        <div class="project-st-list-area project-st-menu-scrollbar">
                                                                            <ul class="projects-st-menu-list">
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="project-list-flow">
                                                                                            <div class="projects-st-heading">
                                                                                                <h2>Web Development</h2>
                                                                                                <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                                                                                <span class="project-st-time">1 hours ago</span>
                                                                                            </div>
                                                                                            <div class="projects-st-content">
                                                                                                <p>Completion with: 28%</p>
                                                                                                <div class="progress progress-mini">
                                                                                                    <div style="width: 28%;" class="progress-bar progress-bar-danger hd-tp-1"></div>
                                                                                                </div>
                                                                                                <p>Project end: 4:00 pm - 12.06.2014</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="project-list-flow">
                                                                                            <div class="projects-st-heading">
                                                                                                <h2>Software Development</h2>
                                                                                                <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                                                                                <span class="project-st-time">2 hours ago</span>
                                                                                            </div>
                                                                                            <div class="projects-st-content project-rating-cl">
                                                                                                <p>Completion with: 68%</p>
                                                                                                <div class="progress progress-mini">
                                                                                                    <div style="width: 68%;" class="progress-bar hd-tp-2"></div>
                                                                                                </div>
                                                                                                <p>Project end: 4:00 pm - 12.06.2014</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="project-list-flow">
                                                                                            <div class="projects-st-heading">
                                                                                                <h2>Graphic Design</h2>
                                                                                                <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                                                                                <span class="project-st-time">3 hours ago</span>
                                                                                            </div>
                                                                                            <div class="projects-st-content">
                                                                                                <p>Completion with: 78%</p>
                                                                                                <div class="progress progress-mini">
                                                                                                    <div style="width: 78%;" class="progress-bar hd-tp-3"></div>
                                                                                                </div>
                                                                                                <p>Project end: 4:00 pm - 12.06.2014</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="project-list-flow">
                                                                                            <div class="projects-st-heading">
                                                                                                <h2>Web Design</h2>
                                                                                                <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                                                                                <span class="project-st-time">4 hours ago</span>
                                                                                            </div>
                                                                                            <div class="projects-st-content project-rating-cl2">
                                                                                                <p>Completion with: 38%</p>
                                                                                                <div class="progress progress-mini">
                                                                                                    <div style="width: 38%;" class="progress-bar progress-bar-danger hd-tp-4"></div>
                                                                                                </div>
                                                                                                <p>Project end: 4:00 pm - 12.06.2014</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="project-list-flow">
                                                                                            <div class="projects-st-heading">
                                                                                                <h2>Business Card</h2>
                                                                                                <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                                                                                <span class="project-st-time">5 hours ago</span>
                                                                                            </div>
                                                                                            <div class="projects-st-content">
                                                                                                <p>Completion with: 28%</p>
                                                                                                <div class="progress progress-mini">
                                                                                                    <div style="width: 28%;" class="progress-bar progress-bar-danger hd-tp-5"></div>
                                                                                                </div>
                                                                                                <p>Project end: 4:00 pm - 12.06.2014</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="project-list-flow">
                                                                                            <div class="projects-st-heading">
                                                                                                <h2>Ecommerce Business</h2>
                                                                                                <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                                                                                <span class="project-st-time">6 hours ago</span>
                                                                                            </div>
                                                                                            <div class="projects-st-content project-rating-cl">
                                                                                                <p>Completion with: 68%</p>
                                                                                                <div class="progress progress-mini">
                                                                                                    <div style="width: 68%;" class="progress-bar hd-tp-6"></div>
                                                                                                </div>
                                                                                                <p>Project end: 4:00 pm - 12.06.2014</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="project-list-flow">
                                                                                            <div class="projects-st-heading">
                                                                                                <h2>Woocommerce Plugin</h2>
                                                                                                <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                                                                                <span class="project-st-time">7 hours ago</span>
                                                                                            </div>
                                                                                            <div class="projects-st-content">
                                                                                                <p>Completion with: 78%</p>
                                                                                                <div class="progress progress-mini">
                                                                                                    <div style="width: 78%;" class="progress-bar"></div>
                                                                                                </div>
                                                                                                <p>Project end: 4:00 pm - 12.06.2014</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                                        <div class="project-list-flow">
                                                                                            <div class="projects-st-heading">
                                                                                                <h2>Wordpress Theme</h2>
                                                                                                <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                                                                                <span class="project-st-time">9 hours ago</span>
                                                                                            </div>
                                                                                            <div class="projects-st-content project-rating-cl2">
                                                                                                <p>Completion with: 38%</p>
                                                                                                <div class="progress progress-mini">
                                                                                                    <div style="width: 38%;" class="progress-bar progress-bar-danger"></div>
                                                                                                </div>
                                                                                                <p>Project end: 4:00 pm - 12.06.2014</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="Settings" class="tab-pane fade">
                                                                    <div class="setting-panel-area">
                                                                        <div class="note-heading-indicate">
                                                                            <h2><i class="fa fa-gears"></i> Settings Panel</h2>
                                                                            <p> You have 20 Settings. 5 not completed.</p>
                                                                        </div>
                                                                        <ul class="setting-panel-list">
                                                                            <li>
                                                                                <div class="checkbox-setting-pro">
                                                                                    <div class="checkbox-title-pro">
                                                                                        <h2>Show notifications</h2>
                                                                                        <div class="ts-custom-check">
                                                                                            <div class="onoffswitch">
                                                                                                <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example">
                                                                                                <label class="onoffswitch-label" for="example">
                                                                                                        <span class="onoffswitch-inner"></span>
                                                                                                        <span class="onoffswitch-switch"></span>
                                                                                                    </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="checkbox-setting-pro">
                                                                                    <div class="checkbox-title-pro">
                                                                                        <h2>Disable Chat</h2>
                                                                                        <div class="ts-custom-check">
                                                                                            <div class="onoffswitch">
                                                                                                <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example3">
                                                                                                <label class="onoffswitch-label" for="example3">
                                                                                                        <span class="onoffswitch-inner"></span>
                                                                                                        <span class="onoffswitch-switch"></span>
                                                                                                    </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="checkbox-setting-pro">
                                                                                    <div class="checkbox-title-pro">
                                                                                        <h2>Enable history</h2>
                                                                                        <div class="ts-custom-check">
                                                                                            <div class="onoffswitch">
                                                                                                <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example4">
                                                                                                <label class="onoffswitch-label" for="example4">
                                                                                                        <span class="onoffswitch-inner"></span>
                                                                                                        <span class="onoffswitch-switch"></span>
                                                                                                    </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="checkbox-setting-pro">
                                                                                    <div class="checkbox-title-pro">
                                                                                        <h2>Show charts</h2>
                                                                                        <div class="ts-custom-check">
                                                                                            <div class="onoffswitch">
                                                                                                <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example7">
                                                                                                <label class="onoffswitch-label" for="example7">
                                                                                                        <span class="onoffswitch-inner"></span>
                                                                                                        <span class="onoffswitch-switch"></span>
                                                                                                    </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="checkbox-setting-pro">
                                                                                    <div class="checkbox-title-pro">
                                                                                        <h2>Update everyday</h2>
                                                                                        <div class="ts-custom-check">
                                                                                            <div class="onoffswitch">
                                                                                                <input type="checkbox" name="collapsemenu" checked="" class="onoffswitch-checkbox" id="example2">
                                                                                                <label class="onoffswitch-label" for="example2">
                                                                                                        <span class="onoffswitch-inner"></span>
                                                                                                        <span class="onoffswitch-switch"></span>
                                                                                                    </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="checkbox-setting-pro">
                                                                                    <div class="checkbox-title-pro">
                                                                                        <h2>Global search</h2>
                                                                                        <div class="ts-custom-check">
                                                                                            <div class="onoffswitch">
                                                                                                <input type="checkbox" name="collapsemenu" checked="" class="onoffswitch-checkbox" id="example6">
                                                                                                <label class="onoffswitch-label" for="example6">
                                                                                                        <span class="onoffswitch-inner"></span>
                                                                                                        <span class="onoffswitch-switch"></span>
                                                                                                    </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="checkbox-setting-pro">
                                                                                    <div class="checkbox-title-pro">
                                                                                        <h2>Offline users</h2>
                                                                                        <div class="ts-custom-check">
                                                                                            <div class="onoffswitch">
                                                                                                <input type="checkbox" name="collapsemenu" checked="" class="onoffswitch-checkbox" id="example5">
                                                                                                <label class="onoffswitch-label" for="example5">
                                                                                                        <span class="onoffswitch-inner"></span>
                                                                                                        <span class="onoffswitch-switch"></span>
                                                                                                    </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @else
                                                    <li class="nav-item">
                                                        <a class="text-center-sm {{ Request::is('login') ? 'active' : '' }}" href="{{ url('/login') }}">Login</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="text-center-sm {{ Request::is('register') ? 'active' : '' }}" href="{{ url('/register') }}">Registracija</a>
                                                    </li>


                                                @endif

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <nav class="navbar navbar-full navbar-dark bg-dark flex-column d-md-none fixed-top p-0">
              <div class="d-flex align-items-center w-100 justify-content-end py-2 px-3">
                <img width="25" height="25" class="mr-2 img-fluid rounded-circle" src="{{ URL::asset('admin-panel/img/product/pro4.jpg') }}" alt="" />
                <span class="admin-name text-white">{{$user->name}}</span>
                	<button class="navbar-toggler hidden-lg-up ml-3" type="button" data-toggle="collapse" data-target="#mainNavbarCollapse">
                	&#9776;
                	</button>
              </div>
            	<div class="collapse navbar-toggleable-md w-100" id="mainNavbarCollapse">
            		<ul class="nav navbar-nav pull-lg-right w-100 text-center">

            			<li class="nav-item {{ Request::is('/dashboard') ? 'active' : '' }} border-top border-dark">
            				<a class="nav-link" href="{{url('/dashboard')}}">Dashboard</a>
            			</li>
            			<li class="nav-item {{ Request::is('kalendar') ? 'active' : '' }} border-top border-dark">
            				<a class="nav-link" href="{{url('/kalendar')}}">Kalendar</a>
            			</li>
            			<li class="nav-item {{ Request::segment(1) == 'prostorije' ? 'active' : '' }} border-top border-dark">
            				<a class="nav-link" href="{{url('/prostorije')}}">Sve prostorije</a>
            			</li>
                </li>
                <li class="nav-item {{ Request::is('bendovi') ? 'active' : '' }} border-top border-dark">
                  <a href="{{ url('/bendovi') }}" class="nav-link">Bendovi</a>
                </li>
            			<li class="nav-item border-top border-dark">
            				<a href="{{ url('/logout') }}" class="nav-link">Log Out</a>
            			</li>

            		</ul>
            	</div>
            </nav>


            <!-- Search div
            <div class="breadcome-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="breadcome-heading">
                                            <form role="search" class="sr-input-func">
                                                <input type="text" placeholder="Search..." class="search-int form-control">
                                                <a href="#"><i class="fa fa-search"></i></a>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <ul class="breadcome-menu">
                                            <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                            </li>
                                            <li><span class="bread-blod">Dashboard V.1</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            -->


        </div>

        <div class="container-fluid">
            <div class="p-3 pt-xl-5"></div>
            <div class="p1169 pt-xl-4"></div>
            @yield('content')
        </div>

    </div>

    <!-- jquery
		============================================ -->
    <script src="{{ URL::asset('admin-panel/js/vendor/jquery-1.12.4.min.js') }}"></script>

    <!-- popper
		============================================ -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <!-- bootstrap JS
		============================================ -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- wow JS
		============================================ -->
    <script src="{{ URL::asset('admin-panel/js/wow.min.js') }}"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="{{ URL::asset('admin-panel/js/jquery-price-slider.js') }}"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="{{ URL::asset('admin-panel/js/owl.carousel.min.js') }}"></script>
    <!-- sticky JS
		============================================ -->
    <script src="{{ URL::asset('admin-panel/js/jquery.sticky.js') }}"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="{{ URL::asset('admin-panel/js/jquery.scrollUp.min.js') }}"></script>
    <!-- counterup JS
		============================================ -->
    <script src="{{ URL::asset('admin-panel/js/counterup/jquery.counterup.min.js') }}"></script>
    <script src="{{ URL::asset('admin-panel/js/counterup/waypoints.min.js') }}"></script>
    <script src="{{ URL::asset('admin-panel/js/counterup/counterup-active.js') }}"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="{{ URL::asset('admin-panel/js/scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ URL::asset('admin-panel/js/scrollbar/mCustomScrollbar-active.js') }}"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="{{ URL::asset('admin-panel/js/metisMenu/metisMenu.min.js') }}"></script>
    <script src="{{ URL::asset('admin-panel/js/metisMenu/metisMenu-active.js') }}"></script>

    <!-- plugins JS
		============================================ -->
    <script src="{{ URL::asset('admin-panel/js/plugins.js') }}"></script>



    <!-- main JS
		============================================ -->
    <script src="{{ URL::asset('admin-panel/js/main.js') }}"></script>


    <!-- custom js -->
    <script src="{{ URL::asset('js/custom.js') }}"></script>
    <!-- tawk chat JS
    <script src="{{ URL::asset('admin-panel/js/tawk-chat.js') }}"></script>
        ============================================ -->
    <script>
        if (typeof active_user === 'undefined') {
            var active_user = <?php if(Auth::check()){echo json_encode(Auth::user());} else {echo json_encode(null);} ?>
        }
    </script>

    @yield('extra_scripts2')

</body>

</html>
