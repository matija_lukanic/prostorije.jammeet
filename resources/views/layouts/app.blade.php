<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Jammeet Prostorije') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('selectizejs/custom.css') }}" rel="stylesheet">

    <!-- Bootstrap CSS -->

    <link href="{{ URL::asset('calendar/packages/bootstrap/main.css') }}" rel='stylesheet' />


    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css" />
    <!-- Third party plugin CSS-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="{{ URL::asset('startbootstrap/css/styles.css') }}" rel='stylesheet' />

    @yield('extra_scripts')
</head>
<body id="page-top">
    <div id="app">

        <nav class="navbar navbar-expand-lg navbar-light  {{ Request::is('/') ? 'fixed-top' : 'bg-white shadow-sm' }}"  id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="{{url('/')}}">
                    <img width="160" class="img-fluid" src="{{ URL::asset('img/logo-w.png') }}">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">Prijava</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">Registracija</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown d-none d-lg-block">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <img class="rounded-circle" width="32" height="32" src="{{ URL::asset('uploads/prostorije') }}/{{Auth::user()->getSmProfile()}}" alt="" />
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div role="menu"  class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                    <a class="dropdown-item" href="{{url('/dashboard')}}">
                                        Dashboard
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>

                            <li class="nav-item d-lg-none">
                                <a class="nav-link" href="{{url('/dashboard')}}">
                                    <img class="rounded-circle" width="32" height="32" src="{{ URL::asset('uploads/prostorije') }}/{{Auth::user()->getSmProfile()}}" alt="" />
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                            </li>
                            <li class="nav-item d-lg-none">
                                <a class="nav-link" href="{{ url('/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="nav-item d-lg-none">
                                <a class="nav-link"  href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                            </li>

                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main>
            @yield('content')
        </main>
    </div>


    <!-- jquery
		============================================ -->
    <script src="{{ URL::asset('admin-panel/js/vendor/jquery-1.12.4.min.js') }}"></script>


    <script src= "{{ URL::asset('selectizejs/dist/js/standalone/selectize.js') }}" ></script>



    <script>


        $(document).ready(function () {

            var arr_selectize_mjesto = ['#mjesto_selectize'];


            $.each(arr_selectize_mjesto, function(ind, sel){

                $(sel).selectize({
                    maxItems:2,
                    plugins: ['remove_button'],
                    onItemAdd(value, $item)
                    {
                        var selectize = $(sel)[0].selectize;
                        var arr_s = selectize.getValue();

                        if(arr_s.length>1){
                            selectize.removeItem(arr_s[0]);
                        }

                    },
                    onItemRemove(value)
                    {
                        var selectize = $(sel)[0].selectize;
                        if(selectize.getValue().length==0)
                        {
                            selectize.addItem(value);
                        }
                    },
                    onBlur()
                    {
                        var selectize = $(sel)[0].selectize;
                        if(selectize.getValue().length==0)
                        {
                            selectize.addItem(last_value);
                        }
                    },
                });

            });
        });


    </script>

    <!-- bootstrap JS
		============================================ -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Third party plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <!-- Core theme JS-->
    <script src="{{ URL::asset('startbootstrap/js/scripts.js') }}"></script>

<script>
    $(document).ready(function (){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    })
</script>

</body>
</html>
