@extends('layouts.admin')


@section('extra_scripts')

<style>
    .social-media-in a{
        background: #dd2121;
    }
    .social-media-in a:hover{
        background: #c61d1d;
    }
    .contact-panel-cs .contact-footer, .contact-panel-cs  div.panel-footer{
        background: #dd2121;
    }
    .breadcome-list{
        margin-top: 60px;
    }
    .dots{

    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    }
</style>

@endsection

@section('title', 'Jammeet- lista bendova')

@section('content')

    <!-- Modal -->
    <div class="modal fade edit-bend" id="bendModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <input name="user_id" type="number" class="d-none" value="{{$user->id}}">
                    <input name="jammeet_id" type="number" class="d-none">

                    <form>

                        @csrf


                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Status</label>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="category0" name="category" class="custom-control-input" value="0">
                                <label class="custom-control-label" for="category0">Bez statusa</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="category1" name="category" class="custom-control-input" value="1">
                                <label class="custom-control-label" for="category1">Dodaj bend u whitelist</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="category2" name="category" class="custom-control-input" value="2">
                                <label class="custom-control-label" for="category2">Dodaj bend u blacklist</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Napomene</label>
                            <textarea name="notes" class="form-control" id="exampleFormControlTextarea1" rows="4"></textarea>
                        </div>
                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
                    <button type="button" class="btn btn-primary save-bend-info" data-dismiss="modal">Spremi promjene</button>
                </div>
            </div>
        </div>
    </div>

        <div class="contacts-area mt-5 mt-md-0 mt-xl-4">
            <div class="container-fluid">

                <div class="row">
                  @foreach($bendovi as $index => $bend)

                      <?php

                        $bendSettings = App\BendSettings::where('user_id', $user->id)->where('jammeet_id', $bend['id'])->first();

                        ?>

                      <div class="col-xxl-3 col-xl-4 col-md-6 col-sm-6 col-xs-12 mb-4 bend-div{{$bend['id']}}">

                          <div class="hpanel hblue contact-panel contact-panel-cs responsive-mg-b-30 shadow p-0">
                              <div class="panel-body custom-panel-jw ">

                                  <div class="social-media-in">

                                     @if (array_key_exists('facebook', $bend))

                                        @if (strpos($bend->facebook, 'https://') === false)
                                          @php $bend->facebook = 'https://'.$bend->facebook; @endphp
                                        @endif

                                        <a href="{{$bend->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a>
                                     @endif

                                    @if (array_key_exists( 'instagram', $bend))

                                        @if (strpos($bend->instagram, 'https://') === false)
                                          @php $bend->instagram = 'https://'.$bend->instagram; @endphp
                                        @endif
                                        <a href="{{$bend->instagram}}" target="_blank"><i class="fa fa-instagram"></i></a>
                                     @endif

                                     @if (array_key_exists( 'youtube', $bend))
                                        <a href="{{$bend->youtube}}" target="_blank"><i class="fa fa-youtube-play"></i></a>
                                     @endif

                                     @if (array_key_exists( 'bandcamp', $bend))
                                        <a href="{{$bend->bandcamp}}" target="_blank"><i class="fa fa-bandcamp"></i></a>
                                     @endif

                                  </div>

                                  <div class="row m-0 w-100">
                                    <div class="container-fluid p-4">
                                      <div class="row m-0 w-100">

                                        <div class="col-12 col-md-auto p-0">
                                          <img alt="logo" width="100" height="100" class="m-b img-circle img-band-profile" src="https://jammeet.com/slike_burza_m/avatari/medium-{{$bend['profil_slika']}}">
                                        </div>

                                        <div class="col pr-0 pl-0 pl-md-4">
                                          <h3 class="mt-2 mb-1">
                                            <a class="font-weight-bold bend-name" href="">{{$bend['name']}}</a>
                                          </h3>

                                          <div class="category-content">
                                              @if ( $bendSettings != null)

                                                  @if ( $bendSettings->category == 1)

                                                      <p class="opacity-4 mb-0 d-flex align-items-center">
                                                          <i class="far fa-check-circle mr-2 col-auto p-0"></i>
                                                          <span class="col p-0">Whitelisted</span>
                                                      </p>
                                                  @endif

                                                  @if ( $bendSettings->category == 2)

                                                      <p class="opacity-4 mb-0 d-flex align-items-center">
                                                          <i class="fas fa-times-circle mr-2 col-auto p-0"></i>
                                                          <span class="col p-0">Blacklisted</span>
                                                      </p>
                                                  @endif

                                              @endif
                                          </div>


                                          @if( count($bend['zanrovi']) > 0)
                                            <p class="opacity-4 mb-0 d-flex">
                                              <i class="fas fa-music mr-2 mt-2 col-auto p-0"></i>
                                              <span class="col p-0">
                                                <?php
                                                  $string = '';
                                                  foreach ($bend['zanrovi'] as $ind => $zanr) {
                                                     if($ind==0)
                                                      {
                                                        $string.=$zanr['name'];
                                                      }
                                                     else if($ind < 5)
                                                      {
                                                        $string.=', '.$zanr['name'];
                                                      }
                                                     else if($ind == 5)
                                                      {
                                                        $string.='...';
                                                      }
                                                  }
                                                 ?>
                                                  {{$string}}
                                              </span>
                                            </p>
                                          @endif
                                          <p class="opacity-4 mb-0 d-flex align-items-center">
                                            <i class="fas fa-map-marker-alt ml1rem mr-2  col-auto p-0"></i>
                                            <span class="col p-0">{{$bend['grad']}}</span>
                                          </p>

                                        </div>

                                      </div>
                                    </div>
                                  </div>


                                <div class="row m-0 w-100 border-bottom bend-info-div p-4">

                                      <?php

                                          $probe = App\TerminiProstorije::bendUkupnoProba(Auth::user()->id, $bend['id']);
                                      ?>
                                  <p class="w-100">
                                    <span>Ukupno odrađenih proba: </span> <strong>{{$probe['prije']}}</strong>
                                    <br>
                                    <span>Trenutno dogovorenih proba: </span> <strong>{{$probe['poslije']}}</strong>
                                  </p>

                                  <p class="w-100">
                                    <strong>Napomene:</strong>
                                    <br>

                                      <span class="notes-content">
                                      @if ( $bendSettings != null)

                                          @if ( !empty($bendSettings->notes))

                                                {{$bendSettings->notes}}
                                          @endif

                                      @endif
                                      </span>
                                  </p>

                                  <button class="btn btn-edit-custom bg-white d-inline-block ml-auto mt-4 font-14 py-1 px-3 shadow" data-bend_id="{{$bend['id']}}" data-toggle="modal" data-target="#bendModal">
                                    <i class="fas fa-pencil-alt mr-1"></i>
                                    Uredi
                                  </button>

                                </div>


                              </div>

                          </div>
                      </div>

                  @endforeach
                </div>
              <br><br>

            </div>
@endsection



@section('extra_scripts2')
<script>

    $(document).ready(function (){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $('.btn-edit-custom').click(function (){

            var id = $(this).data('bend_id')
            var name = $('.bend-div'+id+' .bend-name').text()

            $('#bendModal .modal-title').text(name)
            $('.save-bend-info').data('bend_id', id)

            $.ajax({
                url: '../../api/bendovi/get',
                method: 'post',
                data: {
                    user_id: active_user.id,
                    jammeet_id: id
                },
                success: function(settings){

                    //$('input[name="category"]').val(settings.category);
                    //$('input:radio[name="category"]').filter('[value="'+settings.category+'"]').attr('checked', true);
                    $("#category"+settings.category).prop("checked", true);
                    $('textarea[name="notes"]').val(settings.notes);

                }
            });


        })



        $('.save-bend-info').click(function (){

            var id = $(this).data('bend_id')
            var category = $('input[name="category"]:checked').val();
            var notes = $('textarea[name="notes"]').val();

            $('#bendModal .modal-title').text(name)

            $.ajax({
                url: '../../api/bendovi/save',
                method: 'post',
                data: {
                    user_id: active_user.id,
                    jammeet_id: id,
                    category: category,
                    notes: notes,
                },
                success: function(settings){
                    console.log(settings)

                    var cat = '';

                    if(settings.category==1)
                    {
                        cat = '<p class="opacity-4 mb-0 d-flex align-items-center">'+
                                '<i class="far fa-check-circle mr-2 col-auto p-0"></i>'+
                                '<span class="col p-0">Whitelisted</span>'+
                                '</p>';
                    }

                    if(settings.category==2)
                    {
                        cat = '<p class="opacity-4 mb-0 d-flex align-items-center">'+
                            '<i class="fas fa-times-circle mr-2 col-auto p-0"></i>'+
                            '<span class="col p-0">Blacklisted</span>'+
                            '</p>';
                    }
                    console.log(cat)

                    $('.bend-div'+settings.jammeet_id+' .notes-content').text(settings.notes)
                    $('.bend-div'+settings.jammeet_id+' .category-content').empty()
                    $('.bend-div'+settings.jammeet_id+' .category-content').append(cat)

                }
            });


        })

        $('#bendModal').on('show.bs.modal', function (e) {
            // do something...
        })
    })

</script>

@endsection
