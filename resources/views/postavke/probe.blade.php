@extends('layouts.admin')

@section('extra_scripts')

    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/dropzone/dropzone.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/dropzone/dropzone.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('selectizejs/custom.css') }}">

@endsection


@section('content')

    <div class="mt-5 mt-md-0 mt-xl-4" id='wrap'>
        <div class="row">
            <div class="col-12">
                <div class="single-pro-review-area mt-t-30 mg-b-15">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xxl-6 col-xl-9 col-12 px-0 px-md-3">
                                <div class="product-payment-inner-st shadow px-md-4">
                                        <div class="product-tab-list tab-pane active in" id="description">
                                            <div class="row">
                                                <div class="col-12">

                                                    <div class="review-content-section mt-0 mt-md-3">
                                                        <div id="dropzone1" class="pro-ad addcoursepro">
                                                            <form id="prostorija-form" class="addcourse" action="{{url('postavke/probe')}}" method="post" enctype="multipart/form-data" id="demo1-upload">
                                                             {{csrf_field()}}

                                                                <div class="row">

                                                                    <div class="col-12">
                                                                        <ul id="myTabedu1" class="tab-review-design">
                                                                            <li class="active"><a href="">Postavke za rezervaciju proba</a></li>
                                                                        </ul>
                                                                    </div>


                                                                    <div class="col-12">

                                                                        <?php
                                                                        $brojProba = [];

                                                                        for ($i = 1; $i<=10;$i++)
                                                                        {
                                                                            $brojProba[$i] = $i;
                                                                        }

                                                                        ?>

                                                                        <div class="form-group mb-4 c-required name-div  {{ $errors->has('name') ? 'c-error' :'' }}">
                                                                            {{Form::label('probe_unaprijed', 'Maksimalan broj proba koji bend može rezervirati unaprijed', array('class' => 'd-block'))}}

                                                                            {!! Form::select('probe_unaprijed', $brojProba, $user->probe_unaprijed, ['class' => 'form-control max-100']) !!}

                                                                        </div>


                                                                    </div>
                                                                </div>


                                                                <div class="row mt-4">
                                                                    <div class="col-lg-12">
                                                                        <div class="payment-adress">
                                                                            <button type="submit" class="btn btn-primary submit-btn waves-effect waves-light pull-left">Spremi promjene</button>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </form>




                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('extra_scripts2')
    <!-- dropzone JS -->
    <script src="{{ URL::asset('admin-panel/js/dropzone/dropzone.js') }}"></script>

    <script src= "{{ URL::asset('selectizejs/dist/js/standalone/selectize.js') }}" ></script>

    <script type="text/javascript">


        var active_user = <?php echo json_encode($user) ?>


        var uploaded_imgs = [];
    var submited = false;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
      });

    $(document).ready(function(){


    });

    </script>

@endsection
