@extends('layouts.admin')

@section('extra_scripts')

    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/dropzone/dropzone.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/dropzone/dropzone.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('selectizejs/custom.css') }}">

@endsection


@section('content')

    <div class="mt-5 mt-md-0 mt-xl-4" id='wrap'>
        <div class="row">
            <div class="col-12">
                <div class="single-pro-review-area mt-t-30 mg-b-15">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xxl-6 col-xl-9 col-12 px-0 px-md-3">
                                <div class="product-payment-inner-st shadow px-md-4">
                                        <div class="product-tab-list tab-pane active in" id="description">
                                            <div class="row">
                                                <div class="col-12">

                                                    <div class="review-content-section mt-0 mt-md-3">
                                                        <div id="dropzone1" class="pro-ad addcoursepro">
                                                            <form id="prostorija-form" class="addcourse" action="{{url('postavke/profil')}}" method="post" enctype="multipart/form-data" id="demo1-upload">
                                                             {{csrf_field()}}

                                                                <div class="row">

                                                                    <div class="col-12">
                                                                        <ul id="myTabedu1" class="tab-review-design">
                                                                            <li class="active"><a href="">Uređivanje profila</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-12">
                                                                        {{ Form::hidden('profil_slika', $user->profil_slika, ['id' => 'profilSlikaInput']) }}


                                                                        <div class="form-group mb-4 c-required name-div  {{ $errors->has('name') ? 'c-error' :'' }}">
                                                                            {!! Form::text('name', $user->name, ['class' => 'form-control', 'placeholder' => 'Naziv', 'id' => 'prostorijaNameInput']) !!}

                                                                            @error('name')
                                                                                <div class="c-validation error-div text-white" aria-hidden="true">Naziv mora biti ispunjen</div>
                                                                            @enderror
                                                                        </div>


                                                                        <div class="form-group mb-4 id_mjesto-div {{ $errors->has('id_mjesto') ? 'c-error' :'' }}">
                                                                            {{ Form::select('id_mjesto', $maticno_podrucja_optgroup, $user->id_mjesto, array('class' => 'demo-default', 'id' => 'mjesto_selectize', "multiple" => ""))}}


                                                                            @error('id_mjesto')
                                                                            <div class="c-validation error-div text-white" aria-hidden="true">Mjesto ne smije biti prazno</div>
                                                                            @enderror
                                                                        </div>


                                                                        <div class="form-group mb-4">

                                                                            {!! Form::textarea('opis', $user->opis, ['class' => 'form-control', 'rows' => 1, 'cols' => 5, 'placeholder' => 'Opis']) !!}
                                                                        </div>

                                                                        <div class="form-group mb-4">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-phone"></i></span>
                                                                                </div>
                                                                                {!! Form::text('mobitel', $user->mobitel, ['class' => 'form-control', 'placeholder' => 'Mobitel']) !!}

                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group mb-4">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-facebook"></i></span>
                                                                                </div>
                                                                                {!! Form::text('facebook', $user->facebook, ['class' => 'form-control', 'placeholder' => 'Facebook']) !!}

                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group mb-4">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-instagram"></i></span>
                                                                                </div>
                                                                                {!! Form::text('instagram', $user->instagram, ['class' => 'form-control', 'placeholder' => 'Instagram']) !!}

                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group mb-4">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-youtube"></i></span>
                                                                                </div>
                                                                                {!! Form::text('youtube', $user->youtube, ['class' => 'form-control', 'placeholder' => 'Youtube kanal']) !!}

                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                            </form>

                                                            <div class="row mb-1">

                                                              <div class="col-12">
                                                                  <ul id="myTabedu1" class="tab-review-design">
                                                                      <li class="active"><a style="font-size:16px !important" href="">Profil slika</a></li>
                                                                  </ul>
                                                              </div>

                                                            </div>


                                                            @if($user->profil_slika != 'default.jpg')

                                                                <div class="row text-center text-lg-left profil-slika-div">

                                                                        <div class="col-md-4 col-6 position-relative img-div  is-profile" data-name="{{$user->profil_slika}}">
                                                                            <button type="button" class="close position-absolute delete-img" data-name="{{$user->profil_slika}}" aria-label="Delete">
                                                                                <span class="position-relative" aria-hidden="true">×</span>
                                                                            </button>

                                                                            <span class="d-block mb-4 h-100">
                                                                        <div class="col-auto p-0 position-relative img-container">
                                                                          <img class="img-fluid img-thumbnail cursor-pointer prostorija-img"  src="{{ URL::asset('uploads/prostorije') . '/'. $user->profil_slika }}" alt="">

                                                                            <div class="position-absolute profile-text w-100">
                                                                              <div class="row m-0 w-100">
                                                                                <p class="mb-0 text-white text-center w-100">
                                                                                  Profil slika
                                                                                </p>
                                                                              </div>
                                                                            </div>

                                                                        </div>
                                                                      </span>

                                                                        </div>

                                                                </div>

                                                            @endif


                                                            <div class="row">
                                                              <div class="col-12">

                                                                <form method="post" action="{{url('postavke/dropzone')}}" enctype="multipart/form-data"
                                                                              class="dropzone border-0 position-relative" id="dropzone">
                                                                    @csrf
                                                                </form>

                                                              </div>
                                                            </div>

                                                            <div class="row mt-4">
                                                                <div class="col-lg-12">
                                                                    <div class="payment-adress">
                                                                        <button type="button" class="btn btn-primary submit-btn waves-effect waves-light pull-left">Spremi promjene</button>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('extra_scripts2')
    <!-- dropzone JS -->
    <script src="{{ URL::asset('admin-panel/js/dropzone/dropzone.js') }}"></script>

    <script src= "{{ URL::asset('selectizejs/dist/js/standalone/selectize.js') }}" ></script>

    <script type="text/javascript">


        var active_user = <?php echo json_encode($user) ?>


        var uploaded_imgs = [];
    var submited = false;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
      });

    Dropzone.options.dropzone =
     {
            maxFilesize: 3, //mb
            maxFiles: 1,
            parallelUploads: 1,
            dictMaxFilesExceeded: "Ne možete objaviti više od jedne slika",
            dictInvalidFileType: 'Dozvoljene su .jpeg, .jpg i .png datoteke',
            dictFileTooBig: 'Datoteka je prevelika, maksimalna veličina datoteke može biti 3MB',
            dictRemoveFile: "Ukloni sliku",
            addRemoveLinks: true,
            acceptedFiles: '.jpeg,.jpg,.png',
            autoProcessQueue: false,
            dictDefaultMessage: '<i class="fa fa-download edudropnone d-block opacity-3 font-18 dropzone-help" aria-hidden="true"></i><span class="dropzone-help mb-3 d-block">Kliknite ovdje da biste dodali sliku ili ju povucite i ispustite.</span>',
            renameFile: function(file) {
                var dt = new Date();
                var time = dt.getTime();
                file.name = time+file.name
                return file.name;
            },
            timeout: 5000,

            init: function() {

             this.on("removedfile", function(file) {


                 if (this.getQueuedFiles().length === 0) {
                       $('.dropzone-help').addClass('d-block')
                      $('.dropzone-help').removeClass('d-none')
                 }

                /*
                if(file.accepted)
                {

                    var img = $('#profilSlikaInput').val();
                    $('#profilSlikaInput').val('');

                    $.ajax({
                        url: '../../api/profil-slika',
                        method: 'delete',
                        data:  {
                          img: img
                        },
                        success: function(ids){

                        }
                    });

                }
                */

              });

              this.on("addedfile", function (file) {

                  $('.dropzone-help').removeClass('d-block')
                  $('.dropzone-help').addClass('d-none')

                  if (this.files.length > 10) {
                    this.removeFile(file);
                  }

              });


              this.on("complete", function (file) {

                if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0 && submited) {

                  $('#profilSlikaInput').val(uploaded_imgs);
                  $('#prostorija-form').submit()
                }
              });

          	},
            success: function(file, response)
            {
                //$('#profilSlikaInput').val(response.success);
                uploaded_imgs.push(response.success);
            },
            error: function(file, response)
            {
              $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response);

            }
    };

    $(document).ready(function(){


        var arr_selectize_mjesto = ['#mjesto_selectize'];


        $.each(arr_selectize_mjesto, function(ind, sel){

            $(sel).selectize({
                maxItems:2,
                plugins: ['remove_button'],
                onItemAdd(value, $item)
                {
                    var selectize = $(sel)[0].selectize;
                    var arr_s = selectize.getValue();

                    if(arr_s.length>1){
                        selectize.removeItem(arr_s[0]);
                    }

                },
                onItemRemove(value)
                {
                    var selectize = $(sel)[0].selectize;
                    if(selectize.getValue().length==0)
                    {
                        selectize.addItem(value);
                    }
                },
                onBlur()
                {
                    var selectize = $(sel)[0].selectize;
                    if(selectize.getValue().length==0)
                    {
                        selectize.addItem(last_value);
                    }
                },
            });

        });


      $('.submit-btn').click(function() {

        if( $('#prostorijaNameInput').val() == '' )
        {
            $('.name-div').addClass('c-error')
            $('.name-div').append('<div class="c-validation error-div text-white" aria-hidden="true">Naziv mora biti ispunjen</div>')

            setTimeout(function(){ $('.error-div').fadeOut('slow', function() {
              $('.error-div').remove()
              $('.c-required').removeClass('c-error')
            }); }, 3000);

        }
          else if( $('#mjesto_selectize').val() == '' )
          {
              $('.id_mjesto-div').addClass('c-error')
              $('.id_mjesto-div').append('<div class="c-validation error-div text-white" aria-hidden="true">Mjesto mora biti ispunjeno</div>')

              setTimeout(function(){ $('.error-div').fadeOut('slow', function() {
                  $('.error-div').remove()
                  $('.c-required').removeClass('c-error')
              }); }, 3000);

          }
        else{
            submited = true;
            var myDropzone = Dropzone.forElement("#dropzone");

            if (myDropzone.files.length === 0) {
              $('#prostorija-form').submit()
            }
            else{
              myDropzone.processQueue();
            }

        }

      })

      setTimeout(function(){ $('.error-div').fadeOut('slow', function() {
        $('.c-required').removeClass('c-error')
      }); }, 3000);


    });



    $(document).on("click",".delete-img",function() {

        var r = confirm("Želite izbrisati profilnu sliku?");

        if (r == true) {


            $.ajax({
                url: '../../api/users/profileimg',
                method: 'delete',
                data:  {
                    img: $(this).data('name'),
                    user_id: active_user.id
                },
                success: function(ids){

                }
            });


            $('#profilSlikaInput').val('default.jpg');

            $('.profil-slika-div').remove()

        }

    })

    </script>

@endsection
