@extends('layouts.admin')

@section('extra_scripts')

    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/dropzone/dropzone.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}">

@endsection


@section('content')

    <div class="mt-5 mt-md-0 mt-xl-4" id='wrap'>
        <div class="row">
            <div class="col-12">
                <div class="single-pro-review-area mt-t-30 mg-b-15">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xxl-6 col-xl-9 col-12 px-0 px-md-3">
                                <div class="product-payment-inner-st shadow px-md-4">
                                        <div class="product-tab-list tab-pane active in" id="description">
                                            <div class="row">
                                                <div class="col-12">

                                                    <div class="review-content-section mt-0 mt-md-3">
                                                        <div id="dropzone1" class="pro-ad addcoursepro">
                                                            <form id="prostorija-form" class="addcourse" action="{{url('prostorije/novo')}}" method="post" enctype="multipart/form-data" id="demo1-upload">
                                                             {{csrf_field()}}

                                                                <div class="row">

                                                                    <div class="col-12">
                                                                        <ul id="myTabedu1" class="tab-review-design">
                                                                            <li class="active"><a href="">Dodaj novu prostoriju</a></li>
                                                                        </ul>
                                                                    </div>

                                                                    <div class="col-12">
                                                                        {{ Form::hidden('profil_slika', null, ['id' => 'profilSlikaInput']) }}


                                                                        <div class="form-group mb-4 c-required name-div  {{ $errors->has('name') ? 'c-error' :'' }}">
                                                                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Naziv prostorije', 'id' => 'prostorijaNameInput']) !!}

                                                                            @error('name')
                                                                                <div class="c-validation error-div text-white" aria-hidden="true">Naziv prostorije mora biti ispunjen</div>
                                                                            @enderror
                                                                        </div>

                                                                        <div class="form-group mb-4">

                                                                            {!! Form::textarea('opis', null, ['class' => 'form-control', 'rows' => 1, 'cols' => 5, 'placeholder' => 'Opis prostorije']) !!}
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                            </form>

                                                            <div class="row mb-1">

                                                              <div class="col-12">
                                                                  <ul id="myTabedu1" class="tab-review-design">
                                                                      <li class="active"><a style="font-size:16px !important" href="">Galerija</a></li>
                                                                  </ul>
                                                              </div>

                                                            </div>


                                                            <div class="row">
                                                              <div class="col-12">

                                                                <form method="post" action="{{url('prostorije/dropzone')}}" enctype="multipart/form-data"
                                                                              class="dropzone border-0 position-relative" id="dropzone">
                                                                    @csrf
                                                                </form>

                                                              </div>
                                                            </div>

                                                            <div class="row mt-4">
                                                                <div class="col-lg-12">
                                                                    <div class="payment-adress">
                                                                        <button type="button" class="btn btn-primary submit-btn waves-effect waves-light pull-left">Dodaj prostoriju</button>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('extra_scripts2')
    <!-- dropzone JS -->
    <script src="{{ URL::asset('admin-panel/js/dropzone/dropzone.js') }}"></script>

    <script type="text/javascript">

    var uploaded_imgs = [];
    var submited = false;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
      });

    Dropzone.options.dropzone =
     {
            maxFilesize: 3, //mb
            maxFiles: 10,
            parallelUploads: 10,
            dictMaxFilesExceeded: "Ne možete objaviti više od deset slika",
            dictInvalidFileType: 'Dozvoljene su .jpeg, .jpg i .png datoteke',
            dictFileTooBig: 'Datoteka je prevelika, maksimalna veličina datoteke može biti 3MB',
            dictRemoveFile: "Ukloni sliku",
            addRemoveLinks: true,
            acceptedFiles: '.jpeg,.jpg,.png',
            autoProcessQueue: false,
            dictDefaultMessage: '<i class="fa fa-download edudropnone d-block opacity-3 font-18 dropzone-help" aria-hidden="true"></i><span class="dropzone-help mb-3 d-block">Kliknite ovdje da biste dodali slike ili ih povucite i ispustite. Možete dodati maksimalno 10 slika.</span>',
            renameFile: function(file) {
                var dt = new Date();
                var time = dt.getTime();
                file.name = time+file.name
                return file.name;
            },
            timeout: 5000,

            init: function() {

             this.on("removedfile", function(file) {

                 if (this.getQueuedFiles().length === 0) {
                       $('.dropzone-help').addClass('d-block')
                      $('.dropzone-help').removeClass('d-none')
                 }

                /*
                if(file.accepted)
                {

                    var img = $('#profilSlikaInput').val();
                    $('#profilSlikaInput').val('');

                    $.ajax({
                        url: '../../api/profil-slika',
                        method: 'delete',
                        data:  {
                          img: img
                        },
                        success: function(ids){

                        }
                    });

                }
                */

              });

              this.on("addedfile", function (file) {

                  $('.dropzone-help').removeClass('d-block')
                  $('.dropzone-help').addClass('d-none')

                  if (this.files.length > 10) {
                    this.removeFile(file);
                  }

              });


              this.on("complete", function (file) {

                if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0 && submited) {

                  $('#profilSlikaInput').val(uploaded_imgs);
                  $('#prostorija-form').submit()
                }
              });

          	},
            success: function(file, response)
            {
                //$('#profilSlikaInput').val(response.success);
                uploaded_imgs.push(response.success);
            },
            error: function(file, response)
            {
              $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response);

            }
    };

    $(document).ready(function(){


      $('.submit-btn').click(function() {

        if( $('#prostorijaNameInput').val() == '' )
        {
            $('.name-div').addClass('c-error')
            $('.name-div').append('<div class="c-validation error-div text-white" aria-hidden="true">Naziv prostorije mora biti ispunjen</div>')

            setTimeout(function(){ $('.error-div').fadeOut('slow', function() {
              $('.error-div').remove()
              $('.c-required').removeClass('c-error')
            }); }, 3000);

        }
        else{
            submited = true;
            var myDropzone = Dropzone.forElement("#dropzone");

            if (myDropzone.files.length === 0) {
              $('#prostorija-form').submit()
            }
            else{
              myDropzone.processQueue();
            }

        }

      })

      setTimeout(function(){ $('.error-div').fadeOut('slow', function() {
        $('.c-required').removeClass('c-error')
      }); }, 3000);


    });


    </script>

@endsection
