@extends('layouts.admin')


@section('content')
<div class="mt-5" id='wrap'>
    <div class="row">
        <div class="col-12">

            <div class="contacts-area mg-b-15">
                <div class="container-fluid">

                    <div class="row mx-0 mb-4 w-100 d-lg-none">
                        <a href='{{url("/prostorije/novo")}}' class="btn btn-primary d-inline-block font-14 py-2 px-3" type="button">
                          <i class="fa fa-plus mr-1"></i>
                          Dodaj novu prostoriju
                        </a>
                    </div>

                    <div class="row">

                    @foreach($user->prostorije as $i => $prostorija)

                                <div class="col-xl-3 px-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12 mb-3">
                                    <div class="student-inner-std res-mg-b-30 shadow">
                                        <div class="student-img">
                                            <img  style="height: 205px;" src='{{ URL::asset("uploads/prostorije/{$prostorija->getProfil()}") }}' alt="" />
                                        </div>
                                        <div class="student-dtl">
                                            <h2>{{$prostorija->name}}</h2>
                                            <p class="dp mb-4">{{$prostorija->opis}}</p>
                                            <p class="dp-ag"><b>Trenutno dogovorene probe:</b> {{$prostorija->totalProbaDogovorenih}}</p>
                                            <p class="dp-ag">Broj proba održanih: {{$prostorija->totalProbaHad}}</p>

                                            <div class="row m-0 w-100">
                                                <a href='{{url("/prostorije/{$prostorija->id}")}}' class="btn btn-primary d-inline-block ml-auto mt-4 font-14 py-1 px-3" type="button">
                                                  <i class="fas fa-pencil-alt"></i>
                                                  Uredi
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                    @endforeach

                </div>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
