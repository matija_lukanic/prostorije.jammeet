@extends('layouts.admin')

@section('extra_scripts')

    <link rel="stylesheet" href="{{ URL::asset('admin-panel/css/dropzone/dropzone.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}">

@endsection


@section('content')

    <div class="mt-5 mt-md-0 mt-xl-4 prostorija-page" id='wrap'>
        <div class="row">
            <div class="col-12">
                <div class="single-pro-review-area mt-t-30 mg-b-15">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xl-9 col-xxl-6 col-12">
                                <div class="product-payment-inner-st shadow px-md-4">
                                        <div class="product-tab-list tab-pane active in" id="description">
                                            <div class="row">
                                                <div class="col-12">

                                                    <div class="review-content-section">
                                                        <div id="dropzone1" class="pro-ad addcoursepro">
                                                            <?php
                                                              $url = url("prostorije/".$prostorija->id);
                                                             ?>

                                                              <div class="row">

                                                                    <div class="col-12">
                                                                        <ul id="myTabedu1" class="tab-review-design">
                                                                            <li class="active w-100">
                                                                              <a href="">{{$prostorija->name}}</a>

                                                                                  <form method="post" action='{{url("prostorije/delete/{$prostorija->id}")}}' class="float-md-right mt-4 mt-md-0">
                                                                                     {{csrf_field()}}
                                                                                     <button type="submit" class="btn btn-danger submit-btn waves-effect"><i class="far fa-trash-alt"></i> Izbriši prostoriju</button>
                                                                                   </form>
                                                                            </li>
                                                                        </ul>
                                                                    </div>

                                                                   <form id="prostorija-form" class="addcourse w-100" action="{{$url}}" method="post" enctype="multipart/form-data">
                                                                    {{csrf_field()}}

                                                                      <div class="col-12">
                                                                          {{ Form::hidden('profil_slika', null, ['id' => 'profilSlikaInput']) }}


                                                                          <div class="form-group mb-4 c-required name-div  {{ $errors->has('name') ? 'c-error' :'' }}">
                                                                              {!! Form::text('name', $prostorija->name, ['class' => 'form-control', 'placeholder' => 'Naziv prostorije', 'id' => 'prostorijaNameInput']) !!}

                                                                              @error('name')
                                                                                  <div class="c-validation error-div text-white" aria-hidden="true">Naziv prostorije mora biti ispunjen</div>
                                                                              @enderror
                                                                          </div>

                                                                          <div class="form-group mb-4">

                                                                              {!! Form::textarea('opis', $prostorija->opis, ['class' => 'form-control', 'rows' => 1, 'cols' => 5, 'placeholder' => 'Opis prostorije']) !!}
                                                                          </div>

                                                                      </div>
                                                                    </form>
                                                                </div>



                                                          <div class="row mb-1">

                                                            <div class="col-12">
                                                                <ul id="myTabedu1" class="tab-review-design">
                                                                    <li class="active"><a style="font-size:16px !important" href="">Galerija</a></li>
                                                                </ul>
                                                            </div>

                                                          </div>


                                                          <div class="row text-center text-lg-left">

                                                              @foreach($prostorija->gallery as $img)

                                                                <div class="col-md-4 col-6 position-relative img-div  {{ $img->profile == 1 ? 'is-profile' : '' }}"   data-id="{{$img->id}}" data-name="{{$img->url}}">
                                                                  <button type="button" class="close position-absolute delete-img" data-id="{{$img->id}}" data-name="{{$img->url}}" aria-label="Delete">
                                                                    <span class="position-relative" aria-hidden="true">×</span>
                                                                  </button>

                                                                  <span class="d-block mb-4 h-100">
                                                                    <div class="col-auto p-0 position-relative img-container">
                                                                      <img class="img-fluid img-thumbnail cursor-pointer prostorija-img"  data-id="{{$img->id}}" src="{{ URL::asset('uploads/prostorije') . '/'. $img->url }}" alt="">

                                                                      @if ($img->profile == 1)
                                                                        <div class="position-absolute profile-text w-100">
                                                                          <div class="row m-0 w-100">
                                                                            <p class="mb-0 text-white text-center w-100">
                                                                              Profil slika
                                                                            </p>
                                                                          </div>
                                                                        </div>
                                                                      @endif

                                                                    </div>
                                                                  </span>

                                                                </div>

                                                              @endforeach

                                                            </div>


                                                            <div class="row">
                                                              <div class="col-12">

                                                                <form method="post" action="{{url('prostorije/dropzone')}}" enctype="multipart/form-data"
                                                                              class="dropzone border-0 position-relative" id="dropzone">
                                                                    @csrf
                                                                </form>

                                                              </div>
                                                            </div>

                                                            <div class="row mt-4">
                                                              <div class="col-lg-12">
                                                                  <div class="row">
                                                                      <div class="payment-adress col-12">
                                                                          <button type="button" class="btn btn-primary submit-btn waves-effect waves-light float-left">Spremi promjene</button>
                                                                      </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('extra_scripts2')
    <!-- dropzone JS -->
    <script src="{{ URL::asset('admin-panel/js/dropzone/dropzone.js') }}"></script>

    <script type="text/javascript">

    var uploaded_imgs = [];
    var submited = false;
    var prostorija_id = <?php echo $prostorija->id ?>;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
      });

    Dropzone.options.dropzone =
     {
            maxFilesize: 3, //mb
            maxFiles: 10,
            parallelUploads: 10,
            dictMaxFilesExceeded: "Ne možete objaviti više od deset slika",
            dictInvalidFileType: 'Dozvoljene su .jpeg, .jpg i .png datoteke',
            dictFileTooBig: 'Datoteka je prevelika, maksimalna veličina datoteke može biti 2MB',
            dictRemoveFile: "Ukloni sliku",
            addRemoveLinks: true,
            acceptedFiles: '.jpeg,.jpg,.png',
            autoProcessQueue: false,
            dictDefaultMessage: '<i class="fa fa-download edudropnone d-block opacity-3 font-18 dropzone-help" aria-hidden="true"></i><span class="dropzone-help mb-3 d-block">Kliknite ovdje da biste dodali slike ili ih povucite i ispustite. Možete dodati maksimalno 10 slika.</span>',
            renameFile: function(file) {
                var dt = new Date();
                var time = dt.getTime();
                file.name = time+file.name
                return file.name;
            },
            acceptedFiles: ".jpeg,.jpg,.png",
            addRemoveLinks: true,
            timeout: 5000,

            init: function() {

             this.on("removedfile", function(file) {

                 if (this.getQueuedFiles().length === 0) {
                       $('.dropzone-help').addClass('d-block')
                      $('.dropzone-help').removeClass('d-none')
                 }

                /*
                if(file.accepted)
                {

                    var img = $('#profilSlikaInput').val();
                    $('#profilSlikaInput').val('');

                    $.ajax({
                        url: '../../api/profil-slika',
                        method: 'delete',
                        data:  {
                          img: img
                        },
                        success: function(ids){

                        }
                    });

                }
                */

              });

              this.on("addedfile", function (file) {

                  $('.dropzone-help').removeClass('d-block')
                  $('.dropzone-help').addClass('d-none')

                  if (this.files.length > 10) {
                    this.removeFile(file);
                  }

              });


              this.on("complete", function (file) {

                if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0 && submited) {
                  $('#profilSlikaInput').val(uploaded_imgs);
                  $('#prostorija-form').submit()
                }
              });

          	},
            success: function(file, response)
            {
                //$('#profilSlikaInput').val(response.success);
                uploaded_imgs.push(response.success);
            },
            error: function(file, response)
            {
              $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response);

            }
    };

    $(document).ready(function(){


      $('.submit-btn').click(function() {

        if( $('#prostorijaNameInput').val() == '' )
        {
            $('.name-div').addClass('c-error')
            $('.name-div').append('<div class="c-validation error-div text-white" aria-hidden="true">Naziv prostorije mora biti ispunjen</div>')

            setTimeout(function(){ $('.error-div').fadeOut('slow', function() {
              $('.error-div').remove()
              $('.c-required').removeClass('c-error')
            }); }, 3000);

        }
        else{
            submited = true;
            var myDropzone = Dropzone.forElement("#dropzone");

            if (myDropzone.files.length === 0) {
              $('#prostorija-form').submit()
            }
            else{
              myDropzone.processQueue();
            }

        }

      })

      setTimeout(function(){ $('.error-div').fadeOut('slow', function() {
        $('.c-required').removeClass('c-error')
      }); }, 3000);


    });


    $(document).on("click",".delete-img",function() {

      var r = confirm("Želite izbrisati ovu sliku?");

      if (r == true) {

          $.ajax({
              url: '../../api/profil-slika',
              method: 'delete',
              data:  {
                img: $(this).data('name'),
                id: $(this).data('id')
              },
              success: function(ids){

              }
          });

          $('.img-div[data-id="'+$(this).data('id')+'"]').remove()

      }

    })

    $(document).on("click",".prostorija-img",function() {

        $('.img-div.is-profile').removeClass('.is-profile')
        $('.img-div[data-id="'+$(this).data('id')+'"]').addClass('is-profile')

        if($('.profile-text').length){
            $('.img-div[data-id="'+$(this).data('id')+'"] .img-container').append($('.profile-text'));
        }
        else{

            $('.img-div[data-id="'+$(this).data('id')+'"] .img-container').append(
                              '<div class="position-absolute profile-text w-100">'+
                                '<div class="row m-0 w-100">'+
                                  '<p class="mb-0 text-white text-center w-100">'+
                                    'Profil slika'+
                                  '</p>'+
                                '</div>'+
                              '</div>');
        }


        $.ajax({
            url: '../../api/profil-slika',
            method: 'put',
            data:  {
              prostorija_id: prostorija_id,
              img_id: $(this).data('id')
            },
            success: function(ids){

            }
        });


    })





    </script>

@endsection
