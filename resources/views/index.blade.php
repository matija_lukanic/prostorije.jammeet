@extends('layouts.admin')


@section('content')
    <div class="mt-5 mt-md-0 mt-xl-4" id='wrap'>
        <div class="row">
            <div class="col-12">

                <div class="widget-program-box">
                    <div class="container-fluid px-md-0">
                        <div class="d-flex flex-column flex-md-row align-items-stretch align-self-start justify-content-around justify-content-xl-start">

                            <div class="col-xl-3 bg-transparent px-2 px-xl-4 col-md-4 col-sm-8 mx-auto mx-lg-0 mb-4 p-0 col-xs-12 hpanel widget-int-shape dk-res-t-pro-30">

                                    <div class="panel-body px-3 py-4">
                                        <div class="text-center content-box">
                                            <h2 class="m-b-xs">Kalendar</h2>
                                            <div class="m icon-box mt-3">
                                                <i class="educate-icon educate-event"></i>
                                            </div>
                                            <p class="small mg-t-box">
                                                Dodajte termine za prostoriju u kalendar. Termine bendovi mogu vidjeti online i rezervirati ih.
                                            </p>
                                            <button class="btn btn-danger widget-btn-4 btn-sm  p-0"><a class="d-inline-block text-white p-1 pl-4 pr-4 m-0" href="{{url('/kalendar')}}">Kalendar</a></button>
                                        </div>

                                    </div>

                                    <div class="hpanel shadow-inner hbgred bg-4">
                                        <div class="panel-body px-3 py-4">
                                            <div class="text-center content-bg-pro">
                                                <h3>Title text</h3>
                                                <p class="text-big font-light">
                                                    0,43
                                                </p>
                                                <small>
                                                    Lorem Ipsum passages and more recently with desktop published software.
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            <div class="col-xl-3 bg-transparent px-2 px-xl-4 col-md-4 col-sm-8 mx-auto mx-lg-0 mb-4 p-0 col-xs-12 hpanel widget-int-shape dk-res-t-pro-30">
                                    <div class="panel-body px-3 py-4">

                                        <div class="text-center content-box h-100">
                                            <div class="d-flex align-items-start flex-column h-100 ">
                                                <div class="mb-auto text-center w-100">
                                                    <h2 class="m-b-xs">Prostorije</h2>
                                                    <div class="m icon-box mt-3">
                                                        <i class="fa fa-music"></i>
                                                    </div>
                                                </div>
                                                <div class="w-100">
                                                    <p class="small mg-t-box">
                                                        Dodajte sve prostorije koje imate dostupne za probu kako bi mogli dodavati termine.
                                                        <br>
                                                    </p>

                                                    <button class="btn btn-danger widget-btn-4 btn-sm  p-0"><a class="d-inline-block text-white p-1 pl-4 pr-4 m-0" href="{{url('/prostorije')}}">Prostorije</a></button>

                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                    <div class="hpanel shadow-inner hbgred bg-4">
                                        <div class="panel-body px-3 py-4">
                                            <div class="text-center content-bg-pro">
                                                <h3>Title text</h3>
                                                <p class="text-big font-light">
                                                    0,43
                                                </p>
                                                <small>
                                                    Lorem Ipsum passages and more recently with desktop published software.
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            <div class="col-xl-3 bg-transparent px-2 px-xl-4 col-md-4 col-sm-8 mx-auto mx-lg-0 mb-4 p-0 col-xs-12 hpanel widget-int-shape dk-res-t-pro-30">

                                    <div class="panel-body px-3 py-4">

                                        <div class="text-center content-box h-100">
                                            <div class="d-flex align-items-start flex-column h-100 ">
                                                <div class="mb-auto text-center w-100">
                                                    <h2 class="m-b-xs">Bendovi</h2>
                                                    <div class="m icon-box mt-3">
                                                        <i class="fa fa-users"></i>
                                                    </div>
                                                </div>
                                                <div class="w-100">
                                                    <p class="small mg-t-box">
                                                        Provjerite koji su Vam sve bendovi svirali u prostorijama.
                                                        <br>
                                                    </p>

                                                    <button class="btn btn-danger widget-btn-4 btn-sm  p-0"><a class="d-inline-block text-white p-1 pl-4 pr-4 m-0" href="{{url('/bendovi')}}">Bendovi</a></button>

                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                    <div class="hpanel shadow-inner hbgred bg-4">
                                        <div class="panel-body px-3 py-4">
                                            <div class="text-center content-bg-pro">
                                                <h3>Title text</h3>
                                                <p class="text-big font-light">
                                                    0,43
                                                </p>
                                                <small>
                                                    Lorem Ipsum passages and more recently with desktop published software.
                                                </small>
                                            </div>
                                        </div>
                                    </div>

                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
